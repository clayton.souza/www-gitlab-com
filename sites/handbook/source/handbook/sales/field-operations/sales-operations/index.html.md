---
layout: handbook-page-toc
title: "Sales Operations"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## **Charter**

Sales Operations is a part of Field Operations.
We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies, and direct support.
Sales Operations main focus is on the Sales organization and supports this group through the following key functions:

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://about.gitlab.com/handbook/sales/territories/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Territories</a>
  <a href="https://about.gitlab.com/handbook/sales/field-operations/sales-operations/#sales-operations-go-to-market" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Go To Market</a>
  <a href="https://about.gitlab.com/handbook/sales/qbrs/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Quarterly Business Reviews</a>
  <a href="https://about.gitlab.com/handbook/business-ops/resources/#account-ownership-rules-of-engagement" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Rules of Engagement</a>
    </div>

## **Meet the Team**

- Lindsay Schoenfeld - Senior Manager, Sales Operations
- Amber Stahn - Senior Sales Operations Analyst
- Tav Scott - Senior Sales Operations Analyst
- Melinda Soares - Senior Sales Operations Analyst

## **Teams We Work Closely With**

<div class="flex-row" markdown="0" style="height:80px">
    <a href="https://about.gitlab.com/handbook/customer-success/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Customer Success</a>
    <a href="https://about.gitlab.com/handbook/sales/field-operations/sales-systems/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Systems</a>
    <a href="https://about.gitlab.com/handbook/sales/commissions/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Commissions</a>
    <a href="https://about.gitlab.com/handbook/marketing/marketing-operations/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Marketing Operations</a>
    <a href="https://about.gitlab.com/handbook/business-ops/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Operations</a>
</div>

<div class="flex-row" markdown="0" style="height:80px">
    <a href="https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Deal Desk</a>
    <a href="https://about.gitlab.com/handbook/resellers/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Channel Partner</a>
    <a href="https://about.gitlab.com/handbook/sales/field-operations/field-enablement/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Field Enablement</a>
    <a href="https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Strategy</a>
  </div>

## **How to Communicate with Us**

The #sales-support slack channel is monitored by several groups within Field Operations to give guidance or direction.
Salesforce [chatter](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#salesforce-chatter-communication ) `@sales-support` is monitored by the Deal Desk team and they will re-direct any questions to Sales Operations if needed.

* Slack: [#sales-support](https://gitlab.slack.com/archives/sales-support)
* Salesforce: [@sales-Support](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F94M000000fy2K)

## **How to Get Help**

<details>
<summary markdown='span'>
  Steps to creating an issue
</summary>

1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations), making sure to provide detailed business requirements.
Please leave assignee blank.
1. There are existing templates to use, most will fall under the General Request template. 
1. New Issues that are in review will be tagged with the `SalesOps::New_Request` label automatically on creation.
1. An issue will be assigned to a Milestone, given an assignee and the `SalesOps::Assigned` if it is ready to be worked on.
1. Any issue that cannot be slotted into the next two milestones will be put in the backlog denoted by `SalesOps::Backlog` until it can be planned.
1. Please review the status of any issue on our agile [board.](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations)

</details>

<details>
<summary markdown='span'>
  Steps to requesting help using Salesforce Chatter
</summary>

1. [Contract assistance](https://about.gitlab.com/handbook/sales/#reaching-the-sales-team-internally)
1. [Updating or creating Opportunity Splits ](https://about.gitlab.com/handbook/sales/#opportunity-splits)
1. [Salesforce Lightning for Gmail](https://about.gitlab.com/handbook/sales/#salesforce-lightning-for-gmail)
1. [Support from the Community Advocacy Team](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#working-with-the-community-advocacy-team)
1. [DataFox/DiscoverOrg segmentation conflicts](https://about.gitlab.com/handbook/business-ops/resources/#segmentation)
1. [Reassigning to a Territory Rep](https://about.gitlab.com/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
1. [Requesting Reassignment](https://about.gitlab.com/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
1. [If LEAD or CONTACT is owned by SDR team member](https://about.gitlab.com/handbook/business-ops/resources/#record-creation-in-salesforce)
1. [Locked Deal](https://about.gitlab.com/handbook/business-ops/resources/#locking-opportunities-as-a-result-of-their-at-risk-potential)
1. [Deal Desk assistance](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#salesforce-chatter-communication)

</details>

<details>
<summary markdown='span'>
  Steps to create an issue for an account list import
</summary>

Here are the guidelines for requesting account list loads from Sales Operations.
Please follow the instructions below.
The SLA for account list loads into Salesforce is 5-7 business days.
  
**For uploading a list of net new accounts**
* We have a template you can use to dedupe a list of accounts you have sourced [here in the google drive](https://docs.google.com/spreadsheets/d/1zm4uA2_d7aj31BY2wTxNw6HoElyWJQkjMYY4e1D3tRM/edit#gid=1823098798).
This template will help you dedupe the account list and also format your list for upload into Salesforce.
Please follow the directions in the README in the template doc and reach out to #sales-support in Slack if you have any questions.

**Preparing the list:**
1. Clean up list to remove any duplicates and columns not needed. 
1. Update field names to Salesforce compatible values. Only include the required fields listed below.
1. Unless you discuss with us prior, nothing else will be loaded and the extra columns will be ignored in the import.
1. Account Source format: List - Name of Source - Date with no spaces or characters.
1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations) using the Account List Import [template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues).
Include a link to the list and description of the list load.
1. One tab per sheet, one list load per sheet / one sheet per issue.
 

**Required Fields:**

| **Label** | **Field Name** | **Data Type** |
| ------ | ------ | ------ |
| Account Source | AccountSource | Picklist |
| Employees | NumberOfEmployees | Number(8,0) |
| Account Name | Name | Name |
| Type | Type | Picklist |
| Account Record Type | RecordType | Picklist |
| Website | Website | URL(255) |
| Billing Street | Billing Street| Address |
| Billing City | Billing City | Address |
| Billing State/Province | Billing State/Province | Address|
| Billing Zip/Postal Code | Billing Zip/Postal Code | Address |
| Billing Country | Billing Country | Address |

<details>
<summary markdown='span'>
  Operators Guide: Instructions for Sales Operations team on completing Account List Imports
</summary>

**Prepping the List**

1. Check the data in the provided list: 
    * Only one tab per list
    * Make sure the google sheet template was used to ensure the list has been de duped
    * Double check that everything is in the correct format. [**Reference for correct Billing Address Formatting.**](https://docs.google.com/spreadsheets/d/1_FOkc7CHBDaEzPmpoXtkiQE-u-QB_uuJIcAA4mU1gd0/edit?usp=sharing) and ensure that there are no extra columns, only required fields in the template.
    * If there is an exception and there are additional columns not in the template check the account fields and check to see if they are in the correct format, check for field dependencies, etc. 

1. Add Account Owner ID
    * Add a column to the spreadsheet and Label it Account Owner ID
    * Go into Salesforce Setup>Manage Users>Users and find your User ID (15 or 18) number and copy
    * Paste your User ID into the Account Owner ID column in the spreadsheet 
1. Add Record Type ID:
    * Insert a column to the left of the RecordType column and label RecordType ID
    * Go into Salesforce Setup>Customize>Accounts>Record Types (Pull the number out of the URL (number after id= and before the &))
    * Paste into the RecordType ID column>copy down
1. Format the Account Source Column
    * Naming convention: List-Vendor-Identifier-Date (example:List-DiscoverOrg-FranceAC-20200407
    * Copy and paste values down the column with the correct format
1. Create the Account Source Name in Salesforce
    * Go into Salesforce>Setup>Customize>Account>Fields>Account Source
    * Select New and type the Account Source Name you created in step 4
    * Select the record types that it pertains to (standard and channel)>save
    * Select Reorder>check “Display values alphabetically, not in the order entered
1. Save the prepped list for the data load:
    * Save the Excel file in CSV format on your computer
    * Go into the Salesforce reports tab>Account List Import Folder
    * Clone an existing list report. Add a filter Account Source equals and select the name of the list you created from the picklist
    * Select SAVE AS and type the name of the list you created in step 4
    * Save and Run and leave open as this can be refreshed during the data load 
    
**Data Load Instructions**
1. Open the data loader. Select Insert. Login in production
1. Go to Settings. Change the batch size to 20. Click OK
1. Select Account Object. Browse for the file. Select list CSV file. Click Next
1. Create or Edit Map Fields. Auto-Match Fields to Columns (Usually works for most columns
    * Make sure that website is mapped
    * Map Number of employees to the NumberofEmployees: Manual-Admin field
    * Drop and drag any missing fields
    * Leave RecordType blank (nothing matches)
    * Click OK. Click Next. Select where the error log will save to. Make sure to save to a location otherwise it will save to a mysterious location on your machine
</details>
</details>

<details>
<summary markdown='span'>
  Steps to creating an issue for account list update
</summary>

**Preparing the list:**
1. Create an Account report in SFDC. 
1. Include the Account ID as a field on your report. This is required to do a mass update. 
1. Include the fields on your report that you want updated. 
1. Save your report in a public folder so it can be accessed later if needed.
1. Export your report, making sure you have included the Account ID.
1. Update the export.  Please keep the original values in the export and create a new column with the new values. Ex: If you want to update website, you will have 2 website columns.  Website and then Website New.  Website New will be the column you create and where the new value is captured.
1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations) using the Account List Import [template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues). Include a link to the list and description of the list load.
1. One Tab per sheet, one list update per sheet / one sheet per issue.
1. If you need help pulling the report or walking though these steps, please slack us with a link to the issue you created in the `#sales-support` Slack channel.

</details>


## **How we work**

- [Sales Operations Agile Board](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1655825?label_name[]=SalesOPS)
- [Sales Operations Project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations)
- [Focus Fridays](https://about.gitlab.com/handbook/sales/#focus-fridays)
- Field Operations Calendar

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_hhs5o85g05lho9agbkfhv8lc40%40group.calendar.google.com&ctz=America%2FLos_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

## **Important Resources & Processes**

<details>
<summary markdown='span'>
  Updating Zuora and Salesforce Quote Templates
</summary>

In order to update quote templates that are used in Salesforce, and pulled in from Zuora, please reference the below resources provided by Zuora. 
1. [General overview to update quote templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates)
1. [Leveraging mail merge fields to update templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/C_Customize_Quote_Template_using_Word_Mail_Merge) - This must be completed in Microsoft word and saved accordingly
1. [Reference the merge fields that are supported](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/K_Supported_Merge_Fields_for_Quote_Templates_and_Mapping_to_Salesforce#Charge_Summary.Quote_Rate_Plan_Merge_Fields)
1. [How to displaty multiple quote charges in a table](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/E_Customize_Quote_Templates_Using_Microsoft_Word_Mail_Merge)
1. [Uploading to Zuora and connect to Salesforce](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings) 

</details> 

<details>
<summary markdown='span'>
  Primary Quote System
</summary>

The Primary Quote system is a 1:1 relationship in SFDC that connects the total transaction amount on a quote with the amount on its related opportunity.
This is to ensure we are forecasting the same amount that we will book and enables further automation as the quote is sent to Zuora billing.
To support sales situations that require multiple quotes (for instance: a small deal option and a big deal option), sales reps can identify which one of their quote is "Primary".

[Primary Quote technical documentation here:](/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#primary-quote-system) 

</details> 

<details>
<summary markdown='span'>
  Tech Stack
</summary>

The full company tech stack list with definitions can be found on the [Business Operations - Tech Stack Details page
](/handbook/business-ops/tech-stack/). Here are the tools that the Sales Operations team works with on a daily basis.
1. [Clari](/handbook/business-ops/tech-stack/#clari)
1. [Datafox](/handbook/business-ops/tech-stack/#datafox)
1. [Gainsight](/handbook/business-ops/tech-stack/#gainsight)
1. [Leandata](/handbook/business-ops/tech-stack/#leandata)
1. [Salesforce](/handbook/business-ops/tech-stack/#salesforce)
1. [Chorus](/handbook/business-ops/tech-stack/#chorus)

</details> 

<details>
<summary markdown='span'>
  Overdue Opportunity Process
</summary>

1. An overdue opportunity report goes out every Tuesday to managers.
1. An email alert goes out directly to the opportunity owner when the oppty is past due upon edit of the opportunity. Ex: an action needs to happen to trigger the email. 
1. Sales Operations will give until the 15th of the month and then forklift any opportunity with a close date in the past month to a future close date.
1. Managers have "over due" reports on their dashboards for review as needed.

</details> 

<details>
<summary markdown='span'>
  Salesforce Access Removal Process
</summary>

1. To ensure the appropriate users have access and that we're being fiscally responsible in terms of overall usage, users with no usage in 90 days will be deactivated. 
1. Usage will be reviewed once at the beginning of the second month of the quarter so as not to disrupt any quarter end/quarter start cadences. The dates are scheduled on the Field Ops calander.
1. An [Access Change Request](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#access-change-request) will be created and an email will be sent to users as extra notification.
1. If access is needed in the future, please submit a new [Access Request](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#single-person-access-request) and we can confirm if SFDC is the correct place to gather this information or if other tools can provide it.

<details>
<summary markdown='span'> Instructions for Sales Operations team on completing SFDC Access Removal Process
</summary>

**Identifying the Users**

1. Check the Data in the [Provided Report](https://gitlab.my.salesforce.com/00O4M000004aGGo): 
    * Make sure the last login date is set to LESS than or equal to 90 Days ago. 
    * Verify that no integration users or users that might be tied to an external system are not included in the access removal.  If there are questions, error on the cautious side and work with Sales Systems. 
1. Create an [Access Change Request](handbook/business-ops/employee-enablement/it-ops-team/access-requests/#access-change-request)
    * List the users that will be removed so that we have record of reason and users if needed in the future. 
1. Email Notification:
    * Work with Sales Operations Manager to send the email notification.  This is an extra step for extra visibility to users and might not always be needed depending on volume and other communication that has occured. 
    * Example Email:
    
        Hope that you are having a good week.  We in SalesOps are doing a cleanup of our tech stack tools to ensure the appropriate users have access and that we're being fiscally responsible in terms of overall usage.  During this process, we've discovered that nearly **XXX** Salesforce.com users haven't logged in for 3 months or more.  If you're receiving this message, your user account falls into this bucket. 
        As such, I'm writing to let you know that we'll be deactivating your SFDC license on XXX in an effort to ensure that we have enough licenses to provision to our new and existing Sales people; who leverage the tool daily. 
        So what does this mean for you?  
        Effective XXX you will no longer have access to SFDC
        If there's critical SFDC data that you need for your role, please submit a new Access Request and we can confirm if SFDC is the correct place to gather this information or if other tools can provide it
        Please let me know if you have any questions.

1. Deactivating Users in Salesforce
    * From Salesforce, access the setup menu and then manager users.  
    * Locate the user and uncheck the Active box, or click the Freeze button.  Freeze should only be used if the user can not be fully deactivated due to impact to other system or process. 
    * If a user is frozen set a reminder in an issue to go back and deactivate user once related systems / process have been udpated. 

</details>
</details>


## **Sales Operations Sponsored Dashboards and Maintenance**

The Sales Operations team has sponsored a comprehensive but consumable "Reporting Package" (via SFDC Dashboards) with validated (SalesOps approved) metrics to the Account Executives.
This list will continue to evolve and will be maintained during the onboarding and offboarding process.
In addition to sponsored reporting, Sales Ops will maintain existing reports by archiving or deleting any report or dashboard not used in 180 days.

<details>
<summary markdown='span'>Enterprise Dashboards </summary>

**Enterprise: West**
1. [WEST ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBZ)
2. [FY21 CQ WEST ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBo)

**Enterprise: East**
3. [EAST ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBU)
4. [FY21 CQ EAST ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBj)

**Enterprise: PubSec**
5. [PubSec Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXC3)
6. [FY21 CQ PubSec Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXCD)

**Enterprise: EMEA**
7. [EMEA Enterprise Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oX78)
7. [FY21 CQ EMEA ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slbx)

**Enterprise: APAC**
8. [APAC ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXAR)
9. [FY21 CQ APAC ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXA2)

</details> 

## **Sales Operations Go To Market**

### **Account Ownership Rules of Engagement**

[Account Ownership Rules of Engagement Handbook](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)

### **Sales Territories**

[Sales Territories](/handbook/sales/territories/)

### **Territory Success Planning (TSP)**

TSP is an automated process workflow intended to properly segment & route Salesforce accounts to the correct Sales territory & respective owner.
This clarifies who should own which accounts & reduces current Ops overhead to manage manually.
TSP fields are designed to be real time reflections of the best data we have, not necessarily the current Go To Market approach.
Requests to override the TSP information can also be submitted in the Account Review section of the account.

<details>
<summary markdown='span'>Operators Guide: Territory Success Planning (TSP) </summary>

**Primary TSP Workflow Components**
 1. **Account Routing** (*Next Owner recommendation process*):
     * Sales Segment (i.e. max employee count of the account hierarchy)
     * Primary Account HQ Address (of top Parent Account in the hierarchy)
         * Inputs for both are formulated via our standardized account enrichment tools (in order):
         * Manual Override > DataFox > DiscoverOrg > Ship-To > Bill-To
    * LeadData then compares these TSP Input Values against our [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722) & automatically outputs an Approved Next Owner and Territory.
2. **Account Assignment** (*Account Transfer Process*):
     * (Re)Assignment of an account to the correct owner
     * Updating of Account Territory, Sales Segment, Employees fields

**Firmographic TSP Fields**
   * `[TSP] Sales Segment`: Segment of the account based on the MAX employee count in that account's hierarchy (regardless if MAX is parent or child).
   *  `[TSP] Account Employees`: Number of employees **for this specific account** 
   *  `[TSP] MAX Family Employees`:  MAX employee count (number) in hierarchy
   *  `[TSP] Address (Street, City, State, Post Code, Country)`: Location of Ultimate Parent Account based on the TSP data hierarchy
   *  `[TSP] Geo Story`: Source of address data from TSP Data Hierarchy

**Ownership TSP Fields**
   * `[TSP] Next Approved Owner`: Owner of territory as determined by [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722)
   * `[TSP] Transfer Date`: Date when account ownership will change to `TSP Next Approved Owner`

**Territory TSP Fields**
   * `[TSP] Territory`: Territory account falls under, as per the [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722)
   * `[TSP] Region`: Sales territory region the account falls under
   * `[TSP] Subregion`: Sales territory sub-region the account falls under
   * `[TSP] Area`: Sales territory area the account falls under
  
</details> 

### **What if TSP is wrong? How can I request a change?**

In the event our data enrichment tools are outdated or incorrect (primary address or employee count), you can submit a request to override this information.

<details>
<summary markdown='span'>Process for Requesting TSP Changes:</summary>

1. **Request Process:**
    *  Enter desired TSP information into the following fields (this can be either Account, Employee Count, or both).
    *  Be sure to include source of correct data (ROE must still be followed)   
          *  `[User Input] Employee Count`
          *  `[User Input] Employee Source`
          *  `[User Input] Address Street`
          *  `[User Input] Address City`
          *  `[User Input] Address State`
          *  `[User Input] Address Post Code`
          *  `[User Input] Address Country`
          *  `[User Input] Address Source`

1.  **Operations Review Process:**
Ops will review these requests on a periodic basis, and provide a response in the `[TSP] Override Status` field:
    *  **Approved** - Account changes accepted, & `[TSP] Effective Date` populated by Ops.
    *  **Rejected** - Reason added to `[TSP] Decision Rationale` field.
    *  **Needs Approval/More Info** - Info needed added to `[TSP] Decision Rationale` field.<p/>
1.  **Execution:**
    *  Turnaround time for Approved TSP changes to re-populate typically takes 24-48 hours.
    *  Accounts with a `[TSP] Effective Date` populated will be re-routed each night to the `[TSP] Next Approved Owner`. 
    *  `Account Territory`, `Sales Segment` & `Employees` fields will also be updated upon TSP transfer, to continually align accounts.

</details> 
