---
layout: handbook-page-toc
title: "UX Research Coordination"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Research Coordination at GitLab

Research Coordinators at GitLab are responsible for managing all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management. 

To request support from a Research Coordinator, please follow the [process outlined here](/handbook/engineering/ux/ux-research/#how-ux-research-product-and-product-design-work-together-on-research).

### How many studies can a coordinator support per milestone?

The short answer is approximately 5-10. The long answer is that it depends on the type of study and the target population. When each study only requires 3-5 participants who are relatively abundant in our database and community, the number of studies recruited per milestone could be ~10. This is also contingent upon the Product Designer and Product Manager submitting their complete requests early enough to allow coordinators to juggle multiple recruiting efforts at once. When there are surveys that require 50+ respondents, or studies addressing new user groups or features, the number of studies recruited per milestone may be closer to 5. Those studies require sustained effort and creativity. 

Never hesitate to bring up a recruitment request - the coordinator will work with you to schedule the recruiting effort. 

### Can we combine recruiting requests to make recruiting go faster?

In some cases, absolutely. This works best when two or more studies are quite similar (e.g. both require interviews on a similar timeline and have an identical screener). 

It is not usually advisable to combine recruitment for studies that are quite different from each other, for example a large survey and a set of user interviews. This is due to the different timeframes for these different studies. 

In order to get people scheduled for interviews, we generally want to keep the feedback loop as tight as possible - by inviting qualified participants to schedule for an interview asap after they respond to the screener. This means that the whole recruitment process might take only a matter of days. Conversely, surveys requiring a large number of responses may require multiple rounds of emails through Qualtrics over a period of weeks. By the time you remember to reach out to some users who responded early on, so much time has passed that you're likely to see a very low response rate. This results in us needing to recruit twice. 

### Recruitment methods

1. **GitLab First Look (formerly the UX Research Panel)** is a group of users who have opted in to receive research studies from GitLab. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html). This is a good fit for studies that are aimed at GitLab users who are software developers and related roles. The panel currently does not have many security professionals, nor senior engineering leaders. 

1. **Respondent.io** is a recruitment service that is a good choice for studies aimed at software professionals who are not necessarily GitLab users. This has been a decent source of security professionals and some other harder-to-reach users. 

1. **Social outreach.** Social posts may go out on GitLab's brand channels. The Research Coordinator uses the Social Request template in the corporate marketing project to request this type of post. This is a good choice for studies primarily aimed at GitLab users. Product Managers, Product Designers, and other teammates are highly encouraged to help promote their studies to their networks.

1. **Direct Sourcing.** We can utilize LinkedIn to approach and source potential participants. Anyone at GitLab is able to request a [LinkedIn Recruiter license](/handbook/hiring/sourcing/#upgrading-your-linkedin-account). This [Unfiltered video](https://youtu.be/rc2IX1e2sQ8) and [slide deck](https://docs.google.com/presentation/d/1LI9qXLRQSnikPiHztDQBapGrDn5Nimsf-K8g1r3j9Do/edit#slide=id.g29a70c6c35_0_68) provide an overview on how to use Linkedin Recruiter to source participants for your study.

### Thank you gifts

As a small token of thanks (and to ensure a higher participation/completion rate) we send gifts to research participants. These can be requested by opening an issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Incentives%20request.md).

* User interviews, usability testing, or 'buy a feature' research: $60 (or equivalent currency) Amazon gift card per 30 minutes.

* Surveys, beta testing, card sorts, and tree tests: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift cards, or a GitLab swag item. 

* Design evaluations: Unpaid.

* Swag can also be sent on an ad hoc basis, as requested by researchers, PMs, and others. This is a nice touch and an opportunity for personalization after a particularly good conversation. Coordinators can reach out to the corporate events team (Emily Kyle) for swag codes.

#### Fulfillment

Requests for thank you gifts should be fulfilled at least twice per week so that users receive their gift promptly after participating in research. It's important to maintain a customer service mindset when interacting with participants - receiving their gift is the end of the research cycle and another touchpoint in their relationship with GitLab.

##### Tango Card

Tango Card can be used to send gift cards to users around the world. 

The accounting team funds the account with a lump sum amount from the pre-approved research incentives budget. We issue cards from that prepaid amount. The research coordinator sends a report by the 1st of every month to `ap@gitlab.com` with the following information:

* How much the account was funded at the beginning of the month
* How much was spent on issued cards
* The final balance in the account

The report should be for the previous month (e.g., the report should be sent by 11/1 for cards send between 10/1 and 10/31).

##### Respondent.io

Respondent requires us to pay participants directly through the platform. Coordinators should use their personal card and submit prompt expense reports through Expensify for reimbursement. 

##### Directly through Amazon

**This should not be necessary while Tango Card is operational.** Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

When attempting to send incentives internationally, your credit card or PayPal may be denied. You can offer to send an Amazon.com card (not in the user's country's store) or use [transferwise](https://transferwise.com/), which requires bank routing information. 


### Email tips

When sending emails to GitLab First Look, coordinators may customize emails in a few ways. 
* Always send yourself at least one test email
* Add an opening line above the study details. This can be a way to reference the time of year or add other personalization not in the template.  
* Feel free to lightly re-write parts of the email template to make them sound more like your voice. Some First Look participants receive many emails from us, so changing them up slightly will be nice reinforcement that a human is actually on the other side of the process.
* Experiment with subject lines (`We have a new study for you!`; `See if you're a match with our new study`; etc.)
* Adjust the `sent from` display name to `Name from Gitlab`
* If your response rate is low, but you think that your target participants are in your segment, send one reminder email rather than immediately starting a new segment.
    - Send the reminder no sooner than 4 days after your first email was sent 
    - Add a brief note to the top of the email. Otherwise Qualtrics will just send a link to the survey
    - Write a new subject line (`We still want to hear from you!`; `Reminder: Take our quick survey`)
    - If you suspect that not many of your target participants are in your segment (i.e., because they are security professionals or others that we've struggled to recruit) don't send a reminder. Sending too many reminders increases the risk of unsubscribes. 

Below are email/message templates that you can use for communicating with research participants. Whenever possible, we strongly encourage you to customize each message for the individual and circumstance.

#### Invitation to partake in a user interview, following successful screening

```
Hi! We have a study coming up, and I was wondering if you might be interested in participating with us. Based on your response to our survey, you look like a great fit! Sessions are taking place from `[XX-XX]`, and they last about `[XX]` minutes over Zoom (videoconference).

For this round of testing, we’ll be chatting about `[Replace with research subject. Example: What tools you use, what your process is like, etc.]`.

Participants who complete a session will be compensated with a `[Replace with compensation. Example: $60 Amazon gift card, or approximate value in your home currency]`.

If you are interested, go here `[Link to your Calendly]` and choose one time and day that works for you as soon as possible. There are limited spots available.

Please let me know if you have any questions.
```

#### Outreach internal customers (GitLab) in Slack

```
Hi all! :wave: We are in the process of `[Replace with research subject]` to `[Replace with research goals for context]`.
We need internal customers to answer a few questions. If you would like to help us out, please reply to this survey `[Link to research survey]`. Thank you!
```

### UX research promotion

Coordinators should set aside time to interact with users on social. It's recommended to post any new UX blog posts and search `gitlab ux` at least once per week. 

When appropriate, link to GitLab First Look (and [grab an image](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/tree/master/design/social-media/ads-share-images/gitlab-first-look/png) to upload) in responses to comments about GitLab's UX. This is important for raising awareness of the research program, growing the panel, and performing research with users who may have had negative experiences with GitLab. 

Some sample responses:
* (To a positive comment) `Thank you so much! Would you consider joining our research program if you haven't already? You can specify which areas of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`
* (To a negative comment) `I'm so sorry to hear you've had a negative experience! Would you consider giving feedback through our research program? You can specify exactly which parts of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`

If possible, also tweet (and/or blog!) about interesting findings that arise from studies in a timely fashion. The best content comes from chronicling feedback we received and what we did with it. It's important to close the feedback loop and demonstrate that we're not only listening, but taking action. 








