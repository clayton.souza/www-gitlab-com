---
layout: handbook-page-toc
title: "Code of Business Conduct & Ethics"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Code of Business Conduct & Ethics

GitLab is committed to serving our customers and employing individuals with personal standards consistent with that of our [values](/handbook/values/). This Code is designed to deter wrongdoing and to promote:

  * Honest and ethical conduct, including the ethical handling of actual or apparent conflicts of interest between personal and professional relationships.
  * Full, fair, accurate, timely, and understandable disclosure in reports and documents we file with regulatory agencies and in our other public communications.
  * Compliance with applicable laws, rules, and regulations.
  * The prompt internal reporting of violations of this Code.
  * Accountability for adherence to this Code.

Our Code applies to all directors, officers, employees, and contractors of GitLab and its affiliates and subsidiaries. Agents and vendors of GitLab are also expected to read, understand, and abide by this Code.

This Code should help guide your conduct in the course of our business. Many of the principles described in this Code are general in nature, and the Code does not cover every situation that may arise. Use common sense and good judgment in applying this Code. If you have any questions about applying the Code, please seek guidance. Not all information regarding the conduct of our business is found in this Code. Please review the applicable policies and procedures in specific areas as they apply as found in our [Team Handbook](/handbook/).

The Code of Conduct will be reviewed on an annual basis to ensure compliance with applicable laws and industry standards.

### Complying with the Code

To maintain the highest standards of integrity, we must dedicate ourselves to complying with this Code, company policies and procedures, and applicable laws and regulations. Violations of this Code not only damage our company’s standing in the communities we serve, they may also be illegal. Team members involved in violating this Code will likely face negative consequences. GitLab will take the appropriate disciplinary action in response to each case, up to and including termination. In addition, team members involved may be subject to government fines, or criminal or civil liability.

### Reporting Violations

If you think this Code or any GitLab policy is being violated, or if you have an ethics question, you have several options:

* Discuss the issue with your supervisor or manager.
* Discuss the issue with another supervisor or manager.
* Contact the [People Group](https://about.gitlab.com/handbook/people-group/)
* Contact [GitLab’s 24-hour hotline](#how-to-contact-gitlabs-24-hour-hotline).

All reports (formal or informal) made to a GitLab supervisor, manager or executive should be promptly escalated to the People Business Partners or to the Chief People Officer (CPO).  GitLab will then review the report promptly and thoroughly to determine if an investigation is warranted.

#### Investigation Process

If the People Group has determined it appropriate, GitLab will promptly initiate an appropriate investigation into all possible violations of law and/or GitLab policy. The People Business Partner assigned to the business department will investigate all reports unless:

* the complaint is against a member of the People Business Partner team, in which case the investigation will be conducted by Legal (internal Chief Legal Officer or outside counsel, as appropriate).
* if the case requires the People Business Partner to investigate in their own Client group, the CPO and the People Business Partner will determine if a People Business Partner not supporting the group should lead the investigation.
* the complaint is made against a member of the executive team or there are multiple complainants regarding the same individual and/or issue, then outside counsel will be retained by the CPO to conduct the investigation. The Board of Directors will be notified of any complaints made against a member of the executive team alleging illegal conduct and/or egregious unethical conduct.

If the complaint involves a member of the executive team that has direct oversight for the Legal department or involves a member of the Legal department, then the CPO will work with outside counsel to conduct the investigation. If the complaint involves a member of the executive team that has direct oversight for the People Group or involves a member of the People Group, then Legal will work with outside counsel to conduct the investigation.

GitLab expects all employees and contractors to cooperate fully and candidly in investigations.

#### Investigation Timeline

GitLab will make all reasonable efforts to initiate an investigation into the allegation(s) and conclude the investigation in a timely fashion. Depending on the type of investigation the steps and timeline for each investigation will vary.

#### Investigation Findings

The investigation findings will be reported back to the CPO. Based on the investigation findings, the CPO will make a determination as to whether the allegation(s) were founded, unfounded or inconclusive. This determination will be documented in writing and made part of the investigation report. The determinations are as follows:

* **Violation Found**: Where a violation of GitLab policies, workplace rules or law is found to have occurred, the CPO will review the findings and make a recommendation for corrective action to the executive leader of the accused's reporting line. Together, the CPO and the business unit will determine the proper corrective action. If the accused is a member of the executive team then the CPO will confer with the CEO, and where necessary, the Board of Directors. Once a corrective action has been determined, the accused will be notified of the finding and of the specific corrective actions to be taken. The accused employee's manager will also be notified if appropriate. No details about the nature or extent of disciplinary or corrective actions will be disclosed to the complainant(s) or witness(es) unless there is a compelling reason to do so (for example, personal safety).
* **No Violation Found**: In this situation, the complainant (if known) and the accused should be notified that GitLab investigated the allegation(s) and found that the evidence did not support the claim.
* **Inconclusive investigation**: In some cases, the evidence may not conclusively indicate whether the allegation(s) were founded or unfounded. If such a situation occurs, the complainant (if known) and the accused will be notified that a thorough investigation was conducted, but GitLab was unable to establish the truth or falsity of the allegation(s). GitLab will take appropriate steps to ensure that the persons involved understand the requirements of GitLab's policies and applicable law, and that GitLab will monitor the situation to ensure compliance in the future.

#### How to Contact GitLab's 24-hour hotline:

GitLab has engaged Lighthouse Services to provide an anonymous ethics and compliance hotline for all team members. The purpose of the service is to insure that any team member wishing to submit a report anonymously can do so without the fear of [retribution](#commitment-to-non-retaliation).

Reports may cover but are not limited to the following topics: ethical violations, wrongful discharge, unsafe working conditions, internal controls, quality of service, vandalism and sabotage, [sexual harassment](/handbook/anti-harassment/#sts=Sexual Harassment), theft, discrimination, conduct violations, alcohol and substance abuse, threats, fraud, bribery and kickbacks, conflict of interest, improper conduct, theft and embezzlement, violation of company policy, violation of the law, misuse of company property, falsification of contract, reports or records.

Please note that the information provided by you may be the basis for an internal and/or external investigation into the issue you are reporting and your anonymity will be protected by Lighthouse to the extent possible by law. However, your identity may become known during the course of the investigation because of the information you have provided. Reports are submitted by Lighthouse to a company designee for investigation according to our company policies.

Lighthouse Services toll free number and other methods of reporting are available 24 hours a day, 7 days a week for use by team members.

* Website: [https://www.lighthouse-services.com/gitlab](https://www.lighthouse-services.com/gitlab)
* USA Telephone:
  * English speaking USA and Canada: 833-480-0010
  * Spanish speaking USA and Canada: 800-216-1288
  * French speaking Canada: 855-725-0002
  * Spanish speaking Mexico: 01-800-681-5340
* All other countries telephone: +1-800-603-2869
* E-mail: reports@lighthouse-services.com (must include company name with report)
* Fax: (215) 689-3885 (must include company name with report)

The reports sent to Lighthouse Services are shared with the [Chief Legal Officer](/job-families/legal/chief-legal-officer/) and [the Chair of the Audit Committee](/handbook/board-meetings/#audit-committee-charter).


### Material Nonpublic Information / Inside Information

Employees know that they have to protect Confidential Information.  But did you know that employees also cannot **use** Confidential Information?  Employees are not permitted to use or share Confidential Information for purposes of trading securities, illegal activities or for any other purpose except to conduct [GitLab](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/) business. Insider trading (or dealing) laws and regulations globally prohibit buying or selling a company’s securities while in possession of Confidential Information about that company that is Nonpublic and Material.

Employees can also violate these laws by disclosing Material Nonpublic Confidential Information to another person if, as a result, that person – or any other person – buys or sells a security while aware of that information. If you make such a disclosure or use such information, there can be stiff penalties, even if you yourself stand to make no financial gain.

Clarity on whether information is “Nonpublic” or “Material” or what restrictions exist on the use or distribution of such information is provided below.  Further questions should be directed to the Legal or Compliance Department.

**Definitions**
**Nonpublic Information** is Confidential Information that is maintained or otherwise handled by GitLab that may be harmful to GitLab, it’s employees or others with whom GitLab does business as defined in the Data Classification policy, including, but not limited to, any of the following:
Information used for, or obtained from, a rated entity or its agent for the purpose of determining a rating or pending rating;
Major personnel changes;
Lawsuits that may trigger heavy damages;
Mergers, acquisitions;
Substantial contracts not in the ordinary course of business;
Information concerning the rating committee process, including, but not limited to, the voting breakdown in the committee, the fact that a member of the rating committee disagreed with the ultimate committee decision, and the names or titles of members of the committee;
Nonpublic Information relating to GitLab’s customers and information provided by GitLab’s customers, including internal risk assessment models, information concerning the performance of those models, historical default data for loan portfolios, customer-specific loan pricing information and information concerning portfolio composition and concentration;
GitLab’s nonpublic financial information and sales projections; or
Information regarding GitLab’s business plans, strategies, proprietary systems, algorithms, formulas, methodologies, product designs, processes, research and development information and trade secrets; or Special Personal Information.

**Material Information** has no precise definition and is subject to a variety of interpretations. Accordingly, for the purposes of this Policy, “Material Information” refers to any information that: (i) might have an effect on the market for a security generally; or (ii) might affect an investment decision of a reasonable investor.

Examples of Material Information may include, but are not limited to:
sales results
earnings or estimates (including reaffirmations or changes to previously released earnings information)
dividend actions
strategic plans
new products, discoveries or services
major personnel changes
Merger, acquisition and divestiture plans
Financing plans
Proposed securities offerings
Government actions
Substance of major litigation or potential claims
Negotiation or termination of major contracts
Rating or pending rating actions

If you are not sure whether or not a particular piece of information is Material Information, you should err on the side of caution and assume that it is Material Information. In other jurisdictions, Material Information may be referred to as “inside information” or “price-sensitive information”.

Material Nonpublic Information refers to information that is both Material and Non-Public Confidential Information.

### Physical Assets and Resources

All employees and contractors must protect our [company assets](/handbook/spending-company-money/), such as equipment, inventory, supplies, cash, and information. Treat company assets with the same care you would if they were your own. No employee or contractor may commit theft, fraud or embezzlement, or misuse company property.

### GitLab Internal Acceptable Use Policy

The [Gitlab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/) specifies requirements related to the use of GitLab computing resources and data assets by GitLab team-members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. Our intention in publishing this [policy](/handbook/people-group/acceptable-use-policy/) is not to impose restrictions but outline information security guidelines intended to protect GitLab assets.

### Proper Use of Electronic Media

Our company uses global electronic communications and resources as routine parts of our business activities. It is essential that electronic resources used to perform company business are protected to ensure that these resources are accessible for business purposes and operated in a cost-effective manner, that our company’s reputation is protected, and that we minimize the potential for legal risk.

In addition to following the [Social Media Guidelines](/handbook/marketing/social-media-guidelines/), when utilizing social media think about the effects of statements that you make. Keep in mind that these transmissions are permanent and easily transferable, and can affect our company’s reputation and relationships with team members and customers. When using social media tools like blogs, Facebook, Twitter or wikis, ensure that you do not make comments on behalf of GitLab without proper authorization. Also, you must not disclose our company’s confidential or proprietary information about our business, our suppliers, or our customers.

### Protecting Customer/Third Party Information Privacy

We take the protection of privacy for our customer’s, consumer’s, and other third parties that have entrusted us with information very seriously. Customer or third party information includes any information about a specific customer/third party, including such things as name, address, phone numbers, financial information, etc. Specifically, note that:

* We follow all applicable laws and regulations directed toward privacy and information security. Keeping customer information secure and using it appropriately is a top priority for our company.
* We must safeguard any confidential information customers or third parties share with us.
* We must also ensure that such information is used only for the reasons for which the information was gathered, unless further use is allowed by law.
* We do not disclose any information about a third party without their written approval unless legally required to do so (for example, under a court-issued subpoena).

If you do not have a business reason to access this information, you should not do so. If you do, you must also take steps to protect the information against unauthorized use or release in line with our [Security Best Practices](/handbook/security/).

### Intellectual Property and Protecting IP

Our [intellectual property](/handbook/contracts/#piaa-agreements) is among our most valuable assets. Intellectual property refers to creations of the human mind that are protected by various national laws and international treaties. Intellectual property includes copyrights, patents, trademarks, trade secrets, design rights, logos, expertise, and other intangible industrial or commercial property. We must protect and, when appropriate, enforce our intellectual property rights. We also respect the intellectual property belonging to third parties. It is our policy to not knowingly infringe upon the intellectual property rights of others.

1. Take proper care of any **confidential** information you get from our customers.
2. As an employee or contractor, the things you create for GitLab belong to the company.
   * This work product includes inventions, discoveries, ideas, improvements, software programs, artwork, and works of authorship. This work product is the company’s property (it does not belong to individuals) if it is created or developed, in whole or in part, on company time, as part of your duties or through the use of company resources or information.
3. If you copy code always **check** the license and attribute when needed or appropriate.
4. Check **community contributions** and do not merge it when there can be doubt about the ownership.
5. Only certain authorized individuals may **sign** legal documents such as NDAs. Please refer to our [signature policy](/handbook/finance/authorization-matrix/)
6. View our [DMCA policy](/handbook/dmca) in regards to copyright / intellectual property violations

Assignment of intellectual property is addressed in the [employee and contractor templates](/handbook/contracts/#employee-contractor-agreements), but these may vary from what you agreed to at the time of your contract. For specific information about your obligations regarding intellectual property rights and obligations, please reference your contract.

### Antitrust and Fair Competition

All directors, officers, employees, and contractors must comply with antitrust and competition laws which prohibit collusive or unfair business behavior that restricts free competition. These laws are quite complicated, and failure to adhere to these laws could result in significant penalties imposed on both GitLab and the employees and/or contractors who violated the law.

Unlawful behavior examples:

* entering into agreements with competitors to fix prices, terms of sale or production output
* to engage in bid rigging
* to divide markets or customers
* to attempt to discriminate in prices or terms of sale among our customers
* to otherwise restrict the freedom of our customers to compete
* refusing to deal with certain customers or competitors

Such laws prohibit efforts and actions to restrain or limit competition between companies that otherwise would be competing for business in the marketplace. You must be particularly careful when you interact with any employees, contractors or representatives of GitLab’s competitors, especially at trade association meetings or other industry or trade events where competitors may interact. Under no circumstances should you discuss customers, prospects, pricing, or other business terms with any employees or contractors or representatives of our competitors.

If you are not careful, you could find that you have violated antitrust and competition laws if you discuss or make an agreement with a competitor regarding:

  * Prices or pricing strategy
  * Discounts
  * Terms of our customer relationships
  * Sales policies
  * Marketing plans
  * Customer selection
  * Allocating customers or market areas
  * Contract terms and contracting strategies

Depending on business justification and effect on competition, other practices not involving competitors may also result in civil violations of the antitrust and competition laws. These practices include:

  * Exclusive dealing
  * Bundling / package offerings
  * Resale restrictions
  * Selective discounting

We engage in open and fair procurement activities regardless of nationality or the size of the transaction. Suppliers are selected on a competitive basis based on total value, which includes quality, suitability, performance, service, technology, and price. We strive toward establishing mutually beneficial relationships with our suppliers based on close cooperation and open communication. Terms and conditions defining our relationship with suppliers are communicated early in the supplier selection process. Any agreements to such terms and conditions, or any acceptable modifications, are reached before work begins.

### Honest Advertising and Marketing

It is our responsibility to accurately represent GitLab and our products in our marketing, advertising, and sales materials. Deliberately misleading messages, omissions of important facts or false claims about our products, individuals, competitors or their products, services, or employees or contractors are inconsistent with our values. Sometimes it is necessary to make comparisons between our products and our competitors. When we do, we will make factual and accurate statements that can be easily verified or reasonably relied upon.

### Obtain Competitive Information Fairly

Gathering information about our competitors, often called competitive intelligence, is a legitimate business practice. Doing so helps us stay competitive in the marketplace; however, we must never use any illegal or unethical means to get information about other companies.

Legitimate sources of competitive information include:

  * publicly available information such as news accounts
  * industry surveys
  * competitors' displays at conferences and trade shows
  * information publicly available on the Internet
  * from customers and suppliers (unless they are prohibited from sharing the information)
  * by obtaining a license to use the information or actually purchasing the ownership of the information

When working with consultants, vendors, and other partners, ensure that they understand and follow GitLab policy on gathering competitive information.

### Anti-Money Laundering

Money laundering is a global problem with far-reaching and serious consequences. Money laundering is defined as the process of converting illegal proceeds so that funds are made to appear legitimate, and it is not limited to cash transactions.

Complex commercial transactions may hide financing for criminal activity such as terrorism, illegal narcotics trade, bribery, and fraud. Involvement in such activities undermines our integrity, damages our reputation and can expose GitLab and individuals to severe sanctions.

Our company forbids knowingly engaging in transactions that facilitate money laundering or result in unlawful diversion. Anti-money laundering laws require transparency of payments and the identity of all parties to transactions. We are committed to full compliance with anti-money laundering laws throughout the world and will conduct business only with reputable customers involved in legitimate business activities and transactions.

### Selection and Use of Third Parties/Procurement (Fair Purchasing)

We believe in doing business with third parties that embrace and demonstrate high principles of ethical business behavior. We rely on suppliers, contractors, and consultants to help us accomplish our goals. They are part of the GitLab team and should be treated according to our values. To create an environment where our suppliers and consultants have an incentive to work with GitLab, they must be confident that they will be treated in an ethical manner. We offer fair opportunities for prospective third parties to compete for our business. The manner in which we select our suppliers and the character of the suppliers we select reflect on the way we conduct business.

### Anti-corruption / Anti-bribery

Globally, many countries have laws that prohibit bribery, kickbacks, and other improper payments. No GitLab employee, contractor, officer, agent, or vendor acting on our behalf may offer or provide bribes or other improper benefits in order to obtain business or an unfair advantage. You must avoid participating in commercial bribery and kickbacks, or even the appearance of it, in all of our business dealings. Even in locations where such activity may not technically be illegal, it is absolutely prohibited by our company policy.

**Definitions**

1. Commercial bribery involves a situation where something of value is given to a current or prospective business partner with the intent to obtain business or influence a business decision.
1. Kickbacks are agreements to return a sum of money to another party in exchange for making or arranging a business transaction.
1. A bribe is defined as directly or indirectly offering "anything of value" to influence or induce action, or to secure an improper advantage.
1. "Anything of value" is very broadly defined and can include such things as:
   * Cash
   * Gifts
   * Meals
   * Entertainment
   * Travel and lodging
   * Personal services
   * Charitable donations
   * Business opportunities
   * Favors
   * Offers of employment

**Situations**

1. No employee or contractor shall make or promise to make, directly or indirectly, any payment of money or object of value to any foreign official of a government, political party, or a candidate for political office for the purpose of inducing or influencing actions in any way to assist our company in obtaining or retaining business for or with GitLab.
1. The exchange of appropriate gifts and entertainment is often a way to build our business relationships. However, you must conduct business with customers, suppliers, and government agencies (including U.S. and non-U.S. governments) without giving or accepting bribes including (but not limited to) commercial bribery and kickbacks.

### Gifts and Entertainment

Modest gifts, favors, and entertainment are often used to strengthen business relationships. However, no gift, favor, or entertainment should be accepted or given if it obligates, or appears to obligate, the recipient, or if it might be perceived as an attempt to influence fair judgment.

In general, unless you have supervisory approval you should not provide any gift or entertainment to customers, suppliers, or others that you would not be able to accept from a customer, supplier, or other applicable parties.

All directors, executives, and anyone else in the company participating in vendor selection, must disclose all gifts and entertainment valuing over US$250 for the six months prior to the vendor selection and during the term of the services and for a period of twelve months after services have been completed. The disclosure shall be made to the Legal department, and shall include the value of the gift or entertainment, the individual or company providing the gift, favor, or entertainment, and the date on which it was received. If you have any questions relating to this section, feel free to contact the Legal department.

### Trade Compliance (Export/Import Control)

GitLab is not to engage in activities that could threaten foreign policy and national security interests of the United States or other countries in which GitLab has facilities, international peace and security by distribution of products to end-users that could violate applicable export control laws.  This Trade Compliance Policy defines the security transaction controls and control procedures to be developed and implemented at GitLab.

**What?**

*An export is the transfer of goods, technology, technical data and software transfer to a foreign country or a foreign national.  Be advised that “export” includes downloads, electronic access to technology, emails and even visual reviews.*

**Goods, Technology, Technical Data & Software Transfers**

Goods, technology, technical data & software transfers may be subject to Export Administration Regulations (the “EAR”) and also to the export control laws of other countries in which GitLab operates.  For example, exports by GitLab from Canada would be subject to the EAR and also to Canada’s Export and Import Permits Act and the Canadian Export Control List (the “ECL”).  For export purposes, technology and technical data includes any information that can be used or adapted for use in the design, development, manufacture, utilization, or reconstruction of products or commodities. This can include more than tangible items.  For example, intangible items such as technical services, presentations, product demonstrations and software would be included in technology and technical information for the purposes of export control laws.

For the purposes of the EAR, an export of technology, technical data, and/or software is defined as the release of technology or software subject to the EAR, released in a foreign country and/or any release of technology or source codes (does not include object codes) subject to the EAR to a foreign national.   Release to foreign nationals is deemed to be an export to the home country or countries of the foreign national (a “Deemed Export”).  Some other countries, such as Canada, do not control Deemed Exports under their export control laws, however, US export control laws will still apply to Deemed Exports within those countries.

A Deemed Export does not apply to persons lawfully admitted for permanent residence in the United States (“green card” holders) and does not apply to persons who are protected under the Immigration and Naturalization Act.

It is possible that a license may have to be obtained prior to visits with foreign nationals or discussions of technical information with foreign nations, whether these occur at US facilities or in other countries.

**Release for Export includes:**
> Visual inspection by foreign nationals of U.S. equipment and facilities such as demonstrations and on-site training;
> Oral and written exchanges of information in the U.S. or abroad.  This includes exchange or transmission of information through any media such as facsimile, email, internet, intranet, telephone, downloading, or causing the downloading, of software to locations outside the U.S. or to foreign nationals.
> Applications of situations abroad of personal knowledge or technical experience acquired in the U.S.; or
> Downloading or actual physical shipment of the technology or software.
Similar rules apply in other countries in respect to what is or is not considered to be an export.

**NOT subject to Export Administration Regulation (EAR) Control**
The following technology, technical data and software are not subject to and are outside the scope of the EAR. However, other policy controls and restrictions may apply:
>Technology, technical data and software that is publicly available, meaning published in periodicals, books, print, or electronic media that is available to the public at a price that does not exceed the cost of reproduction or distribution;
>Data that is readily available at universities or other public libraries;
>All patent information available at any patent office;
>All technical data presented at open conferences, meetings, seminars or trade shows, provided the gathering is open to all technically qualified members of the public and that attendees are permitted to take notes or otherwise make a record of the proceedings;
>Technology, technical data or software arising during, or resulting from, fundamental research (as described in the EAR); or
>Technology, technical data or software that is educational in nature (as described in the EAR).

Other countries have similar exemptions, however, the specific exemptions for each country should be reviewed if technology, technical data or software is being exported from those countries to make sure that the exemptions recognized under U.S. law are also recognized under the law of the applicable country from which an export is being considered.

Release of information into the public domain is a conscious decision on the part of GitLab. In many cases, technology (in the form of product data sheets or technical specifications) has been released through distribution at public trade shows. Technology in the form of detailed drawings for production or manufacturing procedures is not public domain data.

**How?**

*Items are given special classification numbers (ECCNs). These numbers help determine whether a license is required.  To determine an ECCN, you must know the ECCNs of the technology that was included in the overall export.  Items that are EAR99 are the easiest to export. That said, the addition of a highly-regulated piece of technology can change the overall ECCN of the entire product.  Other countries have their own classification systems that specify what is and is not controlled under their export control laws.  When obtaining new good, software or technology from third party – make sure you know the ECCN or if an ECCN is not available, the applicable foreign law classification for the applicable good, software or technology.*

**Company Type Classification**
GitLab’s current product Export Control Classification Number is  5D992.c. (Contact the Legal Department for a copy of letter.)

**New Projects/Products/Tenders**
The relevant business unit will work with The Legal Department to determine the export classification of new software as soon as possible during the project. Please note that changes to the encryption functionality may require a new export classification.  This will usually occur after feature scope is determined and the origin of technologies to be used in the project is known. If an applicable product is going to be exported from GitLab’s facilities outside of the U.S., then an export classification will also need to be done for that country.

**Ongoing Surveillance of Export Classification**
All Product Classification sheets will be reviewed annually to ensure the export classification of the software has not changed, if GitLab is still exporting that software.  GitLab will also monitor regulation changes that might affect the classification of GitLab products or other items that could be exported.

**Standard Terms and Conditions of Sale**
Our standard terms and conditions of sale contain text substantially similar to the following:
*Exports and U.S. Government Rights.  The Products furnished to you may be subject to export and other restrictions under the laws and regulations of the United States of America and other countries.  You hereby agree that you shall not transfer, export or re-export, directly or indirectly, any Product or technical data received from GitLab to any destination or entity subject to export or other restrictions under the laws and regulations of the United States of America or any other applicable countries unless prior written authorization is obtained from GitLab and the appropriate United States and applicable foreign agencies.  The Products are provided with Restricted Rights.  Use, duplication or disclosure by the U.S. government is subject to restrictions as set forth in (a) this Agreement pursuant to DFARs 227.7202-3(a); (b) subparagraph (c)(1)(i) of the Rights in Technical Data and Computer Software clause at DFARs 252.227-7013; or (c) the Commercial Computer Software Restricted Rights clause at FAR 52.227-110 subdivision (c)(1) and (2), as applicable. *

**Where?**

*Some countries (and nationals of those countries) are prohibited from accessing US Technology.*

*Countries prohibited from receiving any technology include:  Cuba, Iran, North Korea, Sudan, Syria and the Crimean Region of the Ukraine.*

*US Government Classified Information must only be accessible by US Citizens who have appropriate clearance and a need to know.   If you know or believe GitLab is in receipt of US or Foreign Government Classified Information, please contact Legal or Compliance.*

*Additionally, companies cannot agree to boycott any other country.  If you receive any request to boycott a country, please notify Legal.*

**Embargoed Countries**
The United States restricts imports and exports to certain destination without a specific authorization from the government.   Products exported to an entity within an embargoed country or to a foreign national of that country, e.g., **Cuba, Iran, North Korea, Sudan, Syria, and the Crimean Region of the Ukraine** will require a license, even if the shipment is through a reseller or via another country. Some countries have legislation that prohibits the imposition of U.S. embargo requirements on individuals and companies in those countries (“Blocking Legislation”).

**U.S. Anti-Boycott**
U.S. Anti-Boycott regulations exist to counteract foreign economic boycotts that are at odds with U.S. policy. It is GitLab's policy to comply with the requirements of Anti-Boycott regulations. Actions prohibited by the regulations include:
> Refusing (or agreeing to refuse) to do business with entities prohibited under a foreign boycott.
> Discriminating against a person based on race, religion, sex, national origin, or nationality condemned by a foreign boycott.
> Furnishing information that would assist in furthering a foreign boycott.
> Implementing letters of credit that include prohibited boycott terms or conditions.

Employees encountering requests or issues to avoid certain countries, nationals or protected individuals should contact Legal immediately. Under certain circumstances, the anti-boycott laws may require filing a report with the U.S. Department of Commerce regarding the request to participate in the boycott even if the employee correctly refuses to participate.  Other countries have similar anti-boycott provisions.

**Who?**

*Some people have been prohibited from receiving technology by various government agencies.  To ensure that a particular person or entity is not prohibited, a Denied Party Screen must be completed before any export occurs.  Companies with whom GitLab does business, must undergo a due diligence review which includes a Denied Party Screen, valid contract and background check of the entity.*

**Due Diligence**
If there are any suspicions about any particular customer, reseller, or order, they should be discussed with the Export Compliance Officer or Legal.

There must be sufficient information provided about a customer to ensure a proper Denied Party Screen can be completed.  If this information is missing, the Sales person shall contact the reseller or the end-user to ask them to provide the requisite information.

To ensure traceability the individual performing the Denied Party Screen shall record the end-user site details for all approved orders received directly from end-users as well as any resellers. It is important to always be assured that the end-user and the end-use is legitimate.

**Denied Party Screens**
*Prior to the release of technology to a foreign country or foreign national*, sales operations, order processing, shipping, engineering and/or licensing shall ensure that the receiving person or entity is not on an Export Control Watch List by conducting a Denied Party Screen.  If an export is being done from a country other than the U.S., then the denied parties list for that country will need to be checked in addition to the US denied persons screening. If any of the customers are on any of the Export Control Watch Lists, the Transaction will be escalated to the Export Control Officer who will work with the relevant departments and government agencies to determine if the delivery is allowed. It is important to note that products that are being shipped to an entity on the Export Control Watch Lists will require a license, even if the shipment is through a reseller or via another country. The outcome of this Legal determination will be binding. The completion of this check will be recorded in the appropriate records and in the appropriate customer record in GitLab’s salesforce.com system.

**Why?**

*In some cases, an End Use Certificate may be required from the End User of a product to confirm legitimate uses of the product being exported. If you are suspicious of the intentions of a purchaser, consult Legal.*

**End Use**
GitLab's Products are designed and intended for commercial use.  In the event that GitLab knows or had reason to know that a customer seeks to use GitLab products for improper or nefarious purposes, please contact Legal so that the appropriate End Use statements can be obtained.

If you have any questions or concerns about anything herein, please contact Compliance@gitlab.com.


### Government Customers/Contracting

We must ensure all statements and representation to government procurement officials are accurate and truthful, including costs and other financial data. If your assignment directly involves the government or if you are responsible for someone working with the government on behalf of GitLab, be alert to the special rules and regulations applicable to our government customers. Please review **[GitLab's Public Sector Rules of Engagement](/handbook/sales/public-sector/)** for more details on dealing with the US Federal Government.  Please note that government agencies, in general (US or otherwise) have more strenuous requirements than traditional customers.  Be aware of the heightened sensitivity around dealing with any government entity or foreign official.

Any conduct that could appear improper should be avoided when dealing with government officials and employees or contractors. Payments, gifts, or other favors given to a government official or employee are strictly prohibited as it may appear to be a means of influence or a bribe. Failure to avoid these activities may expose the government agency, the government employee, our company, and you to substantial fines and penalties.

### Record Retention Policy

This Records Retention Policy promotes and assists with the implementation of procedures, best practices, and tools to promote consistent life cycle management of GitLab records.  Because records contain important information, it is essential to take a systematic approach to the management of records.

**1. Records Management**
Records management addresses the life cycle of records, i.e., the period of time the records are in the possession of GitLab.
The life cycle usually consists of three stages: 1) Creation or receipt; 2) Maintenance or use; and 3) Destruction or retention.  This policy is used in conjunction with the Records Retention Schedule.
Records retention does not mean permanent retention.  There are certain situations where documents must be preserved permanently, but most records need only be retained for a specified period of time.  When the retention period for a particular record expires, we should destroy or delete the record.  However, if we destroy or delete a record before its retention period expires, GitLab may be subject to penalties and sanctions.
Unnecessary retention of records creates significant burdens for GitLab.  For example, unnecessary records take up valuable storage space and make retrieval of needed records more difficult and costly.  Consequently, GitLab recommends destruction of records after the retention period expires, except in those instances where GitLab directs otherwise (such as when a Litigation Hold is issued by the Legal Department).  In other words, GitLab requires adherence to the records retention requirements and recommends the destruction of records according to the Retention Schedule.

**2. The Difference between a “Record” and “Non-Record”**
GitLab Records Retention Policy is based on a fundamental distinction between a “record” and a “non-record.”  GitLab Records Retention Schedule applies only to “records.”  Therefore, it is important to understand the difference between a “record” and a “non-record.”

*2.1	What is a “record”?*
A “record” is a Document created or received by a GitLab entity or individual in the transaction of business that contains significant business value.  It is maintained to memorialize GitLab’s policies, procedures, functions, decisions, operations, processes or other business activities.  The business value of a record is derived from the legal, regulatory, fiscal, operational, or informational significance of the content.
“Records” must be retained throughout their life cycle in accordance with GitLab Records Retention Schedule.
“Document” as used above is a piece of written, printed or electronic matter that provides information or evidence or otherwise serves as an official record and can include software application files, paper, slide presentations, spreadsheets, CAD drawings, electronic images, pictures, emails, faxes, and the like.

*2.2	What is a “non-record”?*
Simply stated, a “non-record” is everything that does not qualify as a “record.”  Non-records are materials that GitLab does not own, materials without substantive business value, or materials that have only temporary use as reference or reminders.  They are not subject to GitLab Records Retention Schedule and should be discarded when no longer needed.
Examples of non-records include:  Correspondence that is only needed for a short period of time and does not have substantive business value; Calendars; Informational items such as publications and extra copies of documents kept only for convenience or reference; personal or casual correspondence, Reminders and ticklers that are needed only for a short period of time and do not have substantive business value.  This would include things like reminders, “post-it” notes or superseded drafts.

*2.3	The difference between an original record and a copy*
The original record represents GitLab’s official version of any information asset.  Original records may be created by a GitLab employee, received by GitLab from an outside source, or may be forms processed by a GitLab department.
A copy of a record is typically a duplicate that was created for dissemination or handy reference.  There is no obligation to retain copies.  Copies may be disposed of when they are no longer needed.

**3.  Responsibility for Developing and Managing Records**
GitLab employees are responsible for making and keeping records of their work.  In that regard, they have four basic obligations:
Create records needed to do the business of GitLab, record decisions on actions taken, and document activities for which they are responsible.

Maintain records in a legible and readily identifiable format.

Take care of records so that information can be found when needed.  This means setting up adequate directories and files, and filing materials regularly and carefully in a manner that allows them to be safely stored and efficiently retrieved when necessary.

Carry out the destruction or retention of records under their control; This includes transferring records to storage or destruction of records in accordance with GitLab Records Retention Schedule and verifying records are destroyed in a manner conducive to prevent the release of sensitive information into the public domain (i.e. erasing, shredding, etc.)
The individual or department that maintains the official version of a record is responsible for managing the record.  This is known as the Record Owner.  The Record Owner is responsible for the use, retention, and destruction of that record, unless and until a record is officially transferred to a designated archive or storage center.

**4.  Management of Electronic Records**
The Record Owner ensures that electronically-generated records are identified, and the systems used to store the records comply with this Records Retention Policy and Records Retention Schedule.  Records may be managed in either a paper format or an electronic format.  Electronic recordkeeping systems must be designed to ensure the security and integrity of the records, preservation of records for the time when they are needed, and migration of data to other GitLab systems or subsequent systems.  Consequently, GitLab records should not be maintained solely on the personal hard drive, flash drive, or directories of a GitLab employee assigned only for that person’s use. GitLab employees should create an electronic filing system located in the appropriate server for their department or function, with specific folders for each employee and for each matter, and transfer their GitLab records to those folders, or at least copy them to those folders, as soon as possible after those records are created.

*4.1	Emails and Slack*
Much of the business conducted today is done by email and Slack.  GitLab has separate policies with regard the use of GitLab managed messages and attachments, which covers such things as communication etiquette, prohibited use and content of communications, password and encryption key security and integrity, GitLab’s right to access information, confidential GitLab information, privileged communications and personal use of email.
Email and Slack communications are subject to the same records retention procedures that apply to other documents and must be retained in accordance with GitLab Records Retention Schedule.  The following guidelines apply to emails and Slack:

The status of any particular communication (i.e., whether it is a record and its retention period) is determined by its content, not the method of delivery.

The responsibility of maintaining and retaining an internally created and distributed document most often falls on the author, not the recipient.

GitLab employees who receive emails from outside GitLab are responsible for proper management of those communications.  In an effort to be environmentally conscious, the printing of emails for record retention purposes discouraged.

GitLab strongly discourages the storage of voluminous non-record email messages because of the limited value of such messages, and the cost and confusion associated with unmanaged retention and proliferation of email.  Unmanaged retention of messages fills up storage space on the network servers, extends the amount of time to backup data, requires additional resources to maintain, slows performance, and can potentially lead to an inaccurate history of GitLab because it is haphazard and incomplete.  As a general rule, employees are encouraged to promptly delete any non-record email messages they send or receive that no longer require action and are no longer necessary to GitLab’s business.  If a non-record email no longer has business value, it should be deleted.

**5.  Retention Period**
The retention periods for specific types of records are set forth in GitLab Records Retention Schedule.  The Records Retention Schedule will ensure that records are safely preserved until the retention period has expired.  GitLab recommends that the records be destroyed in the normal course of business in accordance with the Records Retention Policy and Records Retention Schedule.  In some instances, GitLab may choose to permanently preserve information for business or historical reasons.  Such records are specifically designated in the Records Retention Schedule.

**6.  GitLab Records Retention Schedule**
[GitLab Records Retention Schedule](https://docs.google.com/spreadsheets/d/1EbyOSQvjiT4KJGB0_evhJEfKlvp2qKaJJbyzknCEPzA/edit?usp=sharing) establishes the retention periods for various types of records.  It applies to all records, regardless of format.  Retention of records according to the Records Retention Schedule must be observed to ensure consistent application of reasonable due diligence in recordkeeping practices.

GitLab’s Records Retention Program promotes the destruction of records and other documents at the earliest possible time, in accordance with the following principles:

All records should be promptly destroyed pursuant to GitLab Records Retention Schedule.  As discussed above, "non-record” materials are not subject to the Records Retention Schedule and should be discarded when no longer needed.

The retention periods in the Records Retention Schedule applies to the "original" version, or official record, of each information asset.  Copies of records should be destroyed when they are no longer needed.

All Record Control processes will contain documented procedures clearly defining methods for identifying, storing, protecting and retrieving Records.

Identification methods should include a recommended destruction date and a mechanism for efficient retrieval of the information.

Storage will be in such a form as to preserve the integrity of the data and prevent eroding, tampering or altering the data.

Protection of the stored information will be in such a form as to ensure the integrity of the storage mechanism.

Retention will be, at a minimum, for the time period set forth in the Retention Schedule.

Records will be properly disposed of once the respective retention period has lapsed.

Disposal of the information will be in such a form as to protect the sensitive nature of the information therein and prevent release into the public domain.

Litigation Holds or other non-destruct directives issued by GitLab’s Legal Department override any destruction authority granted by the Records Retention Schedule.  In addition, destruction of non-record materials identified in the Litigation Hold is suspended.

When the decision is made to destroy GitLab information, it must be destroyed/erased in a secure and confidential manner that is appropriate to the sensitivity of the informational content.
Employees will consult the Data Classification and Handling Procedures for details on retention, destruction and storage of all Records.

**7.  Litigation Holds**
GitLab is occasionally the subject of threatened or pending litigation or administrative proceedings.  Such proceedings may suspend the normal operation of GitLab Records Retention Policy.  Where appropriate, GitLab will issue a “Litigation Hold” to certain employees, departments or the entire GitLab.  The Litigation Hold will provide a description of records and other documents that must be preserved.  The failure to preserve records, documents and other evidence could subject GitLab to fines or sanctions.  It is therefore very important to manage documents and other evidence in strict accordance with the Litigation Hold.

**8.  Special Situations**

*8.1	Mergers and acquisitions*
Documents that may come into the possession of GitLab through the acquisition of another business entity, or through mergers or other agreements, are subject to GitLab Records Retention Policy.  Such documents will be promptly screened and managed in accordance with the Policy directives.

*8.2	Consolidations*
In the event of consolidations, such as a facility closure, GitLab will continue to manage records associated with such facilities in accordance with GitLab Records Retention Policy.  The following guidelines apply:
All records should be transferred to the new Record Owner, or as directed by GitLab.  Legal Department must be informed of the identity of the new Record Owner and provided a description of the records transferred.
If a successor Record Owner is not identified, GitLab records will be transferred to the department or office most closely associated with the record’s business function.  For instance, personnel files without new Record Owners must be forwarded to the Corporate HR Department.
All other records must continue to be managed in accordance with GitLab Records Retention Policy.  To ensure this, Legal Department must be involved in all decisions concerning the destruction or transfer of such records.

*8.3	Employee Moves (Office Moves, Reassignments and Termination of Employment)*
Employees are responsible to follow GitLab’s Policy for ensuring that records are properly maintained.  When an employee’s duties are being reassigned or the relationship with the employee is terminated, each employee should conduct an inventory of records and other materials in his or her possession and follow the steps listed below:
If the employee has any records that belong in department files, follow the procedures for ensuring that those records are properly stored in the appropriate location.
Dispose of non-record materials as soon as the employee no longer needs them.
Remove or destroy any personal materials.  Employees are reminded that materials relating to their jobs cannot be removed without GitLab’s permission.
Identify active records that are needed for current business and arrange for their transfer to the new Record Owner or as directed.
The supervisor of the former employee whose employment was terminated is responsible for compliance with the steps described above.

*8.4	Contractual obligations*
In some instances, GitLab may be required to maintain certain types of records or other types of information for designated periods of time pursuant to its contractual obligations with third parties.  To the extent agreed to by GitLab, those contractual obligations must be honored.  If there is a conflict between the Record Retention Schedule and a contract, the longer of the two terms shall prevail unless otherwise prevented by law.

**9.  Communications**
GitLab Legal is responsible for implementing and managing the Records Retention Policy.


### Maintain Accurate Financial Records / Internal Accounting Controls

Accurate and reliable records are crucial to our business. Records will be maintained accurately to:

* ensure legal and ethical business practices
* prevent fraudulent activities
* ensure that the information we record, process, and analyze is accurate, and recorded in accordance with applicable legal or accounting principles
* ensure that it is made secure and readily available to those with a need to know the information on a timely basis

GitLab records include:

* booking information
* payroll
* timecards
* travel and expense reports
* e-mails
* accounting and financial data
* measurement and performance records
* electronic data files
* all other records maintained in the ordinary course of our business

There is never a reason to make false or misleading entries. Undisclosed or unrecorded funds, payments, or receipts are inconsistent with our business practices and are prohibited.

### Manage Records Properly

Our records are our corporate memory, providing evidence of actions and decisions and containing data and information critical to the continuity of our business.

Records consist of all forms of information created or received by GitLab, whether originals or copies, regardless of media. Examples of company records include:

* paper documents
* e-mail
* electronic files stored on disk
* tape or any other medium (CD, DVD, USB data storage devices, etc.) that contains information about our company or our business activities

We are responsible for properly labeling and carefully handling confidential, sensitive, and proprietary information and securing it when not in use. We do not destroy official company documents or records before the retention time expires, but do destroy documents when they no longer have useful business purpose.

### Avoiding Conflicts of Interest

We have an obligation to make sound business decisions in the best interests of GitLab without the influence of personal interests or gain. Our company requires you to avoid any conflict, or even the appearance of a conflict, between your personal interests and the interests of our company.

A conflict exists when your interests, duties, obligations or activities, or those of a family member are, or appear to be, in conflict or incompatible with the interests of GitLab. Conflicts of interest expose our personal judgment and that of our company to increased scrutiny and criticism and can undermine our credibility and the trust that others place in us.

Should any business or personal conflict of interest arise, or even appear to arise, you should [disclose it immediately to leadership for review](/handbook/contracts/#approval-for-outside-projects). In some instances, disclosure may not be sufficient and we may require that the conduct be stopped or that actions taken be reversed where possible. As it is impossible to describe every potential conflict, we rely on you to exercise sound judgment, to seek advice when appropriate, and to adhere to the highest standards of integrity.

**GitLab is often required to disclose any organizational or personal conflict of interest to various third parties.  To ensure accuracy in our disclosures, please notify Compliance at Compliance@gitlab.com of any organizational or personal conflicts of interest.**

### Communicating with External Parties

GitLab employees and contractors are not authorized to speak with the media, investors, or analysts on behalf of our company unless authorized by our Marketing department. Unless authorized, do not give the impression that you are speaking on behalf of GitLab in any communication that may become public. This includes posts to online forums, social media sites, blogs, chat rooms, and bulletin boards. This policy also applies to comments to journalists about specific matters that relate to our businesses, as well as letters to the editor and endorsements of products or services.

### Personal Hygiene

When attending Contribute or any conference, public meeting, customer meeting or meet-up, kindly keep in mind you are representing GitLab. Personal hygiene and hygiene in general helps to maintain health and prevent the spread of diseases and various other illnesses. We motivate everyone to maintain cleanliness.
For more information about our Contribute Code of Conduct, read more [here](/company/culture/contribute/coc/).

### Social Responsibility

We pride ourselves on being a company that operates with integrity, makes good choices, and does the right thing in every aspect of our business. We will continually challenge ourselves to define what being a responsible company means to us, and work to translate our definition into behavior and improvements at GitLab. We seek to align our social and environmental efforts with our business goals and continue to develop both qualitative and quantitative metrics to assess our progress.

### Political Activities and Contributions

You may support the political process through personal contributions or by volunteering your personal time to the candidates or organizations of your choice. These activities, however, must not be conducted on company time or involve the use of any company resources. You may not make or commit to political contributions on behalf of GitLab.

### Charitable Contributions

We support community development throughout the world. GitLab employees or contractors may contribute to these efforts, or may choose to contribute to organizations of their own choice. However, as with political activities, you may not use company resources to personally support charitable or other non-profit institutions not specifically sanctioned or supported by our company. You should consult the Legal department if you have questions about permissible use of company resources.

### Human Rights

We are committed to upholding fundamental human rights and believe that all human beings around the world should be treated with dignity, fairness, and respect. Our company will only engage suppliers and direct contractors who demonstrate a serious commitment to the health and safety of their workers, and operate in compliance with human rights laws. GitLab does not use or condone the use of slave labor or human trafficking, denounces any degrading treatment of individuals or unsafe working condition, and supports our products being free of conflict minerals.

### Anti-Slavery and Anti-Human Trafficking Policy

1. Slavery and Human Trafficking are crimes and violations of fundamental human rights. These violations take various forms, such as slavery, servitude, forced and compulsory labour, and/or human trafficking, all of which have in common the deprivation of a person’s liberty by another in order to exploit them for personal or commercial gain. GitLab is committed to acting ethically and with integrity in our business dealings and relationships by implementing and enforcing systems/controls to ensure modern slavery or human trafficking are not taking place in our business, or with those with whom we do business.

2. GitLab is also committed to ensuring there is transparency in our business and in our approach to tackling slavery and human trafficking throughout our supply chains and overall organization, consistent with disclosure obligations we may have under applicable law. To that end, we prohibit the use of forced, compulsory or trafficked labor, or anyone held in slavery or servitude, whether adults or children by anyone working for or with GitLab.

3. All employees, directors, officers, agents, interns, vendors, distributors, resellers, contractors, external consultants, third-party representatives and business partners are expected to comply with this policy.

4. Every Team Member is responsible to assist in the prevention, detection and reporting of slavery and human trafficking by those working for or with GitLab.  Each Team Member is encouraged to raise concerns about any known or suspected incidents of slavery or human trafficking in any parts of our business or supply chains at the earliest possible stage.   If you are unsure about whether a particular act, the treatment of workers more generally, or their working conditions within any tier of our supply chains or business partners constitutes any of the various forms of modern slavery/human trafficking, raise it at Compliance@Gitlab.com.

5. We may terminate our relationship with individuals and/or Business Partners if they breach this policy.

## Code of Business Conduct & Ethics Acknowledgment Form

Team members will review and sign the [Code of Business Conduct & Ethics Acknowledgment Form](https://docs.google.com/document/d/1ehxrg-ieUEx1ARZv4LpysMfxzFmnhgtzmJn1-299g_E/edit?usp=sharing) during onboarding as well as annually during the [Global Compensation Annual Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) cycle. Please access the form template by clicking [here](https://docs.google.com/document/d/1ehxrg-ieUEx1ARZv4LpysMfxzFmnhgtzmJn1-299g_E/edit?usp=sharing).

During the annual merit cycle, people operations will be responsible for sending all team members the updated Code of Business Conduct & Ethics acknowledgment form and ensuring all team members review and acknowledge. The Code of Conduct will be sent to each team member via BambooHR. To review and sign the document: log into BambooHR download the PDF and click on the Code of Conduct Link. Once a team member has reviewed the Code of Conduct they will use the BambooHR signature and date tool to complete the acknowledgement.  No changes should be made to the Code of Business Conduct & Ethics without prior approval from the VP of Legal.

Team members that joined prior to the established requirement to upload the acknowledgement at hire are the exception but will be subject to capturing their acknowledgement during annual reviews.

