---
layout: handbook-page-toc
title: "GitLab Giveaways"
---

## What is a Swag Giveaway?

GitLab Swag Giveaways are an opportunity to share GitLab Swag with members of our wider community. During a giveaway, unique redemtion links are sent to community members where they can select and directly ship themselves swag.

Examples of when a GitLab teams might host a giveaway could include:
*  Recognition of 5 contest winners
*  Completion of a survey by the first 300 participants
*  Participiation in a challenge

GitLab Giveaways are meant to serve as recognition and celebration of a specific group within our community. If you're looking to recognize an individual community member for their contributions, please use our [Nomination for Community Swag process](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/community-rewards-internal/).

### Notes on Giveaway Processing Iterations

Please note that in the past, GitLab has utilized Shopify discount codes for giveways. Use of widespread discount codes for giveways is not sustainable or scalable for the following reasons:

1. High instances of abuse of codes when widely shared
1. Inability to track cost of swag, fulfillment, and shipping costs. All discounts codes come in through out single swag store back end campaign
1. Inability to allocate costs back to relevant teams. 

For these reasons, the community advocates team is working to move all future giveways onto the Printfection platform using their giveaway features. Any giveaways currently using discount codes will be transitioned to this preferred method.

Requests for one-off or small batch shopify discount codes will be handled by the community advocates on a case by case basis.


### How is a printfection giveaway different than a discount code?

Instead of sharing one discount code with a group of people, your team will share a unique link to be redeemed for swag. Review the steps in Phase 3 below to understand the options your team has for sharing giveaway links.


### Example Giveaway Scenarios

Any team at GitLab can use Printfection to execute a swag giveaway to our wider GitLab community. 

Scenarios where you might use a Printfection giveaway:

* Recognize participation in a survey
* Celebrate winning a contest
* Award GitLab team members for reaching a milestone, like an anniversary



## Giveaway Timeline

| Timeline | Action |
| --- | --- |
| T - 6 weeks | If ordering custom swag, you need to place the order at least 6 weeks before you'd like the swag available for shipping. This means you'll need to start the process for ordering and designing new swag BEFORE the 6 week out mark. |
| T - 2 weeks | Ship your custom swag to Printfection. This allows for 1 week for shipping to the warehouse. This is also the best time to communicate with the Community Advocates, for both the warehouse address and to start communication around support for the giveaway |
| T - 1 week | Build giveaway campaigns in Printfection and coordinate with Community Advocates on sending giveaway links |



## Start a Giveaway

The following steps are for GitLab team members who want to give away GitLab swag to the wider GitLab community, customers, or other GitLab team members.

### Phase 1: Determine your budget and campaign/finance tag

1. Understand your budget. Your team is responsible for covering the cost of swag items, shipping, and fulfillment for your giveaway. 
1. Review [Printfection's example shipping and fulfillment costs](https://help.printfection.com/hc/en-us/articles/204467034-Example-shipping-fulfillment-costs) to estimate the total cost of shipping and fulfillment of your swag
1. Obtain a campaign or finace tag for your giveaway. This is **required** to start the giveaway process.

### Phase 2: Source your swag items

Teams have 2 options for sourcing swag for a giveaway.

1. Use available bulk swag:

Current items available for bulk swag use include:
- Neon Tanuki shirts (approx. $7.50 each)
- Purple Tanuki shirts (approx. $7.50 each)
- DevOps Done Write pens (approx. $5 each)
- Orange Tanuki Stickers (approx. $1 each)
- Rainbow Tanuki Stickers (approx. $1 each)


1. Create custom swag:

If you plan to creat custom swag for your giveaway, please follow the guidelines for [ordering new swag](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#ordering-new-swag).


Important things to note about custom swag orders:

- Creating custom swag could incur an additional cost of a customer sourced order with Printfection. If you plan to source swag from a vendor other than Printfection, review the [Printfection documentation regarding receiving fees](https://help.printfection.com/hc/en-us/articles/115002120194-Understanding-customer-sourced-inventory) for an accurate estimate. At the moment, there is not a way to charge this back to your specific campaign, but it is important to understand the cost implications overall and plan to be frugal in your giveaway.
- Once ordered, custom swag will add approx. 6 weeks to your giveaway timeline. This is the average time needed to produce swag and have it arrive/processed at the Pritnfection warehouse. This does **NOT** include time needed by GitLab design team to create or approve swag designs.


### Phase 3: Decide how you will share giveaway links

Your team has 2 options for how to share giveaway order links. Please review this training video overview on giveaway link generation to make an informed decision on the best way to share your giveway. [ADD TRAINING VIDEO LINK]


**Option 1: Preferred** Share unique giveaway links with each qualified participant

Example: 'Thanks for participating in the survey. Here is your unique giveaway link!'

Pros:
- increased ability to monitor who can redeem links for swag
- links can be generated in bulk or one-off using the Printfection chrome extension

Cons:
- manual work required to generate and share links


**Option 2:** Share 1 link to be redeemed a set numnber of times.

Example: 'The first 40 people to redeem this link will get a GitLab shirt!'

Pros:
- sharing only one link cuts down on maual work

Cons:
- high chance of abuse. there is no way to monitor who redeems the link, meaning that if the link is shared outside of the givewawy network, anyone with the link can redeem for swag.
- your team is responsible for the swag, fulfillment, and shipping costs associated with giveaway link abuse.



### Phase 4: Set up your giveaway campaign

1. Open an issue in the [GitLab Merchandise Project](https://gitlab.com/gitlab-com/marketing/community-relations/merchandise/general/-/issues) and tag @community-advocates. Please check back to this handbook section soon for updates on a linked issue template.
1. The community advocates team will review your issue and begin setting up your giveaway in the Printfection campaign


### Phase 5: Share your giveaway links

1. Your team is responsible for distributing giveaway links to qualified participants.


### Phase 6: Close out our giveaway

It's important when your giveaway is over to correctly close out your giveaway in Printfection to avoid additional item storage charges. Work with the community advocate on your giveaway issue to indicate when you are ready to close out your giveaway



## Related Costs

Below are the related costs **in addition to the cost of actual swag items** that you should plan for while planning your giveaway.

### Item Storage in Prinfection

$25 per item, per month.

For example: If you ship 100 hoodies, 2000 stickers, and 1 speaker, the storage cost will be $75 per month. If you ship 1 hoodie, 1 sticker, and 1 speaker, the storage cost will still be $75 per month.

Please note that you must store your items in Printfection for the entire length of your giveaway, meaning the entire time it takes to share codes, fulfill, and ship orders.

At the moment, we do not have a way to charge this cost back to the specific campaign tag for your team. However, it's important that you consider the overall cost of storing your swag items in Printfection when planning your giveaway, and be mindful to keep your giveaway as [frugal](https://about.gitlab.com/handbook/values/#spend-company-money-like-its-your-own) as possible.

### Customer Sourced Orders

There is a receiving fee for shipping and processing swag from a different vendor to the Printfection warehouse. Please review the [Printfection documentation regarding receiving fees](https://help.printfection.com/hc/en-us/articles/115002120194-Understanding-customer-sourced-inventory) for an accurate estimate.

### Fulfillment and Shipping Costs

Giveaway fulfillment and shipping costs varies by item size, cost, and location of shipping. Please review [Printfection's example shipping and fulfillment costs](https://help.printfection.com/hc/en-us/articles/204467034-Example-shipping-fulfillment-costs) for an accurate estimate.





