---
layout: handbook-page-toc
title: "Content"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Content Hack Day

[Content Hack Day](https://gitlab.com/gitlab-com/content-hack-day/blob/master/README.md) is a day dedicated to writing posts for the [GitLab blog](/blog), and all GitLab team members are encouraged to take part. GitLab team members are welcome to brainstorm post ideas in the #content-hack-day channel on Slack or request a Zoom call with content team members to discuss a post.

### Upcoming events

There are currently no upcoming Content Hack Days planned, as we intend to relaunch when the GitLab Unfiltered Blog is launched. You can [follow this issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/726) for progress. When complete, we will announce the next event. 

### Process

GitLab team-members who want to take part can open a new issue in the [Content Hack Day project](https://gitlab.com/gitlab-com/content-hack-day/issues), or pick up an open issue from the [blog posts backlog](https://gitlab.com/gitlab-com/www-gitlab-com/boards?&label_name[]=blog%20post) or any unassigned issue labeled [`content hack day`](https://gitlab.com/gitlab-com/www-gitlab-com/issues?label_name%5B%5D=content+hack+day).

- **Starting from www-gitlab-com project**: If you've chosen an issue from the blog-post backlog, label it `content hack day` and start a merge request for your draft.

- **Starting from the Content Hack Day project**: Once you've chosen an issue to work on from the Content Hack Day, assign to yourself, and start a merge request in [the www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com). Do not create a duplicate issue in the www-gitlab-com project.

**Make sure the merge request and issue are cross-linked**. If you are drafting in a Google doc, include a link to the doc in the MR. The post should be reviewed by a peer before assigning to Rebecca.

In order to be eligible for an individual or team prize, participants must begin working on their post/s on the day of the event, and submit a complete draft (merge request or Google doc in the case of GitLab team-members unfamiliar with creating a blog merge request) by the end of the day on the Friday following the Hack Day (so for the event taking place on 02/08, complete posts need to be submitted by the end of the day on 02/15).

The Monday following the deadline, the managing editor will move all issues with an associated MR to the www-gitlab-com project for tracking. Any issue that doesn't have an associated MR will be not be counted.

#### Publishing

Your post will be reviewed as soon as possible, but please be prepared for some delay! The point of the Hack Day is so that you have a dedicated day for writing and working on posts with your department, but we will stagger publishing your posts throughout the rest of the quarter.

There may be some feedback for you to address after your review; this is a collaborative process and it should hopefully result in the best possible version of your blog post.

A member of the content team will let you know when to expect to see your post live, so you can go forth and share it with your networks.

Please note, it is possible that the [content team](/handbook/marketing/corporate-marketing/content/#team) will determine that your post is a better fit for our audience on LinkedIn or [Medium](https://medium.com/@gitlab). This is to ensure your post gets the best possible exposure and reaches the people who would be most interested in it. We'll notify you when we review your post if that's the case. Don't worry, your contribution will still count towards the prizes!

#### Incentives and rewards

**Team prize**

Criteria: Minimum 50 percent team participation (participation can be in the form of opening issues, submitting MRs, reviewing other team members' blog posts or joining the Zoom call on the day) + the highest proportion of blog posts relative to the team's size.

To be considered for the team prize, all blog posts must be written to the theme:

- How we solved X problem, OR
- How we built X

Prize: An additional [evangelism dinner](/handbook/incentives/#iacv-target-dinner-evangelism-reward)

Tiebreaker: In the unlikely event we have two departments with equal claim to the prize, we will award the prize to the department with the most additional blog posts submitted (i.e. those that are on topics outside of the theme).

**Individual prizes**

- Newbie prize: First-time participant who submits the most complete blog posts.
- Most creative prize: Participant who submits the most creative/unusual/surprising post.
- Viral prize: Participant who submits the post with the most unique views over 10,000 (cut-off is two weeks after publishing; prize will be awarded before next Hack Day).
- **NEW** Wild card prize: Participant who contributes the best improvement to the blog (not a blog post!) or to our YouTube channel. This could be a something that improves the blog's visitor experience, our efficiency, GO WILD. Can you speed up the website's build time? Find a way to automate crawling our team meeting videos for nuggets of delight? Record an awesome demo? We want you to contribute to Hack Day.

Prizes: GitLab noise-canceling headphones OR a GitLab messenger/duffel bag

Note:

- Individuals don't need to write to the theme to qualify, but their posts won't count towards the team prize if they don't.
- We may choose not to award all individual prizes if criteria aren't met.

**Spot prizes**

Swag will be given away randomly throughout the day. Anyone who has joined the Zoom call or been active in the project or the Slack channel is eligible. Names will be entered into a random picker to decide on winners.

Examples of spot prizes: GitLab reusable coffee cup, GitLab water bottle, GitLab speaker, coffee gift card.

**Snack stipend**

GitLab team-members may expense up to US $15 (or equivalent) for snacks and drinks to keep you going throughout the day. Use the category `Meals`.
