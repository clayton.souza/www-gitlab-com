---
layout: handbook-page-toc
title: "Digital definitions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Digital definitions
{:.no_toc}

The purpose of this page is to present definitions for technical jargon and explanations around related technologies. The intended audiences are people who might not be familiar with development tools and methods nor our unique requirements when working on items like [about.gitlab.com](/).

## Layouts

### What is a layout?

A layout is a container where elements can be placed. A layout definition often contains default styles, a document grid, a container for the navigation, a container for the footer, and a schema for page options.

Example: A single column layout on a 1200px wide grid with options for SEO configuration.

### Why are layouts important?

1. Using a layout facilitates updating old pages with new designs.
1. Using a layout facilitates quickly duplicating the functionality and look-and-feel of a page.
1. Using a layout facilitates quickly changing how a page works by switching between layouts.

## Modules / organisms

Related terms: components, fragments, boxes, blocks, organisms, includes, partials.

### What is a module or organism?

1. A module, also known as an organism, is a section where the presentation, goal, and required functionality remains the same, but content can be updated (wording, imagery, links, etc).
1. A module is a block, box, or section of the page, generally kept as small as possible. It's  usually a horizontal slice of layer cake across the page but can also be a chunk of a sidebar.
1. Modules are often made up of many atoms and molecules put together but are organized around a common purpose.
1. Modules often have configurable options to facilitate reuse with different configurations. It might not always be desirable to have a title block for example.

Example: A gated content form.

### Why is it important for a module to be reusable?

1. In order to facilitate updates, the code needs to be reusable. It's not an easy update if you have to build it again.
1. Implementing the same thing over and over again is not an efficient use of resources.
1. In order to keep the codebase clean, navigable, and easy to understand it's important to maintain SSOT.
1. If the same code is implemented several times in several spots then the chance for bugs increases. One of those spots might have a bug where the others don't.
1. Much of what goes into building code is unseen on the page. This includes things like optimizing performance, setting up tracking, preparing assets such as formatting images & videos, building responsive views and layouts, human physiology (fingers on a touchscreen, eyes and perception, etc). Testing and building all of these things takes time, so it's important to reuse and reduce code as much as possible.

### Why is it important for a module to have a single-purpose?

1. In order to facilitate updates, the code needs to be easy to operate.
1. Having a clearly defined purpose for each module enhances the goals of the page and assists with navigation and conversion goals. If a module tries to do 5 things or there are 3 different modules on the page doing the same thing it becomes difficult to manage.
1. An end user might be confused if duplicate modules are on the page.
1. If a module gets too large then it becomes harder to understand the code. Keeping a module single-purpose keeps the module small.
1. Tracking the performance of a module becomes more difficult the more a module changes.
1. When examining from a distance, it's hard to know what module to use if the modules all have several different purposes, sometimes overlapping purposes. "Do I use this module or that one?"

## Atoms

Related terms: components

### What is an atom?

An atom is the smallest reuseable element.

Examples: a button.

### Why are atoms important?

1. Reusable atoms serve to solidify a brand by building a cohesive identity.
1. Atoms facilitate efficiency by creating a kit of parts so we don't have to create the same thing over and over again.
1. Atoms facilitate predictable outcomes by creating constraints around possibilities.

## Molecules

Related terms: fragments, includes, partials

### What is a molecule?

A molecule is conceptually similar to a module, except it's often quite a bit smaller. A molecule combines atoms into a small item with a singular purpose.

Example: A search box is made up of a text input atom and a submit button atom.

### How is a molecule different from a module?

Molecules and modules differ mostly in size and purpose. The singular purpose of a module might be to get someone to sign up for a newsletter. This might include a description, a title, some graphics, margins, padding, and other elements. The singular purpose of an atom would be to gather the email in a text input and submit that with a button. A molecule often lacks the context that a module has.

### Why are molecules important?

1. As mentioned above, modules and molecules are conceptually similar. Molecules benefit from the same things that modules do.

## Templates

### What is a template?

A template is a data-driven reuseable configuration used by certain types of content. A template consists of a layout, a set of modules, a configuration for the modules, and a configuration for the page options.

Example: all blog posts are generated by a template. 

### Why are templates important?

1. Layouts facilitate quickly duplicating setups. A template makes the process even quicker by removing the need to configure the template every time you reuse it.
1. Templates reduce configuration errors by codifying configurations.

## Content management systems

### What is a CMS?

A CMS (content management system) is used to manage the creation and modification of digital content. Usually a CMS has

* An editor interface (WYSIWYG) so content producers don't need to know how to build a website in order to create content.
* A persistence layer that stores data from the editor interface.
* A view generator that combines raw content with templates in order to generate pages.
* (optional) a server that responds directly to user requests and customizes pages based on requests.

### What a CMS isn't

* A CMS doesn't define requirements.
* A CMS doesn't create layouts.
* A CMS doesn't create modules.
* A CMS doesn't define templates.
* A CMS doesn't ensure best practices (though it can encourage them).

## Headless CMS

### What is a headless CMS?

A headless CMS is a CMS that abstracts the business logic away from the display logic, usually via two or more separate interfaces. A content editor doesn't need to interact with the programming and a programmer doesn't need to interact with the content. In a non-headless CMS the two interfaces are intertwined.

![Diagram of a headless CMS](/images/handbook/growth-marketing/what-is-headless.png)

## Statically-generated

Also referred to as SSG (static site generation/generator)

### What is a statically-generated website?

A static web page is delivered to the user's web browser exactly as stored, in contrast to dynamic web pages which are generated on-the-fly by a web application after an end-user request for something such as data modification.

### SSG benefits

* Significantly faster for the end-user in many situations.
* Easier to work with for developers.
* Fewer dependencies on cloud systems.
* Improved security.
* Costs less to host.

### SSG complications

* Dynamic functionality must be performed on the client side.

## Client-side vs server-side

### Client-side

An end-user's web-browser is responsible for client-side tasks. Once a server delivers a document to the end-user, it's up to their computer to modify the document on their computer. Alternately they can make a request for the server to modify the document for them server-side.

### Server-side

Anything not done on an end-user's computer often needs to be processed by an invisible web-server. Once processed the server sends a new version of the page to the end-user or routes them to a new location.
