---
layout: handbook-page-toc
title: "Customer Reference Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Customer reference program at GitLab

The goal of the Customer Reference Program is to provide opportunities for customers to share their story on how GitLab has helped them overcome the challenges, blockers and pain points within their organizations. The Reference Program Managers work as a conduit to help connect customers with opportunities by balancing customer requests with company demands. By providing a single point for outreach to customers, the CRP team prevents customer burnout, sales team burnout, and ensures a diversity of customer stories are shared publicly. 

## Who we are
Team of Experienced Customer Reference Professionals who are focused on building relationships within GitLab and with customers.  
The team gathers and builds information and relationships with the purpose of distilling these impactful insights into action. 

# Strategic driver of the Customer Reference Program
To manage our customer reference relationships like the precious resources they are to the maximum, quantified & qualified benefit for both customers and GitLab. 

**Quick links grid**

| Quick Reference Links |      |
| ----------- | ----------- |
| [Adding new reference customers](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/index.html#process-for-adding-new-reference-customers) | [Requesting a reference for a sales call](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/index.html#requesting-a-reference-customer-to-support-a-sales-call)|
| [Customer Case Studies](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#customer-case-studies) | [Search published case studies by use case, value driver etc](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&state=opened)
| [Customer Reference Collection](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#customer-references-collection) | |
| [Requesting a customer to speak at an event](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event) |    |
| [Peer Reviews](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/peer-reviews/) | [Customer Advisory Board](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/CAB/) |



## Goals of the GitLab Customer Reference Program
- **Credibility** - Reinforce our credibility as an end-to-end DevOps solution partner.
- **Shorten the sales cycle** - Having our advocates involved with sharing their story with potential sales, analysts and the marketplace can help potential sales close sooner
- **Increase Revenue** - A successful Customer Reference Program helps increase revenue by attracting new clients and increasing retention rates.
- **Customer Advocacy** - Encourage our customers to share their success stories with others who are learning about or evaluating GitLab.
- **Align with our customer value drivers** - Our program reflects our customer values drivers of increasing operational efficiencies, delivering better products faster and reducing security and compliance risk.
- **Align with our customer use cases** - Our program supports our [customer use cases](https://about.gitlab.com/handbook/use-cases/) 

## Metrics of the GitLab Customer Reference Program
View information around customer reference pool, keep track of the latest case studies and find out how the Customer Reference team is helping share customer success stories. 
- [Metrics Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/987)


### Customer Reference Types
Some examples of the types of assets we'd use as customer references once we have approval from the customer:
* Logo (The ability to name a company as a customer and use their logo in our marketing materials)
* Live sales reference (both calls and possible in-person)
* Website content
  * Written case studies
  * Blog posts
  * Videos
  * Podcasts
  * Usage quotes
* Event speakers (At industry, third-party and company events)
* References for analysts and press
* Quotes (can be attributable with approval or anonymous)
* Anecdotes (short "snackable" stories, can be attributable with approval or anonymous) Read [GitLab's customer anecdotes](#customer-anecdotes).


### Process for adding new reference customers
   1. Sales team member nominates champion by pushing the **Nominate a Reference** button on the individual contact (champion's name)
   2. Sales team member fills out the GitLab version customer is using, stages the customer is using and any information about the tech stack
   3. CRM team is notified of nomination in SFDC
   4. If additional information is needed, CRM team will outreach to SAL
   5. [Sales enablment video with this process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)

### Sample Interview Questions/topics for Customer joining the Reference Program
    * What led you to GitLab, what problems were you trying to solve?
    * Why did you choose GitLab and what other tools were you using or considering?
    * What has been your experience with GitLab?  
    * How did you make the business case for GitLab and what metrics have you seen improve?
    * What have you heard from GitLab users, what was their adoption curve like?
    * What has been the most unexpected success you have experienced? Adoption rates? Improved speed?

### Requesting a Reference Customer to support a sales call
   1. Sales Team Member selects "Find a reference" button from the selected opportunity Note: Opportunity required to be in stage 3 or later. 
   2. Filter to find the most appropriate reference for your needs. 
   3. Fill out required information and include any customer forms that may be required. 
   4. Hit submit and wait for the approval for the call from the champion's account owner
   5. [Sales enablment video with this process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)

![Reference Edge logo](/images/handbook/marketing/logo_reference_edge.png "Our team uses Reference edge to manage references")

 [Reference Edge Intro Video](https://www.point-of-reference.com/wp-content/uploads/2020/07/WhyWouldntYou-Video.mp4). 

### Customer Reference Request Rules of Engagement (Managing Volume & Frequency of Reference Customer Engagement Requests)
It is critical and the Customer Reference Management team’s responsibility to optimize our reference customer engagements to balance:
* Maximizing the positive impact of both the customer’s and GitLab’s investment of time and effort
* Ensuring continuous, long term customer and GitLab success by avoiding exhausting reference customer relationships<br>

By default, we will request a customer support reference call (sales/analyst relations/public relations) no more than once per quarter per customer. In support of event speaking and/or other participation support (field, alliance, digital, community, etc.), we will request customer support at a maximum of once every two quarters per customer. The CRM ultimately decides if the reference request is suitable for the proposed customer based on a number of factors including but not limited to:
* Past, current, and future planned volume
* Frequency
* Effort required of support requests
* Customer's stated comfortable volume and levels of engagement

### Creating new issue for Customer Reference Program
To create a new issue around the Customer Reference Program, open an issue on the [Customer Reference Program Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program).
Make sure it has the label *Customer Reference Program*. Feel free to add any applicable labels around the request. Assign to Reference Program Manager as needed.

### Customer Case Studies
The most recent customer case studies are found on the [GitLab customer's page.](/customers/)
When we build case studies, we need to have quantifiable metrics and business value to help describe how GitLab helped a customer achieve a significant business result. Our case studies should align with our customer value drivers. Find below a sample of the customer value drivers that we need to find in a good case study.

**Increase Operational Efficiency** 
* Improved collaboration and sharing of best practice; one integrated platform for all developers to use
* Reduction in toolchain complexity
* More frequent deploys - related to cycle time, but easier to measure
* Reduced IT spend - We reduced budget by x. Or we saved $ with GitLab
* Improved efficiency - We shipped more features in the same time…

**Deliver Better Products Faster**
* Improved cycle time - the time it takes for an idea to reach production
* Reduced defects / errors
* Reduced security issues/vulnerability
* Decrease of customer support calls
* Increase revenue, increase in customers and in market share 

**Reduce Security and Compliance Risk**
* Increase in % of on time, on budget releases
* Increase in % of critical security vulnerabilities scanned
* Increase in % of code scanned
* Increase in audit pass rate;  time spent on audits has reduced
* MTTR (Mean time to resolution) of security vulnerabilities has reduced

If a proposed customer case study doesn't have at least one metric to include, then we consider working with the account team to help identify a solid metric before building the case study.

#### Common Interview questions
Interview Questions: (Select the questions we should ask)

**Why GitLab**
- [ ] What insights do you have that might make a good case study today?
- [ ] Describe what your organization does, how the software is used, and how your team helps solve business challenges.
- [ ] What problem were you trying to solve when you were looking at GitLab?
- [ ] What was your process before using GitLab?
- [ ] Why did you choose to replace your current tooling?
- [ ] What were the negative consequences of your previous environment?
- [ ] What would have happened if you hadn't replaced your tooling
- [ ] How was this problem affecting you?
- [ ] What products were you using before using GitLab?
- [ ] What were the required capabilities you were looking for?
- [ ] Why did you choose GitLab?

**What if**
- [ ] What would have happened if you hadn't selected GitLab?

**Feedback on GitLab**
- [ ] How did GitLab solve those problems you were suffering from?
- [ ] Which teams are using GitLab?
- [ ] What do users say about GitLab?
- [ ] How has GitLab's monthly release cycle benefitted your SDLC?
- [ ] Has the regular release cycle supported innovation and collaboration?
- [ ] If your team is not on the most recent version (or within a release or two), why haven't you updated?

**Impact of GitLab**
- [ ] What are the positive business outcomes of introducing GitLab?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] Can you share any larger successes?

**Value Driver 1 - Increasing Operational Efficiencies**
- [ ] Has GitLab simplified workflow?
- [ ] How has GitLab enabled cross-functional relationships?
- [ ] How has it helped modernize architecture?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] What does your current workflow look like?
- [ ] What other tools do you have plugged into GitLab?
- [ ] Can you describe how your deployments look?
- [ ] Does GitLab integrate/plug into your use of cloud resources?

**Value Driver 2 - Deliver Better Products Faster**
- [ ] Are your processes and tools now more streamlined? Can you describe what the process looks like now?
- [ ] How does having control and visibility of the projects help with planning and governance? 
- [ ] Do your developer teams feel more connected and empowered?
- [ ] How has detecting bugs earlier in the lifecycle impacted the output?

**Value Driver 3 - Reduce Security and Compliance Risk**
- [ ] Are projects delivering on time and on budget?
- [ ] How has the additional security testing impacted your projects?
- [ ] Are audits quicker and seamless now?
 
**Impact metrics of using GitLab**
- [ ] Do you have any metrics available?
- [ ] How has GitLab impacted your cycle time?
- [ ] Have you measured throughput/Lead time? Can you give a before and after?
- [ ] What does your deployment frequency look like?
- [ ] Has there been an impact on change failure rate?
- [ ] Is your MTTR (mean time to resolution) improved?
- [ ] What percentage of your releases are now on time and on budget?
- [ ] Are you catching bugs earlier? Are you catching bugs that would have previously been missed?
- [ ] What percentage of your code is now scanned?
- [ ] What percentage of critical security vulnerabilities that previously escaped development have been caught?
- [ ] Has GitLab helped you achieve the KPIs you set out to solve?

**Customer Use Case Questions**

**Version Control & Collaboration (VC&C)/Source Code Management (SCM)** 
- [ ] How does your team create, manage and protect your project artifacts. (source code, designs, configuration, etc)?
- [ ] How were you managing project assets prior to GitLab?
- [ ] Does having one SCM tool improve your development process (efficiency, speed, quality, etc)?

**Continuous Integration (CI)** 
- [ ] How are you automating software development processes, testing and building code?
- [ ] How does GitLab Continuous Integration add value to your SDLC?
- [ ] How has GitLab Continuous Integration helped improve quality and speed of your delivery? 

**Continuous Delivery (CD)**
- [ ] How are you improving the speed to deploy code?
- [ ] Has using GitLab Continuous Delivery empowered your developers to deploy changes faster?  What's their feedback?
- [ ] Has the automated delivery increased your code output while maintaining quality?

**Development, Security, and Operations (DevSecOps)** 
- [ ] How are you identifying application vulnerabilities during your development process?
- [ ] How has detecting security bugs earlier impacted the SDLC?
- [ ] How has GitLab helped support audits and compliance etc?
- [ ] How has shifting security earlier in the delivery lifecycle impacted your speed to deliver?

**Agile Project Management (Agile)** 
- [ ] How are you managing your team's backlog, sprints, milestones, and future work?
- [ ] How do you have visibility of team results?
- [ ] How do you track/monitor project milestones etc?

**Simplify Development Operations / End to End DevOps (DevOps)** 
- [ ] How has collaboration across teams improved with GitLab?
- [ ] How has productivity or efficiency improved using GitLab? 
- [ ] What other benefits have you realized with GitLab and a single application?

**Cloud Native Approach to Applications Development (CloudNative)**
- [ ] How are you developing and managing cloud native applications? 
- [ ] How has GitLab helped you to manage cloud native/microservices? 
- [ ] What are the benefits of using GitLab's kubernetes integration?

**Infrastructure as Code (GitOps)**
- [ ] How are your operations teams managing different versions of their automation/configuration scripts?
- [ ] How are you testing and validating your infrastructure scripts?
- [ ] How are you integrating with other tools to maintain IAC?

**Deployment Strategy Questions**
- [ ] Can you describe the current deployment strategy you are using with GitLab? (target environment types, on-premise infrastructure and/or public cloud services, some combination of these and/or other resources)?
- [ ] Can you share the background and reasoning behind selecting this current deployment strategy?
- [ ] Were there specific business and/or technical reasons that shaped this strategy?
- [ ] **If customer identified they are using cloud services or plan to** Is your organization consuming or planning to consume cloud services? If so, which ones? and over what timeframe? Can you describe your current cloud deployment architecture-- if applicable?
- [ ] Are you using or planning to use containers? If so, can you describe your use of containers?

**Education Program Customers**
- [ ] How is GitLab advancing research at your University/Research Organization? 	
- [ ] What role does GitLab have in advancing open science?
- [ ] How are you using GitLab in your organization?
- [ ] What do the students and researchers like most about GitLab?
- [ ] What advantages does learning GitLab while in University give to students entering the professional world?
- [ ] How are students and researchers using GitLab to collaborate?

**Open Source Program Customers**
- [ ] How is GitLab impacting the number of contributions in your community?
- [ ] What kind of feedback have you received about GitLab from your community? 
- [ ] Did you need to adapt GitLab to fit your community’s needs? 
- [ ] What are the benefits of moving to GitLab for open source communities?
- [ ] What other services did you use before GitLab? (e.g. for hosting, task management, etc)
- [ ] What other services are you still using? (e.g. for hosting, task management, etc)
- [ ] Did you leverage any other open source programs? (e.g. Packet program, AWS credits, etc)
- [ ] (If not using the Community Edition) Which top tier enterprise features, if any, do you find most helpful? 
- [ ] How are you measuring the value that GitLab is delivering for your open source program? 

#### Publishing to the website

1. Start by opening a public issue and an Merge Request in the www-gitlab-com project.
2. Create a `.yml` file in `/data/case_studies` directory under Marketing site repo (www-gitlab-com project). This can be accomplished in the Web IDE.
3. Keep the name of file same as company name (this is not mandatory but it is easier to manage), for eg; if company name is "Foobar", create a file as `/data/case_studies/foobar.yml`.
4.	Once created, add contents of the file using the sample Case Study template, or by scraping the content from an existing case study, and then update the values of each property based on case study details, remember, do NOT change property names.
5. Add images to the same MR. This ensures they show up in the preview app. To accomplish this, stay in your MR and on the left rail open the selected file. Upload the file to the specific directory by hovering over the file and selecting upload file. The Cover image needs to be a .jpg and put in 'source/image/blogimage' directory. The company logo needs to be a color .SVG image and is placed in the 'source/image/case_study_logos' directory. 
6.	In the Web IDE, you can view generated Case Study page in the generated App under the URL, for eg; http://localhost:4567/customers/foobar.
7. Once the case study is ready to publish, remove the WIP label from your MR and ask an approved publisher to post. 

#### Adding customer logo and case study to customer's grid

1. When a customer has approved the usage of their logo on our /customers page, we place their approved logo in the grid on the bottom of the page.
2. Open an MR in the www-gitlab-com project. 
3. Add the approved single-tone .svg logo to the /source/images/organizations file.
4. Once the .svg is live in the organizations file, open the organizations.yml at www-gitlab-com in the /data file
5. Add new lines in the file to correspond to the exsiting format. 
6. Ensure the asset_type and asset_link are filled in and point to the live case study. 
7. Once the preview app shows a successful addition, remove the WIP label from your MR and ask an approved publisher to post. 

### Customer references collection

Our written customer case studies are found on the [/customers page](https://about.gitlab.com/customers/)
Our video customer case studies are found on the YouTube site on [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ__Rq59rec-EMgKsLuNjJE)

### Written Case Study creation process

Written Case studies are vital to showcasing the success of our customers and display how they overcame the pain and challenges their organizations were facing in their software development lifecycle. This is an opportunity to describe how GitLab helps overcome these pain points and provide value to the organnization. Often these stories require time for proof points and metrics to be established within customer organization.  Here are the steps to creating written case studies. 

1. Sales Rep follows process outlined in [Reference Program Process](### Reference-Program-Process) to alert the Customer Reference Manager that customer has agreed to participating in written case study process. 
2. Sales Rep introduces CRM to customer by email. CRM coordinates an introduction call with customer.
3. CRM meets with customer for intro call to uncover customer story and to label case study issue with applicable value drivers and customer use case. CRM will share applicable [interview questions](#### Common-Interview-questions) with customer by email. During this session, CRM will work with customer to determine the best timeframe for interview call. Common time frames are 30 days, 60 days, 90 days or 6 months. CRM distributes the logo permission form to customer to help increase speed of legal approval. CRM will also bring up possibility of Media opportunities for case study. 
4. For the interview call, the CRM leads the discussion. The content creator is on the call to make sure questions are answered to get the case study completed and that they have everything they need to create case study. 
5. CRM gets interview transcribed using rev.com and sends the resulting transcript to customer for redlines/changes and approval to use. CRM to check with customer that legal and communications team is aligned with the process and is approving assets along the way. 
6. Once the transcript is approved for use, CRM puts approved transcript in the customer-identified folder in WIP Case Studes in Google Drive. CRM alerts Customer Content that the transcript is approved and links to it in the case study issue description. 
7. Customer Content drafts case study and alerts CRM when draft is ready for review. 
8. CRM reviews draft for completeness, content and makes sure it acurately refects the customer story. CRM brings in designated use case PMM teammember for additional review (if necessary) and the Alliances/Partner team is alerted to draft by alert in the issue if the CRM deems it necessary. The additional reviewers will be tagged in the issue and also slacked the issue. They will have 3 working days to respond.The Draft is sent to PR team for media opportunity exploration. Draft is sent to content team for a copy edit review. 
9. Customer content uses feedback and creates a clean draft to send to customer. CRM sends draft to customer with Customer content cc'd in the email. This email should be sent as both a link to a google doc as well as a .docx file attached to the email. 
10. Once customer feedback is recieved, customer content addresses any concerns and CRM reviews and sends final version to customer for approval or continued discussion around draft. 
11. If the marketing approval form hasn't already been recieved, CRM sends the marketing approval form for the customer to review, sign and return to GitLab. 
12. CRM will review the PR potential of the reference with the PR team and if they deem it has PR merit, the CRM will propose PR support for the reference to the customer.
12. If customer agrees to media awareness around the case study, introduction to the PR team is co-ordinated by the CRM.
13. Once customer approves final version and a signed marketing permission form is recieved, CRM loads the signed form into the SFDC account and either customer content or CRM publishes case study on the /customers page. 
14. CRM and Customer content begin internal promotion process.
15. CRM alerts customer that the case study is live and sends custom Customer Reference Swag. 




### Requesting a Reference Customer to speak and/or otherwise support an event
To request a reference customer to speak and/or otherwise support an event the first step is to [open the Customer Speaker Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issuable_template=customer-speaker-request) a minimum of 60 days before the event. This issue will be tracked on the Customer Reference Program [board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name[]=Customer%20Reference%20Program).

**Process for fulfilling a request to support a Field Marketing, Corporate Marketing/Company, or Community Event**
   1. Requestor [opens the Customer Speaker Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issuable_template=customer-speaker-request) a minimum of 60 days before the event. This issue will be tracked on the Customer Reference Program [board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name[]=Customer%20Reference%20Program).
   1. CRM will load the request into Reference Edge for tracking purposes. 
   1. CRM will pull a SFDC report of customers in billing city/state/area to view an applicable pool of speakers as well as a report in Reference Edge.
   1. CRM will create a table from the report in the issue listing AE/SAL and customer accounts. CRM will tag any additional AEs/SALs in the issue. 
   1. CRM will work with AEs/SALs to identify customer stories (if known).
   1. CRM will co-ordinate with the Sales and the requestor to refine the speaker pool. 
   1. CRM will decide if a customer is appropriate for the outreach based upon other references that involve that customer
   1. CRM and AE/SALs will reach out with speaking request to approved customers giving customers a minimum of 45 days before the event to respond. 
   1. If customer accepts, introduction to the requestor is coordinated to lead the event engagement.
   1. If customer outreach is unsuccessful (max 3 customers approached), the requestor can look at internal or other alternatives, taking their event forward without further CRM involvement.
   1. Customer Speaker Request issue is closed out once a customer has accepted to speak or if no connection with the customer was made after 2 attempts to connect. From there all communication on the logistics will take place in the main event issue and any content communication will take place in the PMM support issue, listed below. 
   1. When a reference customer agree to present; the CRM should open an [SM Request issue for content and delivery support](https://gitlab.com/gitlab-com/marketing/product-marketing/-/blob/master/.gitlab/issue_templates/A-SM-Support-Request.md) with the PMM team to assist in content development and/or review and when helpful, public speaking skill development.
   1. Requestor will be mentioned in this issue to stay aware of the progress of the content development and help with prioritization.
   1. Requestor will make themselves available to assigned PMM and CRM to ensure content requirements and event themes/audience is well understood.
   1. PMM will lead content generation and content review meetings (with coordination support provided by CRM as needed) with CRM and requestor invited to attend to as optional attendees.
   1. CRM will review the final content and approve that it reflects and showcases customer story.
   1. Requestor and/or AE/SALs will provide direct logistical support (incl dry run session) to the reference customer speaker for both virtual and in-person events with the CRM informed.
   1. CRM will review the event post execution for reference opportunities and repurposing of the delivered content. 

Note: If travel is required for a customer speaker, this will not be funded by the Customer Reference Program.

**Process for fulfilling a request to support a Partner/Alliance Associated event**
   1. Requestor opens an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests" a minimum of 65 days before the event. Ideally 90 days.
   2. Please send a slack notification to the regional [Customer Reference Manager](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue. 
   3. In the issue, please identify Partner/Alliance organization and anticipated outcome.
   4. Requestor to identify tier of customer requested. (Enterprise, Commercial, SMB)
   5. Customer Reference Manager identifies pool of proposed customers with Reference Edge, SFDC, and Alliance customer stories.
   6. CRM distributes pool to requestor and works with them to identify a short list of customers to request.
   7. CRM reaches out to appointed SALs/AEs of customers to verify timing of request.
   8. CRM or SAL/AE outreach to customer a minimum of 60 days before the event. 
   9. If customer outreach is successful, the Alliance Manager is introduced to lead the engagment 
   10. If customer outreach is unsuccessful (max 3 customers approached), the Alliance Manager can look at internal or other alternatives, taking their event forward without further CRM involvement.
   11. When a reference customer agrees to present; the Alliance manager should open a SM Request issue for content support with the PMM team.
   
Note: If travel is required for a customer speaker, it is covered by different parts of the org. (Determined on a per request basis.)

### Requesting a customer for a webinar
   1. Please open an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests" a minimum of 60 days before the event. 
   2. Please send a slack notification to the regional [Customer Reference Manager](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue. 
   3. CRM will pull a SFDC report of customers to view an applicable pool of speakers as well as a report in Reference Edge.
   4. CRM will create a table from the report in the issue listing AE/SAL and customers. CRM will tag any additional AEs/SALs in the issue. 
   5. CRM will work with AEs/SALs to identify customer stories (if known).
   6. CRM coordinate with the Marketing manager to refine the speaker pool. 
   7. CRM will decide if a customer is appropriate for the outreach based upon other references that involve that customer.
   8. CRM and AE/SALs will reach out with speaking request to customers giving customers a minimum of 45 days before the event to respond. 
   8. If customer accepts, introduction to the marketing manager is coordinated.
   9. If customer outreach is unsuccessful, the Event Manager can look at internal alternatives and takes their event forward without further CRM involvement. 
   10. When the customer agrees to present; the Marketing manager should open an SM Request issue for content support with the PMM team.

### Customer Event Presentation Process
**Process to engage with the customer once agreement for presenting at an event is secured**
   1. Customer Reference Manager (CRM) sends the customer an email (cc'ing the Event Manager as well) to request their professional photo/bio/abstract details and shares an outline for creating their success story in their corporate template.  The proposed format:
      * Landscape before GitLab: Describe the challenges/business pains experienced with CI/CD and DevOps before adopting GitLab. What problems did the customer set out to solve?
      * Why GitLab: Explain customer's reasoning for selecting GitLab. Who else was considered and/or why did we stand out?
      * Results after implementing GitLab: Talk about the positive results with GitLab at a high level. This helps set the stage for more detailed, specific metrics / numbers that validate the results in the slide that comes next. 
      * Key benefits and Metrics from GitLab (align with value drivers/ use cases etc)
      * Future Plans with GitLab
    The above format is a proposed flow for the customer only; we welcome customers adding additional details (screenshots/graphic/images etc) to their presentation that reflects their success story to really make it their own. The CRM will work with the PMM to support the customer with messaging and content as required.
   2. The Event Manager supported by the CRM coordinates a maximum of 3 planning calls (including a dry run) with the customer to be respectful of everyone's time. The Event Manager schedules the planning and dry run calls in the calendars of all relevant parties as soon as feasible.<br> 
      * The Event Managers act as the DRI to facilitate these calls, set the agenda, and ensure the process goes smoothly.<br>  
      * The CRM is the DRI for customer content creation only, not the overall event.<br>  
      * The CRM is optional to attend the dry run and the actual event as by then the customer content requirement has been delivered.<br>  
      * The CRM also helps coordinate between the customer and GitLab team members collaborating on the content.


### Requesting a customer for analysts
**Requesting a confidential request (Customer not named in report)**
   1. Analyst Relations team adds label "Customer Speaking Requests" to the report issue as soon as issue is opened.
   2. Analyst Relations team adds a slack message with the issue in the "Customer_References" Channel.
   3. Customer Reference Team will work with Analyst Relations manager to narrow down the pool of potential customers based on used stages etc. 
   4. CRM to review previous customer references for the specific report. CRM to identify 1 speaker to show growth and maturity of product to speak for report again. 
   5. CRM to outreach to SAL/AEs for customers to get approval to speak for report. 
   6. CRM to outreach to customer for request to speak for report. 

### Requesting a customer for Public Relations activities
 1. Please open an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests" 
 2. Please send a slack notification to the regional [Customer Reference Manager](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue.
 3. CRM will review the request and suggest customers for the PR activity to the requestor 
 4. CRM will partner with the AO's for the customer outreach to request their participation in the PR activity. 
 4. CRM will introduce the PR Manager to the customer to liase with them on the PR activity. 

### Customer Logo Permission Form
**Purpose: To reflect our customer base we use customer logos to promote organizations that are using GitLab** 

Customer logos appear on our website and other marketing and promotional materials. To respect the intellectual property of companies that use our product, we request a signed permission form to use their logo. To request logo usage from a customer, please send the [Logo permission form](https://drive.google.com/open?id=1oCRUFM4cjOTHU8DhfSBr6uO_sTYURAGo) to the appropriate member of the customer organization. 
   1. Once we have received back the signed logo form, send the form to the Customer Reference Team.
   2. The Customer Reference Team will attach the signed form to the account on SFDC.
   3. The Customer Reference Team will add the customer to the Reference Edge software with logo usage selected. 
   4. The Customer Reference Team will then add the logo to the customer approved locations (including website and or promotional presentations.)


### GitLab Customer Advisory Boards
Information about the GitLab Customer Advisory Boards [is found on the CAB page](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/CAB/)

### Peer Reviews
Learn about our [Peer Review](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/peer-reviews/) Management Program



### Customer Anecdotes
Customer anecdotes are short, "snackable" customer stories. These can be used on a call or in a meeting to share a point about GitLab by validating it with the customer story. An anecdote can be a summary of a formal, attributable case study or just an anonymous story from a conversation you had with a customer. It's important to keep customers anonymous unless we have explicit approval to use their name.

#### Remove the complexity of multi-tool environments

A communications company recently shared how they built their technology stack. "GitLab checks so many boxes for us that we don't need other tools." The company then explained that they are operating with a team of 2 people because GitLab makes the environment so simple. They said another organization would probably have to employ 15 people to accomplish what they can.

#### Administrative overhead

We hosted a dinner for customers and prospects to mingle with each other and share stories. When the topic of managing the DevOps toolchain came up, the Head of Risk at a large US bank mentioned that she had a team of 20 to keep SDLC tools running in her org. A GitLab customer, the managing director of a product group for a global investment management corporation said, "this is going to break your heart, but I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs."

#### Deliver value faster

Pinterest is not a GitLab customer, but uses Kubernetes together with Jenkins. Because there's no [native kubernetes integration](/solutions/kubernetes/) for Jenkins they needed to dedicate a [team of 4 spending 6 months](https://kubernetes.io/case-studies/pinterest/) to build a custom system to control access management and allow teams to self-serve builds. This is functionality that comes out of the box on day one with GitLab.

#### Adoption at an incredible pace

A large enterprise in the software space is a recent GitLab Premium customer wanting to replace Perforce. They predicted that it would take them 3 years to hit 6K active users. But, the developer experience was so superior that they ended up hitting 6K active users in only 8 months.

#### Security is at the center

A publishing firm is utilizing GitLab best of breed CI/CD functionality and with upcoming product advancements they have informed us that GitLab is a mission critical application for them. They also chose GitLab over the competition because security is currently the biggest aspect they want to improve on internally.

#### Removing the wait

A financial services company recently stated that their teams went from two week releases to six times per day using GitLab. They are able to release this quickly because they do not need to wait for infrastructure.

#### No babysitter required

An online gaming development house has a team of 9 looking after its toolstack for SDLC. GitLab is one of the tools in the stack, but is self-sufficient enough that they only deal with it every six months.

#### Providing happiness everyday

A large media company recently stated that GitLab is the best architected application they have ever used. According to this company, "This product is a joy to use. We cannot believe how much incremental value you crank out with each release."



### Customer Case Studies

Read the current GitLab customer case studies on the [GitLab customer page](https://about.gitlab.com/customers/). 

### Which customer reference team member should I contact?

  - Listed below are areas of responsibility within the Customer Reference team:

    - [Kim](/company/team/#kimlock), Reference Program Manager, Americas
    - [Fiona](/company/team/#fokeeffe), Reference Program Manager, EMEA and APAC
    - [Jen](/company/team/#jlparker), Reference Program Manager, Peer Insights
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights
    - [Ashish](/company/team/#kuthiala), Senior Director, Strategic Marketing
