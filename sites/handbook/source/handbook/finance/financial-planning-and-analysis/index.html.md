---
layout: handbook-page-toc
title: "Financial Planning & Analysis"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Financial Planning & Analysis @ GitLab

## Common Links

 * [Finance Issue Tracker](https://gitlab.com/gitlab-com/finance/issues)
 * [Analtyics Issue Tracker](https://gitlab.com/gitlab-data/analytics) for all data, dashboard, or reporting requests
 * [Slack Channel](https://gitlab.slack.com/archives/fpanda)
 * [Hypergrowth Rule](/handbook/finance/financial-planning-and-analysis/hypergrowth-rule)
 * [Sales Finance](/handbook/finance/financial-planning-and-analysis/Sales-Finance)
 * [FP&A Job Ladder](https://about.gitlab.com/job-families/finance/finance-planning-and-analysis/)

## What is purpose of FP&A @ GitLab?
1. Facilitate execution of GitLab's [strategy](/company/strategy/)
2. Ensure our investor narrative aligns with operating strategy
3. Bring predictability to Gitlab
4. Ensure financial and operational goals of GitLab are defined, documented and achieved
5. Evangelize awareness of strategy and each departments role in achieving it
6. Ensure sound data-driven decision support

## How GitLab’s FP&A plans to get there….
1. Manage the budget and planning processes for GitLab's [operating plan](/handbook/finance/financial-planning-and-analysis/#operating-plan)
2. Build and maintain a [long-term financial model](/handbook/finance/financial-planning-and-analysis/#gitLab-integrated-financial-model) that projects performance into the future
3. Define business drivers and [KPIs](/handbook/business-ops/data-team/metrics/) in our operating and long-term models in collaboration with the business and measure efficacy of the business plan.
4. Own the rolling forecast process and provide real-time information on how departments are performing relative to forecast. Improve forecasts.
5. Provide insights on the business drivers to constantly look for opportunities to improve performance
    - Process Improvements
    - Analytics
    - Education

## GitLab's Operating Plan and Forecasting Process

### Definitions

#### Operating Plan

Purpose: GitLab's operating plan is a guideline to understand how much capital it needs and how the capital will be consumed. The operating plan helps GitLab answer questions like "how fast can we hire and when?"

What: The operating plan is a three statement (Income Statement, Balance Sheet and Statement of Cashflow), non-GAAP bottoms-up plan that spans the current fiscal year. The revenue is driven off the GitLab revenue model and the expense part of the plan is at the headcount and vendor level. The Operating Plan will be a 90% confidence Plan.

Governance: The Operating Plan is approved by the board of directors every year.

#### Actuals

What: Actuals are results that have been reported or exist in a system that is designated as a single source of truth for the item that is being measured. Each month accounting closes the month and financial results are recorded in our ERP system and are published in our financial statements. These actuals are compared to the operating plan.

#### Monthly Forecast

Purpose: In a dynamic high-growth business, GitLab's needs may change through the year and we need to be able to predict what is going to happen.

What: Forecast is a dynamic assessment based on current expectations of financial performance. The FP&A team will publish a monthly forecast for revenue driven by key metrics and expenses driven by headcount and vendors. A monthly forecast does not extend the forecast period. For example in March 2020, the forecast will span from February 2020 to January 2021 with February actuals and March 2020 to January 2021 forecast - this will be called the (1+11) forecast. A monthly forecast will not be formally compared to actuals in variance.

Governance: The monthly forecast is approved by VP Finance and reviewed with CFO.

#### Quarterly Forecast (Rolling four quarters)
Purpose: In a dynamic high-growth business, GitLab's needs may change through the year and we need a guidepost to hold business leaders accountable. We plan our expenses at a high level (e-group) and we expect this group to make prioritizations and trade-offs while remaining accountable against the plan parameters. By formally reforecasting quarterly, we can quickly evaluate and incorporate new initiatives into our forecasting model. That being said, we do follow an annual plan to set our goals and measurement for our top-level targets of revenue, profitability and expense management.

What: Forecast is a dynamic assessment based on current expectations of financial performance. The (3+9), (6+6) an (9+3) quarterly forecasts include revenue driven by key metrics and expenses driven by headcount and vendors. On the quarterly forecast another quarter will be added to the end of the forecast period so that we have a valid rolling four quarters. For example in May of 2020 we will forecast from Feb 2020 to April 2021 adding FY22-Q1 with FY21-Q1 (Feb 2020-Apr 2020) as actuals. The team may go out farther to the end of the next fiscal year.

Governance: The quarterly rolling forecast is approved by the eGroup and CEO and reviewed with the board of directors. eGroup will be held accountable to the quarterly rolling forecast for expenses. For revenue the company will always be held accountable to Plan.

#### Pizza Guidance - Forecasting

We try not to take ourselves to serious all the time, but there is nothing mellow about a little camaraderie to prove out who has the best quarterly bookings forecast. At the beginning of quarter anyone at GitLab can provide IACV bookings guidance so long as they provide supporting documentation to his / her guidance. Once the quarterly IACV bookings are finalized, the person with the most accurate forecast will recieve a pizza gift card of his / her choosing, so long as it is available as an online gift card.

The rules, quarterly forecasts, and results can be found [here](https://docs.google.com/spreadsheets/d/17MKHuFBLrLDS9OkghhoriaOn0IhISsu8OVfWrD6qG2w/edit#gid=310945519). If you are interesting in playing, please join the #fpanda channel and let the channel know your interested.

"Good forecast guidance comes from eating pizza" - Pizza

#### Target
What: Target is a goal or objective that may be higher or lower than the Plan or Forecast. Targets are typically used in conjunction with setting OKRs, compensation plans and other performance objectives.
Governance: A target that relates to IACV is usually agreed upon by the DRI of the target and VP Finance or CFO. Other targets are part of OKRs and reviewed by the CEO.

#### Baseline
What: Baseline is a measurement of actual expense or revenue that relates to a certain point in time (i..e month, quarter or year). We use baselines to measure progress of improvement against actual results. The progress can be stated in monthly, quarterly or annualized terms.

### Mechanics of the Operating Plan:
- The Operating Plan Bookings Plan is developed by completing a bottoms up assessment of expected renewals, churn, expansion bookings and new bookings. This Plan is validated via analysis of historical cohort behavior, landed account potential (LAM), marketing Plans and sales capacity.
- The Operating Plan includes expenses for sales and marketing with capacity to achieve bookings at a 50% confidence level. Expenses for R&D, G&A, Sales Commissions and Cost of Revenue are planned at the 90% confidence Booking Plan level. The Operating Plan also assumes that the corporate bonus target will be fully achieved at the 50% confidence level.
- The [rolling 4 quarter forecast](/handbook/finance/financial-planning-and-analysis/#rolling-4-quarter-forecast) builds off of the revenue projected in the revenue model. During the rolling 4 quarter, departments build out plans to help meet their strategic goals. Understanding the departmental plans determines how much of the capital will be consumed for foreseeable future.

- The [budget](/handbook/finance/financial-planning-and-analysis/#budgeting-@-gitLab) looks to GitLab's [long term profitiablity goals](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets) and incorporates plans made during the rolling 4 quarter forecast in addition to revenue forecasts from the revenue model. In order to have a budget, GitLab must have a forecast(s) for it's operating plan. Therefore, the revenue and rolling 4 quarter forecasts are critical to set budgets. Budgets help GitLab understand how it's spends its cash and help predict long term targets, but ultimately budgets are a guideline.

### Operating Plan Diagram
![alt text](/handbook/finance/financial-planning-and-analysis/financial-workflow.png "Operating Model")

## Planning process @ GitLab

* Planning begins with an annual planning process that drives the annual board approved budget.
* Every month as part of the close process the team will generate a monthly forecast
* Every quarter as part of the close process the team will generate a four quarter rolling forecast
* If GitLab is exceeding the Operating Plan, on a quarterly basis the executive team can decide to increase expense as long as we maintain EBIT goals approved by the board of directors.

### Annual Planning Steps (WIP)
* Align executive team on strategic priorities
* Sign-off on financial priorities for the year
* Develop baseline financial case for the year
* Review product investments vs expected revenue generation.
* Revise and update the annual sales compensation plan.
* Set annual quota assignments for revenue producing roles.
* Set expected amount for annual compensation increases.
* Set targets for any contributors on a company based performance plan.
* Set company targets for board, investors and creditors.
* Our Annual Plan is viewable internally as a google slide presentation.  Search on "[current year e.g. 2018] Plan" to view.
* Build out bottoms up integrated sales and marketing financial model with key assumptions documented so they can be tracked
* Set targets agreed upon by Product, Marketing and Sales.
* Generate a the operating plan based on a headcount list, tbh list, vendor level spend list
* Headcount list communicated to recruiting, proposed vendor list communicated to procurement (future with Purchase Orders in place for existing vendors)

#### Annual Planning Important Dates (TBD - 2020 shown)
* **28th of October** - Five quarter rolling forecast kick-off
* **22nd of November** - Five quarter rolling forecast completed
* **22nd of November** - Sales Compensation Direction
* **2nd of December** - Preliminary outlook reviewed at Board of Directors
* **16th of December** - Plan iteration and discussion at egroup meeting
* **18th of December** - Product Roadmap and Investments (board review)
* **17th of January** - Go to market planning deep dive (board review)
* **24th of January** - Plan sent to Board for async commentary
* **31st of January** - FY22 Plan Approved by Board
* **7th of February** - Sales compensation plans distributed to sales team

****

## Monthly Close Process and Variance Meeting (WIP)

GitLab’s FP&A team will participate in a rigorous monthly close process. The close process has clear deadlines to best support our accounting team, key dates to deliver information to the EVPs / department heads, analyze the performance of our budgets and forecasts against actuals, update our internal forecasts and eventually update our investor guidance to prepare for the quarterly earnings call. Our philosophy is to compare our forecasts to results so that we can constantly improve our forecasting methodologies and approaches to hold ourselves accountable. Additionally, this process drives accountability to the business owners of the budget.

### Monthly FP&A Close Process

These dates are based on a 10 day accounting close. FP&A needs two days to do variance analysis, post close and another two days to lock rolling forecasts and complete guidance analysis.

BD=Business Day, so for example BD4 means four business days after the month has ended. Let’s say the month ended on a Thursday. BD1 would be Friday, BD2 would be Saturday, etc.

#### Close process (today)

**Bold denotes Accounting deliverable**
- BD -2: AP Accrual notification for FP&A team.
- BD1: Collect Marketing, Legal, Hosting accruals for accounting. Send out Investor Update notification.
- BD2: **Payroll, Vendor and accruals booked.**
- BD3: **Legal accruals booked. Hosting accruals booked.** Finance Business Partners: Review vendor accruals to check for accuracy.
- BD4: **AR Closed.**
- BD5: **Revenue, Sales comp booked**. Variance reviews with EVPs for Marketing, R&D, G&A (without sales comp and allocations).
- BD6: **Allocations prepared**
- BD7:
- BD8: **Preliminary financials released.** Variance review with CRO. Investor Update signed off by CFO, VP, Finance.
- BD9:
- BD10: Investor update sent out by CEO.
- BD11: **Updates to preliminary financials finalized, accounting closed.**
- BD12: Variance Review Package ready for CFO.
- BD13: Revenue/EBIT guidance updated.
- BD14: Monthly rolling forecast locked.
- BD15: Review and sign off on revenue of variance package by CFO, VP, Finance.

#### Close process (future)

**Bold denotes Accounting deliverable**

- BD -2: **AP Closed.**
- BD1: Ensure all accruals for Marketing, Legal, Hosting are collected.
- BD2: **Payroll, Vendor and accruals booked.**
- BD3: Finance Business Partners: Review vendor accruals to check for accuracy.
- BD4: **Revenue, AR Closed.**
- BD5: **Sales Comp Booked**, Variance reviews with EVPs for Marketing, R&D, G&A (without sales comp and allocations).
- BD6:
- BD7:
- BD8: **Preliminary financials released**, Variance review with CRO.
- BD9:
- BD10: **Accounting Close**
- BD11: Review accounting controller book and fluxes. Prepare for 10Q/10K drafting.
- BD12: Variance Review Package ready for CFO.
- BD13: Revenue/EBIT guidance updated.
- BD14: Monthly rolling forecast locked.

#### Quarterly Close (Future assuming public company):

**4 weeks before earnings call**
- BD5:  Publish flash results to the board. Call with the Audit Committee Chair on where the quarter ended (CFO, PAO, VP Finance, IR).

**3 weeks before earnings call**
- BD7: First sales forecast for bookings is published for the new quarter. Including first commit.
- BD7: Finance team deep dive with VP Enterprise on top 10 large deals.
- BD8: Guidance analysis based on rolling forecast Revenue.
    - Run all forecasts on Bookings (see below).

**2 weeks before earnings call**
- BD12: Review updated sales forecast.
- BD14: Guidance analysis based on rolling forecast EBIT. Analyze share counts and implied EPS.

**1 week before earnings call**
- BD16: Metrics package and Q&A document ready for earnings call.
- BD17: Next updated sales forecast.
- BD18: Lock quarterly rolling forecast and approval from eGroup.


#### Guidance process (future):
**Step 1: Run all forecasts for bookings**
* Run cohort based forecast model validated by Landed Addressable Market forecast model.
* Run pipeline based forecast model.
* Run pacing and linearity based forecast model.
* Review sales team forecast, including CRO commit, large deal deep dive.
* Review renewal base forecast.
* Review professional services bookings.
* Review True Ups forecast.

**Step 2: Review key metrics trends and reforecast**
* Reforecast key metrics.

**Step 3: Analyze revenue**
* Detail out variance error in terms of bookings timing, bookings linearity, revenue components, product mix.
* Review revenue forecast at component level.
* Complete revenue risk analysis. Generate revenue risk chart.

**Step 4: Analyze expense profile and EBIT**
* Review detailed variance materials to identify expense risks.

**Step 5: Generate guidance proposal, ranges and waterfalls**
* Generate both revenue and EBIT guides. Analyze EPS.


### Variance Meeting with CFO

Each month after the financials have been published, GitLab reviews all aspects of the business including Corporate Metrics, Bookings, Revenue, Gross Margins, Expenses, Balance Sheet and Cash Flow. The goal of this review is to do a comprehensive review so that finance leadership has a pulse on the business and the financials.  

The variance analysis will compare department budgets with actual results and examine any material differences between budgeted and actual costs. Additionally, the actuals for expenses will be compared to the quarterly rolling forecast. The expenses are reviewed at the divisional department level, allowing GitLab to measure progress in meeting its Plan (Q1-Q4) or rolling forecast (Q2-Q4). The team also evaluates the accuracy of forecasts, and operating model.

#### Variance Deck details

To be Updated

#### Variance Analysis
The study of differences between budgetary and expected cost. At GitLab, different measures of materiality thresholds are measured during the variance analysis process, including the Monthly Finance Planning Meeting. During the variance analysis processs the GitLab FP&A team analyzes and isolates any variance in question to the lowest level possible. The team reviews detailed items in order to identify the root cause of the variance. This could include transaction date, cost center, vendor, location, department or additional low level details.

At GitLab, analysts begin the variance analysis process with trended data to understand if there is a recognizable pattern. This helps determine if the variance was caused by incremental change or driven by one particular event and helps identify the root path quickly.

The FP&A team takes the following into consideration while evaluating variances in relation to materiality thresholds:

- The percentage size of the variance (i.e. what was the overall varinace by percentage)
- The intent of the variance (i.e. did something stand out as deceptive)
- The correlation to other variance (i.e. did a immaterial difference in one place cause a material difference in another)
- The inherent character of the variance (i.e. does the expense correlate to the traits of the business)

#### Performance Indicator: Variance percentage

We measure our team performance based on our forecast accuracy, also known as variance percentage. Variance percentage is defined as the difference between actuals and Plan or rolling forecast. We calculate it as follows:

Variance Percentage (Plan) = (Actuals - Plan)/Plan  or

Variance Percentage (Forecast) = (Actuals - Rolling Forecast)/Rolling Forecast

Our goal is to have revenue and EBIT variance percentage within +/- 2% on a quarterly basis.

##### Types of Threshold and Materiality

Generally accepted accounting principles (GAAP) does not provide definitive guidance in distinguishing material information from immaterial information. Therefore, GitLab uses a percentage based approach for defining materiality thresholds and can be found below. The [Plan vs Actuals vs Forecast](https://app.periscopedata.com/app/gitlab/525851/Plan-vs-Actuals-vs-Forecast) Sisense dashboard provides the data for the threshold analysis via a color coded legend.

Thresholds Materiality

We believe that Revenue & EBIT actuals that have a greater variance of  +/- 5% vs Plan or Forecast is considered material. 

Logging Threshold Differences

Coming soon...


### Variance Meeting with EVPs

Additionally, each finance business partner will run a meeting with the VP of Finance and the EVP to review the past month. The information should be presented as timely as possible. Given the accounting close is 10 days, the team is asked to use pre-close numbers for the review to increase the speed of information. During the meeting, the Finance Business Partners will review GitLab results in addition to a detailed overview. Each division can expect to review the following during the monthly meetings:

1. Company results
   -  Bookings IACV, TCV
   -  Spending OpEx vs Plan
1. Divisional level review including an executive summary with relevant insights and watchpoints
  - Last month vs plan/rolling forecast
  - Projection for the current quarter
1. Detailed variance details that helps the EVP understand the financial picture of their expenses
1. Discuss upcoming changes to financial processes that EVPs need to be aware of
1. Discuss upcoming changes to help Finance Business Partner update the rolling forecast
1. Review next 4 quarters vs Plan for the Division and for each department on the quarter

Process
Following the month-end close, the Finance Business Partners will create a variance deck and distribute department income statements to the related budget owners and the e-group members. Each department is then responsible for comparing these reports, which contain actual costs, to the budget. Departments, with guidance from the Finance Business Partners, should analyze their data and if necessary, discuss items of interest and take appropriate action. Any questions regarding the cost data should be discussed with the Finance Business Partner.

#### Marketing Specific Monthly Variance Analysis
In partnership with Marketing Operations and Accounting, we set out to improve the timing and accuracy of budget vs. actuals tracking for marketing programs opex and to provide data driven insights to these teams during the close process by BD3 at the beginning of a new monthly accounting period.

The utilization of [campaign finance tagging](https://about.gitlab.com/handbook/marketing/marketing-operations/#campaign-cost-tracking) for marketing program spend enables month-end reconciliation of expense actuals, prepaids, and accruals at the budget line item level. The components of this analysis are as follows:

1. **Actuals:**
    - Prepaids provided by accounting on BD2 in a new monthly accounting period, grouped by campaign finance tag
    - Transactional ledger detail of expenses grouped by campaign finance tag. This can be pulled using the Marketing Campaigns Tagging Dashboard in Sisense, or an income statement in NetSuite.
1. **Budget totals:**
    - Using the Marketing_Non-HC_R4QF_FY21 budget document, join the datasets based on campaign finance tagging to pull the relevant line items from the budget.

Once these components are in place, a comparison showing the difference between budget vs. actuals may indicate the need for an accrual or indicate a budget overage. The finance business partner can then take appropriate action with the accounting team or the business to resolve. The basic structure of this analysis should resemble the following:

| Campaign Finance Tag | A: Expense | B: Prepaid (GL Acct# 1150) | C: Total (A+B) | D: Budget line item | E: Variance (D-C = Potential Accrual/Overage) |
| ------ | ------ | ------ |------ | ------ | ------ |
| ISOdate_CampaignShortName | $000's | $000's | $000's | $000's | $000's |


#### Making Changes to the Plan to Optimize for Growth, Efficiency, or Risk

Predictability is a top priority. If we are exceeding our Plan, the company will have opportunities to make additional investments that fit within our EBIT and free cash flow margin structure. We encourage our team members to actively seek opportunities that will help us grow faster or become more efficient each quarter. Ideally, team members can do this within the budget allocation. If a growth/efficiency initiatives requires additional spend will be evaluated and agreed upon by eGroup, as follows:

1. If the investment will allow us to increase bookings or increase efficiency in the current year or in future periods with a positive return.  
1. If the investment de-risks future years bookings achievement
1. If the investment decreases risk / increases compliance per our Enterprise Risk Management program

#### Out of Budget Business Case

In the event an organization is asking to spend money outside either the [Annual Plan](/handbook/finance/financial-planning-and-analysis/) or [Quarterly Forecast](/handbook/finance/financial-planning-and-analysis/), you should develop a business case showcasing what the spend is for and how GitLab can benefit from spending it.

This business case should be completed in conjunction with your [Finance Business Partner (FBP)](/handbook/finance/financial-planning-and-analysis/) to articulate the financial impact of the request.  Below are a few examples of the question you should be trying to answer:
* What is the purpose of the spend?
* How much is the spend?
* What kind of spend is it? (One time, contract, utility-based, etc..)
* What is the Return on Investment?
* If the ROI cant be measured what KPI are we trying to move?
* Can we measure the impact in terms of ROI or KPI improvement?
* Have we investigated any other options?

Together with your FBP , this analysis should be presented to your leader and then to eGroup for Approval.


## Investor and Board Communication

Throughout the year the Executive Business Partner team puts together a calendar of events for board members and other events associated with planning. Those events can be found in the [GitLab Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit?usp=sharing) sheet.


## By Cost Center - Where GitLabbers Report

Below is a table showing the structure of GitLab departments as they exist in NetSuite. Please [check out the Team Page](/team/chart) to see our org chart.

|Cost of Sales        |Sales               |Marketing                 |R&D                    |G&A | GitLab.com | GitHost |
|:------:             |:------:            |:------:                  |:------:               |:------:|:------------: | :------------: |
|Education Delivery   |Business Development|Content Marketing         |Product Strategy       |Business Operations|
|Consulting Delivery  |Channel             |Sales Development         |Infrastructure         |CEO        |
|Customer Support     |Commerical Sales    |Field Marketing           |Development            |Finance    |
|                     |Practice Management |Partner Marketing         |Quality                |People Success |
|                     |Customer Success    |Strategic Marketing       |Security               |Recruiting |
|                     |Enterprise Sales    |Brand & Digital Design    |UX                     |Legal           |
|                     |Field Operations    |Marketing Ops             |Product Management     |Accounting            |
|                     |                    |Inbound Marketing         |                       | |  
|                     |                    |Campaigns                 |                       | |
|                     |                    |Digital Marketing         |
|                     |                    |Owned Events              | | |
|                     |                    |Sponsored Events          | | |
|                     |                    |Community Relations       | | |
|                     |                    |Communications         | | |
|                     |                    |Awareness                 | | |


## By Function - Where GitLabbers Report

|Sales               |Marketing                 |Engineering         |Product                   |G&A |
|:------:            |:------:                  |:------:            |:------:                  |:------:|
|Business Development|Content Marketing         |Customer Support    |Product Strategy          |Business Operations|
|Channel             |Sales Development         |Infrastructure      |Product Management        |CEO        |
|Commerical Sales    |Field Marketing           |Development         |                          |Finance    |
|Practice Management |Partner Marketing         |Quality             |                          |People Success |
|Customer Success    |Strategic Marketing       |Security            |                          |Recruiting |
|Enterprise Sales    |Brand & Digital Design    |UX                  |                          |Legal           |
|Field Operations    |Marketing Ops             |                    |                          |Accounting            |
|Education Delivery  |Inbound Marketing         | | |  
|Consulting Delivery |Campaigns                 | | |
|                    |Digital Marketing         |
|                    |Owned Events              | | |
|                    |Sponsored Events          | | |
|                    |Community Relations       | | |
|                    |Communications         | | |
|                    |Awareness                 | | |


### Monthly Investor Update
****

Each month we send to our investors an update no later than the 10th day following the end of the month. For further reference see our [blog post](/blog/2018/10/17/how-we-keep-investors-in-the-loop/).
#### Format
1. Thanks
1. Asks
1. Key Metrics
1. Lowlights
1. Highlights
1. Expectations (next month)

#### Process
1. On the 1st day of the month, the FinOps lead prepares a draft of the previous months investor update in a google document and posts in the #investor-update slack channel.
1. On the same day the draft is added to the #investor update slack channel, the FinOps lead will `@mention` e-group members with asks for topics related to the investor update agenda.
1. The e-group members will have no later than the 9th of each month to review and add input.
1. No later than the 10th of each month the will CEO send the update to the investor mailing list.
1. Once the investor update is sent to the investor mailing list, the FinOps lead will add the current investor update to the #investor-update slack channel, along with highlight commentary on GitLab's operating metrics.

#### Relevant Locations for Investor Update Process

##### Revenue Model & Metrics Tab within the Financial Model

**1: Sales Metrics 2019 Monthly Metrics**
* Gross New IACV → New IACV (Revenue Model)
* Gross Growth IACV → Growth IACV (Line 14 Revenue Model)
* Expiring Subscriptions gets updated once every 3 months
* Won Renewal ACV → Renewal ACV (Line 8 Revenue Model)
* Lost Renewals ACV + Growth Downgrade IACV + Growth Refund IACV + New Refund IACV + New Downgrade IACV → Lost Renewals, Credits, Downgrades (Line 11 Revenue Model)
* Update new month with % of Lost Renewals, Credits, Downgrades
* TCV --> filter out Professional Services, sum all relevant information, & in Revenue Model paste number in Line 34 for TCV
* Double check to see if Revenue model Net IACV = Net IACV in Sales Metrics Sheet
* Note: ACV on Sales Metrics Sheet will not equal ACV on Revenue Model because of Professional Services in the Sales Metrics Sheet

**2: Sisense**
* Exit ARR --> Filter data by current MRR month & then sum using (=subtotal(109, ARR column))
* Retention
* Licensed Users --> Make sure EDU & OSS are excluded
* Active Hosts
* Customer Count By ARR

**3: Metrics Report (SFDC)**
* Gives an overview of what happened within the stated month

**4: All Pipeline Report (SFDC)**
* Relevant number is the Grand Total Sum
* Make sure the month is correct

**5: Late Stage Report (SFDC)**
* Relevant number is the Grand Total Sum
* Make sure the month is correct

**6: Orders Processed Report (SFDC)**
* Ensure that closed deals are updated for the month (Bottom of Funnel Forecast Line 56 - 64)

**7: Revenue Spreadsheet sent from Controller**
* Non-GAAP Revenue -> Pull grand total and input into Line 29 in Revenue Model

**8: Cash Flow Forecast (Accounting Will Send Over -- typically within the 4th of the Month)**
* Step 1: Find Ending Balance for Latest Month (Line 60)
* Step 2: Add to Financial Model Balance Sheet

**9: Pingdom**
* GitLab.com Availability, Response Time

**10: LTV for Metrics**
* LTV with DCF Tab (Line 116)
* Go back to the Metrics Tab (Current Month and 2 Months Prior -- get the average)
* Put that average number * .90 (GM) in Line 116
* Pull LTV number into Line 123 & input into Financial Model

**11: BambooHR**
* To get updated team members number -- check Income Statement Monthly
* If not updated within IS Monthly, go to BambooHR and pull actuals --> Put new numbers into IS Monthly (Lines 47 - 51)

**12: Zendesk**
* Step 1: Reporting --> Insights
* Step 2: Corp Dash - OKR Overview should have the relevant information for Customer Support
* Put them in the metrics tab (Line 8,9,& 10)

**13: Snowflake**
* Get GitLab.com Licensed Users --> Filter down by product category (Bronze, Silver, & Gold) --> Get grandtotal of each category and overall
* Additionally, Add Revenue for each Product in the GitLab.com Model --> Information comes from Revenue spreadsheet listed above (Number 7)

**14: Sales Pipeline Analysis**
* Model is being updated & TBD

#### Once Metrics & Financial Model Are Completed for Investor Update Process
* Add relevant numbers to the GitLab.com & GitLab Allocation Model
* Copy All of Metrics Tab in the Financial Model & Paste it in Static Metrics Sheet
* *Note:* Remember to remove forecast, format new month to actual (from orange to light blue), copy/paste hyperlinks for names within static sheet, & anything in the previous month that was italicized needs to be back to normal.

****

### Budgeting @ GitLab

GitLab uses several different methods to budget specific revenue and expenses. During the Rolling 4 Quarter Forecast planning process GitLab may use one of the following methods to allocate revenue or expenses.

#### Spreading

Recording activities based on the percentages across a certain time period. This can either be on a percentage spread or an amount spread

#### One Time Adjustments

Recording activities based on a one time basis across a certain time period. These activities are geared toward, but not limited to, marketing expenses.

#### Headcount Growth (+/-)

Recording activities based on headcount growth across a certain time period.

****

## Headcount and the Recruiting Single Source of Truth

The Finance team is the owner of the GitLab Hiring Plan (GHP), which is the SSOT for GitLab’s hiring plan for the future rolling 12 months. The GHP is a live document that is maintained by the Finance Business Partners (FBPs) and contains the current hiring plan that is used in the GitLab Financial Model. We have a SSOT to ensure there is one place hiring managers, finance, and recruiting go to see what hiring is included in the forecast and is eligible to be opened up in Greenhouse.  This increases our predictability as a company and streamlines the hiring process.


### Hiring Managers

When you are ready to open a new vacancy, access the GHP and look for your role on your FBP’s tab in the GHP.

Each role has a unique GHP ID number listed, which is part of the information you need to provide to the [Recruiting Manager or Lead](/handbook/hiring/recruiting-alignment/#recruiter-coordinator-and-sourcer-alignment-by-department) for your department to have the position opened in Greenhouse.  If there is no GHP ID, the job will not be opened in Greenhouse.

Due to the territory alignment related to Sales headcount, please reach out to the Sales Strategy & Analytics team before reaching out to your Finance Business Partner.

If you can’t find the GHP ID for the particular role or you want to make a change, you need to contact your Finance Business Partner.  Some examples of changes that require contacting your Finance Business Partner are; a different job family, change in level, different location requirement, change in forecasted start date, and swapping out different forecasted job openings.  When in doubt ask your Finance Business Partner.

### Recruiting Managers and Leads

If a hiring manager comes to you to open a role and they do not have a GHP ID, please direct them to contact their FBP to work with them on getting a GHP ID. If a Hiring Manager provides a GHP ID to you and it does not match what you see on the GHP, check with your FBP before opening the role in Greenhouse so that the FBP can work with the Hiring Manager to make adjustments to the forecast and the GHP.

### Finance Business Partners

### *Maintaining the GHP for Finance*

On a weekly basis FBPs will update their department’s hiring forecasts using the R4QF - Financial Model Feed - GitLab Team file, which flows into the GitLab Financial Model and into the GHP. FBPs may update their headcount forecast on a more frequent basis depending on their individual department’s business needs, but at a minimum it must be done on a weekly basis. When updating, the FBP is to replace any rows that were future hires in the R4QF - Financial Model Feed - GitLab Team file that have been hired with the hired employees name, employee ID, and start date. The status column also needs to be updated to hired. The FBP then needs to update the R4QF - Financial Model Feed - GitLab Team file to reflect any changes to what is now in forecast.  This could require adding new roles, deleting roles, trading out roles, or adding backfills. The process is described in more detail below. Every vacancy requires a GHP ID.

The status column is important to keep up to date in the R4QF - Financial Model Feed - GitLab Team file. Any role that is marked new hire or backfill will pull into the GHP. Anything that is marked hired will not pull into the GHP. This is to ensure a clean working document for hiring managers, Recruiting, and Finance to reference.  

The following information must be included in the R4QF - Financial Model Feed - GitLab Team file because it flows into the GHP:

- GHP ID
- Hiring Manager (this can be a name or a position)
- Start Date
- Employee ID (once a person is hired)
- Name (once a person is hired)
- Job Title
- Division
- Department
- Level
- Region (if a specific one is required for the role, otherwise may be blank)
- Territory (Sales specific)
- Status (new hire, backill, hired)

### *Adding a vacancy*

To determine what GHP ID to use the FBP references the GHP. Each FBP has their own mapping tab that shows the used GHP IDs and the GHP IDs that are available. The numbering for the GHP IDs are similar to a credit card. The first two digits of the unique GHP ID represent the FBP’s division, the next two numbers represent the department. Then there are seven digits that start with 0000001 that sequentially grow from there for every role.  

When a new role is added, the FBP goes to their mapping tab in the GHP and finds the next unused number for the division department combination to use. Once a number has been used in Greenhouse for a job, it can not be reused. If the role is a future role and has been deleted, but was never input into Greenhouse, the FBP can use that number for it’s replacement or a different role since it was not used yet.

### *Backfilling due to leaving the company*

If there is a position that needs backfilling due to an employee leaving the company then the row the employee is on in the R4QF - Financial Model Feed - GitLab Team file is deleted and a new row is added to the department in the R4QF - Financial Model Feed - GitLab Team file for the backfill position. The status selected should be backfill and a GHP ID needs to be added to the row.

### *Backfilling due to internal movement*

If the employee is moving internally to another department and the position will be backfilled, then two GHP IDs are required so that the vacancy for the internal movement and the vacancy for the backfill can be opened in Greenhouse. The vacancy for the internal movement must be added to the R4QF - Financial Model Feed - GitLab Team file, which should include the employee’s name, employee ID, start date, new job title, updated division and department, and level. It should also include a GHP ID, so that Recruiting can open the role in Greenhouse. This could require two FBPs to sync, if the movement is to another Division. Each FBP owns their own Division, so please reach out to the FBP that the person is moving to, so they can add the role and GHP ID.  For the backfill due to the internal movement the process in the section above should be followed.

### *Backfilling due to leaving the company or internal movement, but employee staying until backfill complete*

If the employee is leaving the company or is moving internally but is waiting to do this until a backfill is hired, then they need to remain in the R4QF - Financial Model Feed - GitLab Team file. For this scenario, instead of adding a new row with the job to the the R4QF - Financial Model Feed - GitLab Team file the status beside the row that contains the current employee’s name and job position should be changed to backfill and a GHP ID should be added to that row for the vacancy opening in Greenhouse. This will cause the vacancy to show up in the GHP so that it can be hired for but will not double count a headcount in the financial forecast. Once the current employee has changed jobs or left the company, and if the vacancy has not yet been filled, the FBP adds another line to the R4QF - Financial Model Feed - GitLab Team file with the same GHP ID and changes the name to TBH, adds the forecasted start date, and deletes the prior row.

### *Reconciling*

When the FBP updates their headcount forecast and therefore the GHP, they will reconcile to the open jobs that are in Greenhouse and to the recent hires that are in BambooHR. To do this they will use a table that ties the two data sources together so they can reconcile based off of the GHP ID field to see what has been hired and what is in process. They will then update their headcount forecast with the new hires. If there is anything in Greenhouse that does not reconcile to the GHP and a change needs to occur, they will reach out to Recruiting and the hiring manager to let them know.

### *Approvals*

When a job is opened in Greenhouse it is routed for approvals. The second required approval is from Finance. This allows the FBPs another opportunity to check the GHP ID on open jobs and ensure everything reconciles to the GHP. If something does not reconcile to what is in the GHP, the FBP will reach out to the hiring manager to discuss. Ideally conversations with hiring managers and leaders will occur when the hiring manager sees that what they are requesting doesn’t tie to the GHP. But if the conversations do not occur then, they will occur at this time to ensure that everyone is in agreement and that if tradeoffs need to be made for financial reasons, they can be made then.

Finance is also a required approval on all job offers. This allows Finance to see the financial details of the job offer before any offer is sent, so that they are enabled to have conversations with their leaders about implications to their Plan if needed.

****

## Expense Controls and Improving Efficiency
1. The primary mechanism to ensure efficient spend of company assets is the [Procure to Pay](https://about.gitlab.com/handbook/finance/procure-to-pay/#procure-to-pay-process) process, and specifically completion of the [vendor and contract approval workflow](https://about.gitlab.com/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow) prior to authorization. The procurement team or your finance business partner can assist with questions related to this process.

1. The second mechanism is the budget vs actual review to determine reasons for variances vs plan.

### Vendor and Contracts Approvals Tracking for Marketing Programs Spend
1. The Marketing FP&A team maintains a status document containing the finance issues submitted by the marketing organization for approvals.
1. This dynamic list contains links to the issues, start dates, budgeted amount, approval status (y/n), and other pertinent details such as department. The list is kept current with updates on a weekly basis.
1. Please contact the Finance Business Partner for Marketing with questions concerning this information.
***


### Campaign Finance Tag Verification Process
****

The Finance Business Partner for Marketing will review transactions within the general ledger accounts for marketing program expense (6100’s) on a weekly basis to ensure that campaign tags are present. An exception report will be delivered to the Marketing Operations team either validating that all tags are present, or pointing out those transactions where the tag is missing. Marketing Operations will then work with the Marketing team to add back the missing tags.

A live dashboard to assist in identifying these transactions can be found in Sisense, but it is not linked here due to the sensitive nature of this data. The weekly desktop procedures for this process are as follows:
1. Monday morning- the financial analyst will use the Sisense dashboard to pull transactions from the 6100 Marketing account without campaign tags. This data will be transferred to a Google Spreadsheet and the link will be shared in the ongoing issue.
1. Wednesday- the Marketing Operations team will update the Google doc with the appropriate tags
1. Friday- the Accounting team will work through the new tags and get them appropriately accounted for

****

## Long-term model

GitLab maintains a financial model and communicates high level financials five years out.

#### Integrated Financial Model

GitLab's [integrated financial model](/handbook/finance/financial-planning-and-analysis/#gitlab-integrated-financial-model) is synonymous to its operating plan, but the integrated financial model is based soley on [business drivers](/handbook/finance/financial-planning-and-analysis/#business-drivers). The integrated financial model dynamcially answers questions like “What are our limits in comparison to the total addressable market?” and provides "What-if" scenarios. The integrated financial model also helps set the long term profitablity goals. GitLab has established business drivers in each function of the business and continues to refine them to ensure its tracking accordingly. The integrated financial model can also be considered a top-down plan.

#### Forecasting Growth Using a Growth Persistence Model

Scale Venture Partners developed a [Growth Persistence Model](https://www.scalevp.com/blog/predictable-growth-decay-in-saas-companies) based on research on private and public software companies. The Persistence Model shows that ARR growth rates typically decline year over year, with each year’s growth being, on average 85% to that of the preceding year. We use this methodology to set our growth targets.

### Business Drivers (also known as Gearing Ratios)

At GitLab, planning starts with long term goals. GitLab uses business drivers to forecast out long term goals by division. By using division specific business drivers that tie into the long term goals, GitLab is able to quickly and dynamically shift priorities based on external or internal changes.

Business drivers are the key inputs and activities that drive the operational and financial results of a business. [Business Drivers cited by CFI](https://corporatefinanceinstitute.com/resources/knowledge/modeling/business-drivers/)

## Long Term Targets

Our long term profitability target for EBITA (Earnings Before Interest Taxes and Amortization) is 20% of revenue.
Other financial planning targets are described in the divisional area (e.g. sales, marketing, development, etc.) in this handbook. We plan to achieve our long term profitability target as our revenue growth rate approaches 30%.
The long term target for operating expenses as a percentage of revenue for G&A is 10%.  
The long term target for R&D spend is 20%. We expect it to take longer to reach the long term target compared to typical companies as we plan to continue to invest in R&D to drive higher than average growth.
We have a large TAM at $60B and believe that high sustained growth rates will allow us to capture a larger percentage of market share compared to our competitors.

| Division | Long Term Target  |
|---|---|
| Cost of Sales  |15% of Revenue   |
| R&D  | 20% of Revenue  |
| Marketing  | 12.5% of Revenue  |
| Sales   | 22.5% of Revenue  |
| G&A  | 10% of Revenue  |
| **Total**  | **80% of Revenue**  |


## Where to find our Models?
1. GitLab Financial Model:  Google Drive -- FP&A >> Models >> GitLab Financial Model
2. Revenue Model: Google Drive -- FP&A >> Models >> Revenue Model
3. Capacity Model(s): Google Drive -- FP&A >> [FBP - Function Owner Folder, i.e S&M] >> Planning >> Capacity Model
4. Rolling 4 Quarter Forecast sheets: Google Drive -- FP&A>> Planning >> [Current Fiscal Year] >> [Current Fiscal Year] Plan >> [Quarter-Year thru Quarter-Year]]
5. Rolling 4 Quarter Forecast Feed sheets for Program Spend & Team: Google Drive -- FP&A>> Planning >> Financial Model >> Feed Sheet
6. Budgets vs. Actuals: Google Drive -- FP&A>> [FBP - Function Owner Folder, i.e S&M]>>  Budget vs. Actuals >> [Current Fiscal Year] >> Dashboard File

## How To Add A New Department into Models
1. Create new departments in each of the R4QF spreadsheets (Programs & People) feed
2. Make sure the new department is rolled up into [Department] tab onto both of the feeds. Right now, you would need to add ’New Department’A5:AF! to the [Department] tab because currently it’s only referring to current departments
3. Add new department to Headcount rollup
4. Make sure the new department is in the Compensation, Base Salary, Benefits, and T&E tabs of the Capacity model
5. The New Department needs to be added to the Account Based Department Budgets tab
6. Ping Financial Analyst in order for the new department to be added in the Allocation Accounts tab and Department Post Allocated Budgets tab in GitLab Financial Model
7. Everything else should flow through after that

For new department approvals and more detail on the process, please reach out on the #fpanda channel in Slack for desktop procedures.

## Department Structure

### Creating New Departments
Successfully creating new departments require that various company systems be updated to capture newly defined department structures. Once the need for a new department arises, follow these steps:

1. Create an issue using the *dept_change* template.
2. In the issue, add the appropriate team members from each department included in the checklist.
3. Disclose the nature of the new department. Is the new department simply a name change, or there is a structural modification?
4. Provide all detail necessary to ensure team members are assigned to the newly created departments.
5. Create a MR to update the [Cost & Reporting Structure](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/cost_center_division_department_mapping.csv) in the GitLab Data project with the new department. This ensures the new departments are in GitLab's data warehouse.

Below is a table showing the structure of GitLab departments as they exist in BambooHR and netsuite. Please [check out the Team Page](/company/team/org-chart) to see our org chart.

## Cost & Reporting Structure

At GitLab, the lowest level of tracking reporting structure starts with departments. For reporting and accounting purposes, departments are assigned both a cost center and a division.  In certain cases, a department may span multiple cost centers based on GitLab's allocation methodology.

The cost center, divison, and department reporting structure can be found in the Data Team repo located below. In efforts to keep a single source of truth for organizational and reporting purposes, the Data Team maintains any changes implemented by the FP&A team as a result of an organizational change.

[Cost & Reporting Structure](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/cost_center_division_department_mapping.csv)

#### 1. Cost Center

A grouping of departments within GitLab to which costs may be charged for accounting purposes.

#### 2. Division

A grouping of departments in which specific activities are carried out within GitLab. Aligning specific departments to divisions ensure the proper operations and efficiency for supporting and creating value for GitLab's customers.

#### 3. Department

An area of special expertise or responsibility within a GitLab division.

## Allocation Methodology

GitLab allocates cost items based on consumption of resources or headcount. Below are the workflow and diagrams that illustrate how various cost items are allocated:

### Workflow

#### Pre-Allocation P&Ls

The grid highlighted as `Pre-Allocation P&Ls` in the diagram below highlights what GitLab P&L structure would look like if there were no allocation methodology. The grid represents Cost Centers and Departments that fall within those Cost Centers. In order to support different dimensional P&Ls and ensure expenses are flowing to the proper Cost Center, GitLab applies an allocation methodology. Therefore, the `Pre-Allocation P&Ls` is meant to provide clarity and be a starting point to the allocation methodology process.

#### Allocation Methodology

The grid highlighted as `Allocation Methodology` in the diagram below highlights the specifc processes that occur in order to achieve proper expense allocations to their respective Cost Centers.

###### Step 1: Yellow Process

The allocation methodology is applied to the G&A Company Allocation department. Items that are recorded to the G&A Company Allocation department are software expenses and one off expenses. The software expenses are for software used by the entire company (examples include gmail and slack). At times one off expenses are also recorded to the G&A Company Allocation department (examples include Contribute, payroll service fees, etc).

While credit card processing fees are not allocated company wide, it is important to call out the distinction of applying credit card processing fees to Sales. GitLab does not consider credit card processing a cost to it's product or service. GitLab believes that its credit card processing fees are more appropriately classified as marketing and selling expense because these fees are related to the cash collection process for completing customer orders, which supports sales and marketing classification.

**Note:** One thing to note during the Yellow Process is the Infrastructure expenses are removed. Infrastructure is allocated during the next phase of the allocation cycle. Therefore no shared expenses will hit the Infrastructure P&L.

###### Step 2: Red Process

The allocation cycle for GitLab's SaaS offering [GitLab.com](https://gitlab.com) starts. The Finance Business Parter for R&D maintains an allocation model to provide Accounting with an allocation breakout of expenses as well as a P&L based on allocation. The model can be found by searching My Drive > GitLab.com Model. The allocation for GitLab.com is broken out into two different methods.


1. Free vs Paid Users
   - Infrastructure
   - 3rd Party Expenses [i.e. Hosting Services]

1. Percentage of Revenue
   - Customer Support

- Allocation for Infrastructure and 3rd Party expenses are based off of usage from free, paid and internal users of GitLab.
- Allocation of Customer Support are based off of expected revenue from GitLab's self hosted and SaaS offerings.

These allocation breakouts will be documented in the GitLab.com model and will flow into the Post Allocation P&Ls. GitLab allocates these expenses to 3 Cost Centers

1. Marketing - Free users
1. R&D - Internal users
1. Cost of Sales - Paid users


###### Step 3: Blue Process

The Blue Process, which encapsulates the Yellow and Red process, is meant to bring the full allocation cycle together before the Post Allocation P&Ls are rendered.

#### Post Allocation P&Ls

The grid highlighted as `Pre-Allocation P&Ls` in the diagram below highlights the final process during the allocation cycle. As a result, new P&Ls are generated to show the breakout of the Infrastructure team across Cost Centers as well as GitLab.com.

### Diagram

[![alt text](/handbook/finance/financial-planning-and-analysis/allocate.png "P&L Breakout")](https://drive.google.com/drive/folders/1hPZll6nWRk5waMTATJV9f4TojCSTxpCI)

## Misc

### Creating & Updating Models

If you create and/or update to a model that materially impacts it, please make sure to make a quick note of what you did in the [ChangeLog](https://gitlab.com/gitlab-com/finance/blob/master/changelog.md). This is important because it keeps us on all the same page.

**Note: At this time, GitLab use's the term `Division` to describe the makeup of its internal structure. In the future, the plan is to use the term `Function` to describe it's internal structure.**


### Updating Budget vs. Actuals
****

1. Open file: FP&A > Templates > Corporate F&A > Open 'Budget vs. Actual' Blank File GSheet
*  Please note that Budget vs. Actuals for all departments already exist within the FP&A Shared Drive
2. Open Netsuite and go to Budget vs. Actual report
*  Download the spreadsheet and copy & paste into the relevant month (e.g. For March 2019, paste into the March 2019 BvA Tab)
3. Open Netsuite and go to 'Income Statement Detail with JE name data' report
*  Copy / paste spreadsheet into the relevant month (e.g. For March 2019, paste into the March 2019 Detail Tab)
4. Once these spreadsheets are loaded in the spreadsheet, the numbers should auto-populate for the relevant months

## How to work with us

### On issues

Issues the FP&A team works on have the `~"FP&A"` label.  This applies to both the Finance and Analytics Issue Trackers. This will ensure that expectations are set accordingly and the work gets delievered in a timely and professional way.

The Analytics part of the FP&A team focuses on delivering project based works and at times will assist with ad-hoc analysis.

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate various status updates. There is currently 1 status update configured in Geekbot:

#### Weekly Status Update
The **Weekly Status Update** is configured to run at noon on Fridays, and contains three questions:

1. ***What progress was made on your deliverables this week?*** (MRs and Closed Issues are good for this)

    The goal with this question is to show off the work you did.

2. ***What do you plan to work on next week?*** (think about what you'll be able to merge or close by the end of the week)

    Think about what the next most important thing is to work on, and think about what part of that you can accomplish in one week. If your priorities aren't clear, talk to your manager.

3. ***Who will you need help from to accomplish your plan for next week?*** (tag specific individuals so they know ahead of time)

    This helps to ensure that the others on the team know you'll need their help and will surface any issues earlier.

#### Retrospectives

The FP&A Team holds a quarterly planning [retrospective](https://docs.google.com/document/d/1XeaPpMRskohFPLvevhPGETiIsMyXzCAdHJkAbdEFJZs/edit?ts=5d8a8dcf#) to discuss what went well and what didn't go well. The team will take action items away from the Retrospective to improve the planning process and overall efficiency at GitLab.

The FP&A Team uses GitLab's Engineering methodology to hold the Retrospectives, which are detailed on the [Team Retrospectives](/handbook/engineering/management/team-retrospectives/) page.

## Team Tips
#### Tips - Rolling 4 Quarter Forecast

- Meeting each department and function leader prior to the planning cycle is a must and useful for building long term relationships. Set up a coffee chat to do an informal intro.
- Having a Rolling 4 Quarter Forecast kickoff helps everyone know that the planning cycle is about to begin.
    - This should generally occur 3-4 weeks out before the planning cycle starts and should be communicated through as many channels as possible (Slack, Team Calls, Company Call, etc)
- Scheduling invites 3-4 weeks out with department and function leaders around the same time as the kickoff is important. This helps get on everyone's calendar and ensure topics are not rushed
    - Having a templated calendar invite helps save time. There will be multiple folks involved in the process, broken out by each function.
    - It also helps if the Rolling 4 Quarter Forecast models are up to date prior to meeting. This would include the current team, planned hires, and program spend so leaders have a clear guide during the meeting. It won't be perfect and will inevitablty change during or shortly after the meetings, but this is an efficient way to make the meetings productive.
    - Meeting with the function leads after the department leads help tie everything together. However, if possible, it can be beneficial to meet with the function leads before and after to ensure buy in and close any gaps.
- When applicable, it is important to always include the function operation leads to the meetings so they can help faciliate any work or changes during or after the meetings.
- Continously gather feedback from stakeholders on what can be done more efficient
- Planning is a team sport. It requires effort and communication on all sides to make it work well. Build the relationships, be patient, and be humble as some folks have less experience with planning while others have a lot of experience.

**Note: During the last quarter of the year, GitLab runs a 5 quarter forecast that aligns with it's annual planning efforts.**
