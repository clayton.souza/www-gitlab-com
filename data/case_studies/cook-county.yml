title: "Cook County Assessor’s Office"
cover_image: '/images/blogimages/cookcounty.jpg'
cover_title: |
  How Chicago’s Cook County assesses economic data with transparency and version control
cover_description: |
  Chicago’s Cook County Assessor’s Office adopted GitLab to provide transparency and accountability with real-time data in a single source of truth.
twitter_image: '/images/blogimages/cookcounty.jpg'
twitter_text: 'Cook County’s Assessor’s office adopted GitLab in an experiment to transform a government office with transparency and agility.'
customer_logo: '/images/case_study_logos/logo_cook-county-assessors-office-colo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Government
customer_location: Chicago, IL
customer_solution: GitLab Silver
customer_employees: 270
customer_overview: |
  Cook County’s Assessor’s office adopted GitLab in an experiment to transform a government office with transparency and agility.
customer_challenge: |
  With taxpayers’ best interest in mind, Cook County needed a data management platform for secure and accurate property assessments.

key_benefits:
  - |
    Single source of truth
  - |
    Public digital transparency
  - |
    Improved version control
  - |
    Educated forecasting economic behaviors 
  - |
    Government transformation

customer_stats:
  - stat: 1.8
    label: Million properties assessed
  - stat: 31
    label: Repositories, more than 1400 commits
  - stat: $270 
    label: Billion estimated market value

customer_study_content:
  - title: the customer
    subtitle: Assessing property for Cook County
    content:
      - |
        Chicago’s Cook County Assessor’s Office (CCAO) works to predict the value of 1.8 million real estate properties in the area, including residential and commercial properties. [The Assessor’s Office](https://www.cookcountyassessor.com/about-cook-county-assessors-office) is headed by Assessor Fritz Kaegi and a staff of about 240 people who serve the public to establish a uniform, fair, and accurate property assessment. 
      - |
        The values set on real estate are used as a basis for levying taxes and determining property tax distribution among taxpayers. The CCAO reassesses properties every three years and evaluates one-third of the county each year. In any given year, they’re reviewing between 400,000 to 600,000 properties. The office leverages property characteristic data from residential sales to create the updated projected market value.

  - title: the challenge
    subtitle: Lacking data management and version control
    content:
      - |
        Record keeping is an essential part of the CCAO. Legacy scripts, the inability to integrate older software, and lack of assistance from previous officeholders presented significant challenges to ensuring an uninterrupted, timely reassessment process. On top of that, records were primarily paper-based and only recently have software development programs been used. The software development scripts in place had zero version control and no commentary, so understanding the data after the fact was nearly impossible.
      - |
        The CCAO wanted to implement replicable and reportable software algorithms in order to have a single source of truth for future data. Ideally, the tool would be agile and dynamic in a rapidly changing environment, but would also be discernible to those with a similar technological background. For those who did not, the data would be used as the basis of an easy-to-understand reporting structure.
      - |
        The goal was to create a more [transparent digital platform](/blog/2019/09/02/creating-a-transparent-digital-democracy/) for property owners to view and understand how assessments are established. This would provide stakeholders with comprehensive data about assessments in a way that previously didn’t exist.

  - title: the solution
    subtitle: Creating a single source of truth and transparency
    content:
      - |
        The current Cook County Assessor, Fritz Kaegi, and his team wanted to restore trust in how the government office operated and believed transparency was the basis for rebuilding that trust.
      - |
        “[Our office] ran on a platform of fair, ethical, and transparent assessments. In order to achieve that third pillar, we absolutely have to publish the code that we use to value (a) house,” said Robert Ross, chief data officer at the Cook County Assessor’s Office. To change the narrative, the team adopted GitLab when they entered office
      - |
        There are a number of variables in assessing property values. Code variables are central to Cook County’s assessment, which makes it imperative that the data is made public. Ross and his team now publish all the code variable information in GitLab, making it accessible for public consumption. 
      - |
        Cook County has established a single source of truth that can be updated in real-time. GitLab manages the code transparently and requires little maintenance from data officers.
   
  - blockquote: With GitLab, I can produce research and show people in the office. They suggest changes to the research and I can make those changes without having to worry about version control and saving my work. One repository makes it so that I can focus more on the actual work and less on the mechanics of working.
    attribution: Robert Ross 
    attribution_title: Chief Data Officer

  - title: the results
    subtitle: Changing the way government operates
    content:
      - |
        The CCAO did an end-to-end audit of the legacy valuation process and replicated all the previously manual processes. In doing so, they improved the process where they saw errors and now publish all the code in GitLab.
      - |
        There are four repositories, with over 1,400 commits. People can now download the data and [run the code](/blog/2020/07/03/challenges-of-code-reviews/) off the data. The office has had at least 6,700 people download these large datasets. The data officers use GitLab’s Git history, issue tracker, and milestones, and document every project in real-time. 
      - |
        Property owners can now access and own the information that creates their home’s value. Access at this level has never been done before. “No county assessor has ever used a public-facing repository for their work,” Ross said.
      - |
        Establishing governing policies has primarily always been done behind closed doors. Cook County is using GitLab to take an experimental step towards open source government policies. 
      - |
        The level of transparency now includes documenting any mistakes and fixes. The CCAO openly tracks mistakes they have made by creating issues that showcase the errors and then commits to publicly announce how these errors will be corrected. “Accepting mistakes and righting them completely diffuses battles and is no longer a political issue,” Ross added.
      - |
        Data is vital to predicting values and forecasting economic behaviors. At the CCAO, data scientists are part of the team that creates public policy, in that they follow the data to find physical evidence for planning. “We are publishing code that is the basis of your assessment. We think your home is worth $500,000 and if you want to know why, here is the code that produced that estimate,” Ross said.
 
  - title: the impact
    subtitle: When the world changes, how do you respond?
    content:
      - |
        The team calculated the 2020 residential property assessments in January 2020, unaware of the impacts that COVID-19 would have on a global scale. By February, reassessments had been completed and sent to residents in four townships. In March, Governor J.B. Pritzker declared a state of emergency for all counties in Illinois as a result of the virus. The declaration had an immediate impact for the CCAO. The office had two choices: 

          * Do nothing and proceed as planned with residential values and mail out assessments.
          * Do something and adjust residential values as a result of COVID-19 impacts.

        After deciding to take action by adjusting values with [COVID-19 impacts](/blog/2020/05/15/startup-covid-tracking/) in mind, the CCAO’s staff realized this problem wasn’t going to be easy to solve. Estimating market values of properties requires knowing the sales information of similar properties. Residential sales data has an inherent two-month processing lag before it is published, and the team needed up-to-date information and couldn’t wait. The stay-at-home orders also halted sales and created a lapse in data. 
      - |
        The team used market signals, historical knowledge that residential prices fall when unemployment rates rise, census data around the composition of workers per sector, and current unemployment data to create COVID-19 adjustments to home values. Using data around employment by industry, the team predicted where in the county the greatest impacts were going to be felt and made adjustments accordingly. 
      - |
        “What we did was actually a challenge that every state government had to face. Policymakers across the country found themselves in a position of making a decision within the absence of all of the data that they were accustomed to usually having to make such decisions,” Ross said. 
      - |
        In February 2020, the local unemployment rate was 3.4%, and by April it spiked to 17.5%. GitLab repositories hosted the raw data to create the analysis around how to adjust residential valuations. The code in GitLab helped them generate a vector of adjustments to make. The CCAO is able to stay up-to-date on property assessments with version control, a single source of truth, and a transparent code management system.   
