---
layout: markdown_page
title: "Gartner Peer Insights 'Voice of the Customer': Application Release Orchestration 2020"
---
## Gartner Peer Insights 'Voice of the Customer': Application Release Orchestration 2020*

Gartner's Peer Insights' 'Voice of the Customer' synthesizes hundreds of reviews and ratings from the perspective of peers to help IT decision makers choose the best solution them. We're excited to be recognized as a "Customers' Choice" for Application Release Orchestration based on the qualifications set by Gartner Peer Insights. 

See how Gartner Peer Insights rates GitLab overall, our Application Release Orchestration capabilities in relation to the market, and why it's valuable below.

![Gartner VoTC2020](/images/analysts/votc-aro-2020.png){: .small}

This graphic was published by Gartner, Inc. as part of a larger research document and should be evaluated in the context of the entire document. The Gartner document is available upon request by clicking the link on this page.
{: .note .font-small}

### Why is this important?

Peer-based reviews provide important data points from users who have hands on experience with a specific solution. They are most beneficial when paired with market research from experts and provide a different perspective for decision makers that are looking to find the right solution for their business, in this case ARO. Peer reviews give more detail into lessons learned from end users accounting for personal experiences that are valuable to form an educated opinion when going through a buying process. Another key benefit is the ability to find and sort through reviews based on specific demographics, whether it's around a particular technology, size of an organization, industry, or geographic location.


### Key results in the publication:

Reviewers evaluated eligible vendors based on a variety of criteria used to create high level comparisons. The comparisons cover "overall vendor rating,"willingness to recommend," and "product capabilities" in one group. The other group of comparisons covers "evaluation and contracting," integration and deployment," as well as "service and support."  Here are the takeaways for these two groups:

**Overall rating, willingness to recommend, and product capabilities**

GitLab led the way in total number of responses for each of these criteria, showing that our end users have a high degree of confidence in expressing their opinions and experiences. No other vendor had greater than 94 responses for the groups below.
* Overall rating: GitLab rated a 4.6 out of 5 with 192 responses, tied for the highest overall rating among included vendors.
* Willingness to recommend: 95% of respondents said they were willing to recommend GitLab for ARO out of 192 responses.
* Product capabilities: GitLab rated 4.6 out of 5 for ARO capabilities with 190 total responses.


**Evaluation and contracting, integration and deployment, service and support**

GitLab also received the highest number of responses for this set of criteria for each group.
* Evaluation and contracting: GitLab rated a 4.5 out of 5 with 127 responses.
* Integration and deployment: Just below the highest rating of 4.7, GitLab rated a 4.6 out of 5 with 175 responses.
* Service and support: GitLab rated 4.5 out of 5 with 169 responses, again tied for the highest score among eligible vendors.

Taking into account all of the data above, we're extremely pleased to see a large number of total responses because it helps indicate a more trustworthy summary rating. Along with high ratings for each individual criteria group, this resulted in our designation of "Customers' Choice."

![ARO quadrant image](/images/analysts/AROcustomerchoice.png){: .small}



#### Peer Review Highlights

Here's what a few GitLab users have to say:

**5 Star Review: Great Release Management, Fits With Continuous Delivery Pipelines**
> "Great application to manage releases, do code reviews, and request merges. We integrated it in our continuous delivery process..."

**5 Star Review: Amazing Version Control and Release Manager**
> "Our release system works amazingly with GitLab. With it, we can use branching strategies to control our releases so we don't push wrong code to production."

**5 Star Review: GitLab Is An Invaluable Tool For Deploying Our Applications Fast**
> "GitLab is an invaluable tool for not only storing our codebase, but also deploying code and managing the entire lifecycle of our application."

You can find the full list of GitLab Peer Insights reviews [here](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/reviews?sort=-helpfulness).


### The GitLab Application Release Orchestration vision

At GitLab, we're continuing to invest in [Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/) as a core part of our overall [Release stage vision](https://youtu.be/pzGCishRoh4) in both the short term and long term. We're constantly iterating to improve existing capabilities, adding new features that help coordinate complex releases, and looking for additional ways to replace manual work with automation wherever possible. Long term, we're focused on improving overall usability, maturity and nimbleness when orchestrating releases in GitLab, with an emphasis on making release management easier across multiple projects. 


Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s research organization and should not be construed as statements of fact. Gartner disclaims all warranties, express or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.
{: .note .font-small .margin-top40}

Gartner Peer Insights 'Voice of the Customer': Application Release Orchestration, March 2020
{: .note .font-small .margin-top40}
