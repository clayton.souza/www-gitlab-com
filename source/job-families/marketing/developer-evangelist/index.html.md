---
layout: job_family_page
title: "Developer Evangelists"
---

## Developer Evangelist

As a Developer Evangelist, you will connect with other developers, contribute to open source, and share your work externally about cutting-edge technologies on conference panels, meetups, in contributed articles and on blogs. Your work will foster a community inspired by GitLab and will drive our strategy around developer love and GitLab’s participation in the open source ecosystem.

As the Technical Evangelism team, we collaborate and connect with communities that love technology and open source as much as we do. Our team interacts with developers across the globe at conferences as well as online, and co-creates with the open source community on the most impactful projects in the ecosystem.

Our focus is on generating awareness about GitLab by rolling up our sleeves, contributing to the ecosystem, and enabling others to become evangelists outside the company as well. Not afraid to be hands-on, you might write sample code, author client libraries, and work with strategic GitLab partners such as the Heroes, users, and customers to spark and engage our developer communities.

### Job Grade

The Developer Evangelist is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Lead the conversation around the latest technology advancements and best practices in the developer community at in person and online venues
* Contribute to relevant open source projects, foundations, and SIGs in order to give GitLab a voice and front seat access to the developments in our space of interest
* Channel information back to product and engineering about your learnings being an active contributor in the community
* Reach mass developers by creating unique content that educates the ecosystem and brings reflected glory to GitLab
* Conduct interviews with media via phone, podcasts, video and in-person
* Be a force in the community and never compromise on the tech!

### Requirements

* In-depth industry experience building software and contributing to open source in the cloud computing ecosystem
* At least 1 year of experience giving talks and developing demos, webinars, videos, and other technical content
* Meaningful social presence with engaged followers
* Ability to manage the fast moving conference schedule with its CFP deadlines and show dates
* Self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Ability to travel up to 40% of the time
* You share our values and work in accordance with those values.
* Ability to use GitLab

## Senior Developer Evangelist

### Job Grade

The Senior Developer Evangelist is a [grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

* Same as Developer Evangelist plus,
* 2-3 year experience giving talks and developing demos, webinars, videos, and other technical content to audiences of 300 and larger
* Experience serving as a media spokesperson

## Staff Developer Evangelist

### Requirements

* Same as Senior Developer Evangelist plus,
* Hold positions of influence in open source projects and organizations such as SIG leads, maintainer status, author status
* Social following of 10k+ followers or equivalent
* Experience giving talks and developing demos, webinars, videos, and other technical content as keynote speaker

### Job Grade

The Staff Developer Evangelist is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Manager, Developer Evangelism

As the Manager, Developer Evangelism, you will be responsible for building our technical evangelism function with the ultimate objectives of:

* Driving awareness of GitLab as the single application for the entire DevOps cycle
* Amplifying GitLab's Cloud Native thought leadership in DevSecOps
* Beginning conversations about GitLab as the leading cloud-agnostic player
* Inspiring and empowering our community to become developer evangelists
* Increasing GitLab excitement among developers and SREs

### Job Grade

The Manager, Developer Evangelism is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Define and execute the Technical Evangelism vision and strategy. Collaborate to align it across teams
* Identify opportunities and build the resources to equip the GitLab team and the wider community to become technical evangelists
* Create, report and iterate on the relevant key performance indicators to effectively measure the impact of GitLab's technical evangelism initiatives
* Mentor, guide, and grow the careers of all team members
* Build and continually evolve the team's processes to make them more effective
* Enable the Technical Evangelism team to produce and execute their quarterly OKRs
* Develop a hiring plan according to the dynamic needs of a rapidly growing organization

### Requirements

* You have 3-5 years of experience running a developer relations program, preferably open source in nature.
* You have 2+ years of experience leading a team of developer evangelists/advocates.
* Broad knowledge of the DevOps landscape and key players. Experience and connections in the Cloud Native ecosystem are a plus.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
* Ability to use GitLab
* You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

## Specialities

Read more about what a [specialty is at GitLab](/company/team/structure/#specialist).

### Developer Evangelist, Kubernetes

#### Requirements

* Experience using container technologies in general and Kubernetes in particular
* Contributor to Kubernetes or related projects such as Helm

### Developer Evangelist, CI/CD

#### Requirements

* Experience running or building CI/CD systems
* Contributor to a CI/CD project such as GitLab runners, Tekton, Spinnaker, Jenkins

### Developer Evangelist, GitLab

#### Requirements

* Deep knowledge of the GitLab product, particularly its CI/CD and Secure functionalities
* Ability to create engaging demos and other content based on GitLab that tell a story

### Developer Evangelist, Ecosystem

#### Requirements

* Deep connections in the cloud native ecosystem with demonstrated involvement in community events and projects
* Experience driving influencers to events, content, and other experiences
* Ability and experience nurturing community members and convert them to champions
* Partnerships experience to push relevant technical initiatives through the finish line keeping in mind holistic campaigns such as KubeCon events

### Technical Evangelism Program Manager

**Mission:** Build a powerhouse technical evangelism program at GitLab that encompasses teammates and the wider community. Create a program to increase GitLab’s awareness in the wider community.

As part of this role you will have the opportunity to work closely with deeply technical leaders and marketing experts. You will be coached to develop your own technical thought leadership platform and also have the opportunity to grow your career as a technical marketer.

#### Job Grade

The Technical Evangelism Program Manager is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Develop speaker’s bureau database that includes speakers and thought leadership platforms
* Manage the conferences' Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions
* Review slide decks and develop a speaker training program
* Develop slide deck formats for speaker’s that include slides of interest to GitLab campaigns
* Support technical evangelism content development in line with thought leadership platforms
* Liaison with PR and content to execute on evangelism campaigns, as well as extend the effort of recorded talks at various events
* Work hand-in-hand with the community relations team to extend the evangelism program’s reach.
* Define metrics of measurement with the Tech Evangelism Director
* Instrument for metrics of measurement and report on them regularly
* Build thought leadership in GitLab product areas over time
* **Senior Responsibilities:**
  * Uplevel the speaker's bureau to an influencer bureau and run the program with all modes of content creation in mind - speaking, technical demos, blog posts, media, etc.
  * DRI integrated campaigns with other GitLab teams to achieve the above
  * Create product <> tech evangelism liaison plan for future technical evangelist hires with Director of Tech Evangelism

#### Requirements

* Excellent written and verbal communication skills
* Background in or deep curiosity about the cloud computing and devops ecosystem
* Track record of success in a software company
* At least 3-5 years work experience in a fast paced working environment
* Exceptional organizational skills
* Technical skills are a plus
* Relationships in the software DevSecOps space are a plus

## Performance indicators

* Impressions per month. Number of combined impressions/organic views from the TE team, from a designated number of sources (e.g. social, events, content, etc.)
