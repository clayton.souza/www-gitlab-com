---
layout: job_family_page
title: Senior Manager, Engineering
---

Managers in Engineering at GitLab see their team as their product. While they are technically credible and know the details of what engineers work on, their time is spent safeguarding their team's health, hiring a world-class team, and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals. Engineering Leadership at GitLab is cross-discipline. A Senior Manager, Engineering will manage both [Frontend Engineering Managers](/job-families/engineering/frontend-engineering-manager/) and [Backend Engineering Managers](/job-families/engineering/backend-engineer/#backend-manager-engineering).

#### Job Grade

The  Senior Manager, Engineering is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Manage up to six engineering teams
* Conduct managerial interviews for candidates, and train engineering managers to do said interviews
* Generate and implement process improvements, especially cross-team processes
* Hold regular [1:1s](/handbook/leadership/1-1/) with team managers and skip-level 1:1s with all members of their team
* Management mentorship

#### Requirements

* Technical credibility: Past experience as a product engineer and leading teams thereof
* Management credibility: Past experience (3 to 5 years) as an engineering manager
* Ability to understand, communicate and improve the quality of multiple teams
* Demonstrate longevity at at least one recent job
* Ability to be successful managing at a remote-only company
* Humble, servant leader
* Ability to use GitLab

#### Nice-to-haves

* Be a user of GitLab, or familiar with our company
* Prior Developer Platform or Tool industry experience
* Prior product company experience
* Prior high-growth startup experience
* Experience working on systems at massive (i.e. consumer) scale
* Deep open source experience
* Experience working with global teams
* We value diversity, inclusion and belonging in leadership
* Be inquisitive: Ask great questions

## Levels

* Senior Manager, Engineering
* Director, Engineering
* Senior Director, Engineering
* VP, Development

## Performance Indicators

* [Hiring Actual vs. Plan](/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan)
* [Team/Group MR Rate](/handbook/engineering/development/performance-indicators/#mr-rate)
* [Handbook Update Frequency](/handbook/engineering/development/performance-indicators/#handbook-update-frequency)

## Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with a VP of Development
* Next, candidates will be invited to schedule a 45 minute second interview with a Director of Engineering
* Next, candidates will be invited to schedule a 45 minute third interview with another member of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with a member of the Product team
* Next, candidates will be invited to schedule a 45 minute fifth interview with a VP of Engineering
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
