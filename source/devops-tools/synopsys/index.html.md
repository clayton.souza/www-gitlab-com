---
layout: secure_and_defend_competitors
title: "GitLab vs Synopsys"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

## Summary

Synopsys owns a portfolio of several scanning tools, including Coverity (SAST), Black Duck (SCA), Seeker (IAST), Defensics (Fuzzing), and recently acquired Tinfoil (DAST).

* BlackDuck does Software Composition Analysis (SCA) including dependency scanning, container scanning, and license management.

* Coverity for SAST includes spell-checker-like capability with an IDE plug-in that alerts the developer to vulnerable phrases as they code. It also has a dashboard that pulls in IAST from Seeker for a unified view. Coverity covers 20 programming languages. Our research indicates that Coverity costs around $12k USD per year for 5 users.

* Seeker for IAST employs an agent to test the application for vulnerabilities. It is used during functional testing so that security tests are done in the normal course of other testing. Seekers has an API to integrate with Dev IDEs. Seeker works with Java/all JVM languages. Seeker is only available on premise.

## Comparison to GitLab

Although Synopsys can integrate with IDE's and DevOps tools via the API, complete testing coverage requires multiple software licenses and a heavy integration and maintenance effort.  GitLab Ultimate automatically includes a full suite of broad security scanning with every code commit.  GitLab's scan results are provided to the developer inline in their Merge Requests with no integration required.

GitLab security scanning includes not only SAST but also DAST, Container and Dependency scanning, License Compliance scanning, and Secrets detection. All of these are included in GitLab Ultimate and integrated directly into the developer's workflow.  Finding vulnerabilities is only the beginning. Delivering those findings to the developer for immediate remediation is key to shifting left to reduce both cost and risk.

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-defend strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Synopsys</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Licensing and management is much simpler as it is all included in a single tool</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No special integration is required to surface vulnerabilities as part of development’s regular MR workflow</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Widespread recognition as a security testing leader by Gartner and across the industry</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strong managed services offering for customers who are looking to outsource their security scanning</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Polaris Software Integrity Platform provides a single console to manage all of Synopsys’ testing products</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Good integration with IDEs and local developer environments</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab is much newer to the security testing space and does not yet have the feature depth or market recognition held by Synopsys</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No managed services offering</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Poor visualization of vulnerabilities and limited fix recommendations</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each kind of testing is a separate piece of software that must be licensed and integrated with the DevOps lifecycle separately</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Professional services engagements are pushed by sales and can be costly for customers</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-defend" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-defend feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Synopsys</b> |
| --- | :-: | :-: |
| SAST | &#9989; | &#9989; |
| DAST | &#9989; | recently acquired a startup |
| IAST | | &#9989; |
| SCA: Vulnerability Scanning | &#9989; | &#9989; |
| SCA: Open Source Audit | &#9989; | &#9989; |
| Fuzz Testing | &#9989; | &#9989; |

</div>
