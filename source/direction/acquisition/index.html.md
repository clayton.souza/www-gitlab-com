---
layout: markdown_page
title: Product Direction - Acquisition
description: "The Acquisition Team at GitLab focuses on running experiments to increase the paid adoption of our platform at the point of signup. Learn more!"
canonical_path: "/direction/acquisition/"
---

### Overview

The Acquisition Team at GitLab focuses on running experiments to increase the paid adoption of our platform at the point of signup, we strive to promote the value of GitLab and accelerate site visitors transition to happy valued users of our product. Acquisition sits at the beginning of the journey for individual users and organizations as they get their first introduction to the value proposition of GitLab. Our goal is to ensure that users understand the unique value that GitLab provides and thereby ensure that prospects seamlessly transition into healthy, valued users of GitLab. We will gain a deep understanding of the value that our users realize from the different Stages of GitLab, and ensure that value is susinctly communicated to our site visitors in order to efficently transition them from prospects to active, valued users.

**Acquisition Team**

Product Manager: [Jensen Stava](/company/team/#jstava) | 
Engineering Manager: [Jerome Ng](/company/team/#jeromezng) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Emily Sybrant](/company/team/#esybrant) | 
Full Stack Engineer: [Alex Buijs](/company/team/#alexbuijs) | 
Full Stack Engineer: [Nicolas Dular](/company/team/#nicolasdular)

**Acquisition Team Mission**

*   To promote the value of GitLab and accelerate site visitors transition to happy valued users of our product.

**Acquisition KPI**


*   Direct signup ARR growth rate
*   Definition:  (Direct signup ARR in the current period - direct signup revenue in the prior period)/direct signup revenue in the prior period
* The Analysis can be found in the [Product KPIs Dashboard in Periscope](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=7634169&udv=0)
* Calculation: 
  * For SaaS, Direct signup ARR includes all namespaces that start a subscription for the first time, during the first 24 hours after the namespace creation.
  * For Self-Managed, we link all subscriptions to their Salesforce accounts and leads. We include in Direct Signup ARR all subscriptions which have a lead or an account created up to 24 hours before the subscription start date. 

_Supporting performance indicators:_

1. Time to sign up
   - Definition: Time of account signup completion - Time of first visit
2. Percent of paid conversion 
   - Definition: Total number of unique site visitors / Total number of  paid signups
3. Percent of conversion 
   - Definition: Total number of unique site visitors / Total number of signups
4. Free to paid conversion
   - Definition: Free to paid conversion rate (free user upgrades / free user signups in period)
5. Time to upgrade
   - Time of account signup completion - Time of upgrade to paid package

### Problems to solve

Do you have issues... We’d love to hear about them, how can we help GitLab contributors find that ah-Ha! Moment. 

Here are few problems we are trying to solve: 


*   **Clarity** - Whose problems do you solve?
    *   Your product does A LOT, the feature set is expansive, but I can't tell if it will work for me.
*   **Call to Action** - I want to signup! What is the best way to do that? 
    *   I can't tell which offering of GitLab I should signup for? 
    *   What package should I signup for?
*   **Usability** - Signing up for a paid package is a cumbersome process...
    *   As a dev-ops lead, how do I buy this product for my team?
    *   How do I signup for a paid package from my self hosted instance?


### Our approach

Acquisiton runs a standard growth process. We gather feedback, analyze the information, ideate then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. The team is dedicated to improving GitLab’s ability to convert visitors from about.gitlab.com into healthy, valued users of the product. Given GitLab’s commitment to [iteration](https://about.gitlab.com/handbook/values/#iteration) and the [Growth Groups](https://about.gitlab.com/direction/growth/#acquisition-conversion-expansion--and-retention-groups) test and learn strategy, the Acquisition Team will break down each incremental improvement in two distinct and important ways:



1. Each item in our roadmap will be broken into an [MVC](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc)
2. Each item in our roadmap will be rolled out incrementally and measured against a control

Though this framework is not new to GitLab, it is especially important to note for this roadmap. Many of the items listed below have the **potential** to create negative downstream impacts to revenue and potentially a users experience with our product. While we obviously seek to avoid those cases, we believe that we should take controlled risks to quickly learn from our users behavior rather than increasing the scope of these tests to avoid all negative edge cases. That said, we will document any possible negative after effects resulting from each test, as well as a plan to monitor and potentially mitigate those negative effects. Moreover, we will call out an appropriate test rollout plan given the risk associated with running each test. That said, our prioritization will take those potential negative effects into account, and will reflect what we believe to be the highest potential impact to our growth KPI as possible given an [ICE score](https://about.gitlab.com/direction/growth/#growth-ideation-and-prioritization). Happy learning!


### 🚀 Current Priorities

<table>
  <tr>
   <td>ICE Score
   </td>
   <td>Focus
   </td>
   <td>Why?
   </td>
  </tr>
  <tr>
   <td><a href="https://docs.google.com/spreadsheets/d/1K7jfIciCso1-xHMwXRxaRjug3sBINop0v0vkFFz44ao/edit?usp=sharing">7.0</a>
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/87">SAAS Paid Sign Up Experience</a>
   </td>
   <td>As a user, when I sign up for a paid package, the process is convoluted, complex and confusing. I don't understand why I have to create 2 accounts in order to signup and pay. I would rather try to figure out how to use the product for free rather than invest my time trying to figure out how to pay.
   </td>
  </tr>
  <tr>
   <td><a href="https://docs.google.com/spreadsheets/d/1MaSkEOjSHJMbpC9gcEyyCj9FR3IccifcHrq90n_acrQ/edit?usp=sharing">6.7</a>
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/88">SAAS Trial Sign Up Experience</a> 
   </td>
   <td>As a user, when I sign up for a trial, the process is convoluted, complex and confusing. I don't understand why I have to create 2 accounts in order to sign up. I would rather try to figure out how to use the product for free rather than invest my time trying to figure out how to pay.
   </td>
  </tr>
  <tr>
   <td><a href="https://docs.google.com/spreadsheets/d/1j_5Iqh0E5tZhKLrqRk27afa7hnz7BdQfYuu69Scu2TU/edit?usp=sharing">6.0</a>
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/89">Self-Hosted Paid Sign Up Experience</a>
   </td>
   <td>As a user, when I sign up for a paid package, the process is convoluted, complex and confusing. I don't understand why I have to create 2 accounts in order to sign up. I would rather try to figure out how to use the product for free rather than invest my time trying to figure out how to pay.
   </td>
  </tr>
</table>

### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 


### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)

[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

Reports & Dashboards
