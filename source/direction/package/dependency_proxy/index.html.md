---
layout: markdown_page
title: "Category Direction - Dependency Proxy"
description: "Dependency Proxy works hand-in-hand with the planned Dependency Firewall to prevent unverified providers from introducing potential security vulnerabilities."
canonical_path: "/direction/package/dependency_proxy/"
---

- TOC
{:toc}

## Dependency Proxy

Many projects depend on a growing number of packages that must be fetched from external sources with each build. This slows down build times and introduces availability issues into the supply chain. ​​For organizations, this presents a critical problem. By providing a mechanism for storing and accessing external packages, we enable faster and more reliable builds.

Our vision for the Dependency Proxy is to provide a product that will provide fast, reliable access to all of your dependencies, whether they are hosted on GitLab or any other vendor. In addition, the Dependency Proxy will work hand-in-hand with the planned [Dependency Firewall](/direction/package/dependency_firewall/), which will help to prevent any unknown or unverified providers from introducing potential security vulnerabilities.

Currently the Dependency Proxy allows you to proxy and cache images from DockerHub. This can help you to speed up your pipelines and reduce your external dependencies. However this is only the first step. In the coming milestones, we will expand the Dependency Proxy from a single, hardcoded endpoint, to the place where you can setup and manage all of your registries (both packages and images) in one place. 

There are a few important terms that are worth sharing:

- **Hosted registry:** A registry that is hosted on GitLab.
- **Remote registry:** A proxy to a registry located on a remote server
- **Virtual registry:** collection of hosted and remote registries accessed through a single logical URL. This hides the access details of the underlying repositories letting users work with a single, well-known URL.

Our current plan is to:

- Continue to support proxying and caching images from DockerHub.
- Expand the Dependency Proxy to allow for the creation and management of remote and virtual registries. 
  - [Maven](https://gitlab.com/groups/gitlab-org/-/epics/3610)
  - [NPM](https://gitlab.com/groups/gitlab-org/-/epics/3608)
  - [PyPI](https://gitlab.com/groups/gitlab-org/-/epics/3612)
  - [Container Images](https://gitlab.com/groups/gitlab-org/-/epics/3756)
  - [NuGet](https://gitlab.com/groups/gitlab-org/-/epics/3611)
  - [Conan](https://gitlab.com/groups/gitlab-org/-/epics/3613)
  
This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Proxy)
- [Overall Vision](/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## Usecases listed

1. Provide a single method of reaching upstream package management utilities, in the event they are not otherwise reachable. 
1. Cache images and packages for faster build times.
1. Track which dependencies are utilized by which projects when pulled through the proxy.
1. Audit logs in order to find out exactly what happened and with what code.
1. Operate when fully cut off from the internet with local dependencies.

## User flow

The below diagram demonstrates how you can use the Dependency Proxy to create a virtual registry which will look for and fetch dependencies from your hosted and remote registries. This will allow you to download all of your dependencies with a single URL, instead of having to remember which packages are hosted where. 

[![Diagram](https://mermaid.ink/img/eyJjb2RlIjoiZmxvd2NoYXJ0IExSXG5cdEFbR2l0TGFiIEhvc3RlZCByZWdpc3RyeV1cbiAgQltQcm9qZWN0IGNvZGVdXG4gIENbVmlydHVhbCByZWdpc3RyeV1cbiAgRFtQaXBlbGluZV1cbiAgRVtSZWxlYXNlXVxuICBGW1JlbW90ZSByZWdpc3RyaWVzXVxuICBHW0RlcGVuZGVuY3kgRmlyZXdhbGxdXG5cbiAgQSA8LS0-IENcbiAgQiAtLT4gRFxuICBEIC0tPiBFXG4gIEYgLS0-IEdcbiAgRyAtLT4gQ1xuICBDIDwtLT4gRCIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)](https://mermaid-js.github.io/docs/mermaid-live-editor-beta/#/edit/eyJjb2RlIjoiZmxvd2NoYXJ0IExSXG5cdEFbR2l0TGFiIEhvc3RlZCByZWdpc3RyeV1cbiAgQltQcm9qZWN0IGNvZGVdXG4gIENbVmlydHVhbCByZWdpc3RyeV1cbiAgRFtQaXBlbGluZV1cbiAgRVtSZWxlYXNlXVxuICBGW1JlbW90ZSByZWdpc3RyaWVzXVxuICBHW0RlcGVuZGVuY3kgRmlyZXdhbGxdXG5cbiAgQSA8LS0-IENcbiAgQiAtLT4gRFxuICBEIC0tPiBFXG4gIEYgLS0-IEdcbiAgRyAtLT4gQ1xuICBDIDwtLT4gRCIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)
**Note:** *The above diagram shows all of your dependencies being resolved through the Dependency Proxy. Usage of this feature is not required. You can easily use your hosted and remote registries without grouping them in a virtual registry.*

## What's next & why

Next, we have [gitlab-#208080](https://gitlab.com/gitlab-org/gitlab/-/issues/208080) which will resolve a bug in which images are not being successfully pulled from the cache. 

Then, [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/-/issues/11582) will enable the Dependency Proxy for private projects. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2920](https://gitlab.com/groups/gitlab-org/-/epics/2920), which includes links and expected timing for each issue.

## Competitive landscape

* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Docker+Registry#DockerRegistry-RemoteDockerRepositories)
* [Nexus](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-ProxyRepository)

Artifactory is the leader in this category. They offer 'remote repositories' which serve as a caching repository for various package manager integrations. Utilizing the command line, API or a user interface, a user may create policies and control caching and proxying behavior. A Docker image or package may be requested from a remote repository on demand and if no content is available it will be fetched and cached according to the user's policies. In addition, they offer support for many of major packaging formats in use today. For storage optimization, they offer check-sum based storage, deduplication, copying, moving and deletion of files.

The below tables outline our current capabilities compared to JFrog's Artifactory and Sonatype's Nexus. 

| Container Registry | GitLab | Artifactory| Nexus   |
| -------            | ------ | ----       | ----    |
| Local registries   |  ✔️     |    ✔️       |   ✔️     |
| Remote registries  |  [Coming soon*](https://gitlab.com/groups/gitlab-org/-/epics/3756)     |    ✔️       |   ✔️     |
| Virtual registries |  [Coming soon](https://gitlab.com/groups/gitlab-org/-/epics/3756)      |    ✔️       |   ✔️     |

**The Dependency Proxy currently supports one hardcoded remote registry, which allows you to proxy and cache container images hosted on DockerHub.*

| Package Registry | GitLab | Artifactory| Nexus   |
| -------          | ------ | ----       | ----    |
| Local registries   |  ✔️     |    ✔️       |   ✔️     |
| Remote registries  |  [Coming soon](https://gitlab.com/groups/gitlab-org/-/epics/2614)     |    ✔️       |   ✔️     |
| Virtual registries |  [Coming soon](https://gitlab.com/groups/gitlab-org/-/epics/2614)     |    ✔️       |   ✔️     |

## Top Customer Success/Sales issue(s)

The top customer success issue is [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), which will introduce authentication and allow users to leverage the Dependency Proxy with private projects.

## Top internal customer issue(s)

The top internal customer issue is [gitlab-#496](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/496) which will update Gitlab.com to use the Dependency Proxy for caching frequently used images from DockerHub.

## Top Vision Item(s)

Our top vision item is the epic [gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614), which will expand the Dependency Proxy to work with the GitLab Package Registry. Once implemented, Admin will be able to create remote and virtual registries, so that they can resolve all of their Group's dependencies with a single, logical URL. 

Our plan is to start with Maven, then move on to NPM, NuGet, and PyPI. 
