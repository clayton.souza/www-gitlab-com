---
layout: markdown_page
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-06-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 136       | 10.55%          |
| Based in EMEA                               | 345       | 26.76%          |
| Based in LATAM                              | 19        | 1.47%           |
| Based in NORAM                              | 789       | 61.21%          |
| **Total Team Members**                      | **1,289** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 900       | 69.82%          |
| Women                                       | 389       | 30.18%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,289** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 70        | 74.47%          |
| Women in Leadership                         | 24        | 25.53%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **94**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 440       | 81.33%          |
| Women in Development                        | 101       | 18.67%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **541**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.27%           |
| Asian                                       | 49        | 6.62%           |
| Black or African American                   | 21        | 2.84%           |
| Hispanic or Latino                          | 41        | 5.54%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 29        | 3.92%           |
| White                                       | 447       | 60.41%          |
| Unreported                                  | 151       | 20.41%          |
| **Total Team Members**                      | **740**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 8.88%           |
| Black or African American                   | 4         | 1.87%           |
| Hispanic or Latino                          | 9         | 4.21%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.21%           |
| White                                       | 136       | 63.55%          |
| Unreported                                  | 37        | 17.29%          |
| **Total Team Members**                      | **214**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 12.16%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 4.05%           |
| White                                       | 47        | 63.51%          |
| Unreported                                  | 15        | 20.27%          |
| **Total Team Members**                      | **74**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 116       | 9.00%           |
| Black or African American                   | 34        | 2.64%           |
| Hispanic or Latino                          | 66        | 5.12%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 37        | 2.87%           |
| White                                       | 720       | 55.86%          |
| Unreported                                  | 314       | 24.36%          |
| **Total Team Members**                      | **1,289** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 56        | 10.35%          |
| Black or African American                   | 10        | 1.85%           |
| Hispanic or Latino                          | 26        | 4.81%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 15        | 2.77%           |
| White                                       | 303       | 56.01%          |
| Unreported                                  | 131       | 24.21%          |
| **Total Team Members**                      | **541**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.64%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.06%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.19%           |
| White                                       | 57        | 60.64%          |
| Unreported                                  | 23        | 24.47%          |
| **Total Team Members**                      | **94**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.40%           |
| 25-29                                       | 231       | 17.92%          |
| 30-34                                       | 346       | 26.84%          |
| 35-39                                       | 285       | 22.11%          |
| 40-49                                       | 280       | 21.72%          |
| 50-59                                       | 111       | 8.61%           |
| 60+                                         | 16        | 1.24%           |
| Unreported                                  | 1         | 0.16%           |
| **Total Team Members**                      | **1,289** | **100%**        |


Source: GitLab's HRIS, BambooHR
