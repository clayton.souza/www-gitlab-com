---
layout: markdown_page
title: "TMRG - GitLab Women"
description: "An overview of our remote TMRG GitLab Women"
canonical_path: "/company/culture/inclusion/erg-gitlab-women/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
You will be provided an overview of our remote TMRG GitLab Women


## Mission



## Leads
* [Kyla Gradin](https://about.gitlab.com/company/team/#kyla)
* [Joyce Tompsett](https://about.gitlab.com/company/team/#Tompsett)


## Upcoming Events 



## Additional Resources
