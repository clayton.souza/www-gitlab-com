---
layout: markdown_page
title: "Upstream Diversity Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value            |
|--------------|------------------|
| Date Created | August 1, 2020   |
| Target Date  | October 31, 2020 |
| Slack        | [#wg_upstream_diversity](https://gitlab.slack.com/archives/C017XNA4LDA) (internal only) |
| Google Doc   | [Upstream Diversity Working Group Agenda](https://docs.google.com/document/d/15m8If-AcX6DTnOHt89kzIetOeu8OhCzpC7Uk2OpP50E/edit) (internal only) |

## Business Goal

GitLab has switched to an outbound-only recruiting model in an effort to enhance both the quality of our recruiting efforts and the diversity of our team simultaneously. One problem left unsolved by this approach is that our technical recruiting efforts are downstream from bottlenecks such as the talent pool of the technology industry in general and the university system, both of which do not reflect society at large. Our diversity value means we have an ethical imperative to try and create more opportunity in the technology industry for currently underrepresented groups. This is despite the fact that it may not benefit GitLab directly because of the timespans involved, and even if it does it may not be measurable.

This working group will design and launch a program to harness the technical talent of the GitLab team to teach technical skills (programming, networking, security, etc) to members of underrepresented groups. To do this we may partner with an existing program that already has students and curriculum. Secondarily, we may try to see if we can help solve infrastructure issues that are holding some people back (such as access to high-bandwidth connections or computers) through a monetary or equipment donation program.

## Exit Criteria

* A list of committed volunteers from the GitLab team to teach
* A partnership with an existing program with students and curriculum
* A launch date for the program
* A decision on the viability of an infrastructure donation program

## Roles and Responsibilities

| Working Group Role    | Person                    | Title                                                |
|-----------------------|---------------------------|------------------------------------------------------|
| Facilitator           | Roos Takken               | Engineering People Business Partner                  |
| Member                | Candace Byrdsong Williams | Diversity and Inclusion Partner                      |
| Member                | Mo Khan                   | Senior Backend Engineer, Secure:Composition Analysis |
| Member                | Robert Marshall           | Distribution Engineer                                |
| Executive Stakeholder | Eric Johnson              | EVP of Engineering                                   |
