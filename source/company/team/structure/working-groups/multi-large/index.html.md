---
layout: markdown_page
title: "Multi-Large Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | June 22, 2020 |
| End Date | TBD |
| Slack           | [#wg_multi-large](https://gitlab.slack.com/archives/C016JU3CZKJ) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1dbJZNAiTVvwJ9ICu10FpxP9AaAVDXDVkATmpzSONztE/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

### Charter

The charter of this working group is to improve the operational efficiency of running multiple, large, independent GitLab sites without needing linear growth on staffing.

### Scope and Definitions 

In this context,

*  **GitLab sites** means GitLab instances under a GitLab domain managed by GitLab (i.e, not operated on behalf of customers)
   *  Automation developed should be applicable and usable to self-managed installations in the future
   *  The scope excludes customer instances to focus on the problem of running multiple large sites fully under our control, where we are the ultimate DRIs for what policies and solutions we adopt as a company to apply to those instances.
*  **Multiple** means up to 10 primary instances
* **large** means instances that start at the [1K users Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/1k_users.html) and can scale beyond the [50K users Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/50k_users.html) to **a million active users**
* **independent** means they're fully isolated from each other, they don't have a common virtual operations centre

Examples of these types of instances would include GitLab.eu, GitLab.cn. The working group may possible consider GitLab Federal, although this has to be an explicit decision, as operating federal entails clearances and such.

#### Sequence Order Of Deliverables

The following list outlines the order of deliverables per Multi-large (FKA Scaling) Working Group Agenda on [2020-07-13](https://docs.google.com/document/d/1dbJZNAiTVvwJ9ICu10FpxP9AaAVDXDVkATmpzSONztE/edit#bookmark=id.dej0jql4zr9f). The goal of this list is to define deliverables for the Working Group such that its charter can be finite.

##### Deprecate NFS

1. Make Web/API work without NFS ([issue tracker](https://gitlab.com/groups/gitlab-org/-/epics/1316#note_377457528))
1. Make CI/CD live traces work without NFS ([issue tracker](https://gitlab.com/groups/gitlab-org/-/epics/3791))
1. Make Pages work without NFS (Epic: [GitLab Pages Architecture Updates](https://gitlab.com/groups/gitlab-org/-/epics/1316#note_377457528))

##### Helm Charts

1. Move Web/API nodes of .com to Helm charts (no issue tracker)
1. Move Pages nodes of .com to Helm Charts (no issue tracker)

##### Operator

1. Make Operator for Openshift (no issue tracker)
1. Make Operator work with GKE (might be little work or no work) (no issue tracker)
1. Use Operator for .com (no issue tracker)

##### Stateful Nodes

1. Not sure when to move stateful nodes (Gitaly/PostgreSQL/etc.)? (no issue tracker)

##### Day 2 Automation

1. Automate Day 2: scaling/backups/restores/upgrades (no issue tracker)

#### Use Case: Day 2 Operations

In this use case, we start with the [1K users Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/1k_users.html) (Day 1) and map the necessary "moves" to grow and migrate said instance thorugh the [available reference architecture milestones](https://docs.gitlab.com/ee/administration/reference_architectures/) (Day 2: 2k, 3k, 5k, 10k, 25k, 50k) so that this can be managed within GitLab. Therefore, the working group focuses on the step-functions that scale the sites in code, which includes:

- How the site is migrated from one level to another while the site is online, i.e., extracting services into nodes (database, storage, etc) in an efficient way while reducing up front costs
- How a single team manages multiple, large sites
  - Observability
  - Code deployments
  - Back-ups
  - Protecting against DDoS attacks

### Horizontal Scaling Across Multiple Instances

For clarification, this working group is focused on horizontal scaling across multiple GitLab sites. Single-site scaling (e.g., GitLab.com) is already covered under the charter of the [Scalability Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/).

## Roles and Responsibilities

| Working Group Role                       | Person                          | Title                                    |
|------------------------------------------|---------------------------------|------------------------------------------|
| Executive Stakeholder                    | Steve Loyd  | VP of Infrastructure |
| Facilitator                              | Gerardo "Gerir" Lopez-Fernandez (interim) | Engineering Fellow, Infrastructure            |
| DRI           | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure |
| Functional Lead                          | Nailia Iskhakova                | Software Engineer in Test, Database      |
| Functional Lead                          | Andrew Thomas                    | Principal Product Manager, Enablement  |
| Functional Lead                          | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure       |
| Member                                   | Chun Du                         | Director of Engineering, Enablement      |
| Member                                   | Steven Wilson                   | Manager, Distribution                    |
| Member                                   | Jason Plum                      | Staff Engineer, Distribution             |
| Member                                   | Tanya Pazitny                   | Quality Engineering Manager, Enablement  |
| Member                                   | Mek Stittri                     | Director of Quality Engineering          |
| Member | Marin Jankovski | Sr Engineering Manager, Infrastructure, Delivery & Scalability |
| Member | Brent Newton | Director, Infrastructure, Reliability Engineering |

## Related Links

- [Working group kick off meeting](https://www.youtube.com/watch?v=TguakWdOPlw&feature=youtu.be)
- [Defining Day-2 Operations - DZone Agile](https://dzone.com/articles/defining-day-2-operations)
- [Scalability Team - GitLab](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/)
